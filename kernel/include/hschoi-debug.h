

/*************************************************************************/
#if 1
#include <linux/delay.h>
#define PRINTF  printk
//#define PRINTF  pr_info
//#define PRINTF OSA_printf
#define MDELAY  mdelay
#else
#define PRINTF  
//#define PRINTF OSA_printf
#define MDELAY  
#endif




/*************************************************************************/
#if 1 //defined(CONFIG_DEBUG_HSCHOI_MSG) 
#define myPRN(level, fmt, args...) \
    do{ \
    ((void)PRINTF("\t\t"level" : "fmt, ##args)); \
    }while(0)

#define myDBG(level, fmt, args...) \
    do{ \
    ((void)PRINTF("\t\t"level": %s() "fmt" ... %s@%d\r\n", __FUNCTION__, ##args, __FILE__, __LINE__ )); \
    }while(0)

#define myKDBG(level, fmt, args...) \
    do{ \
    ((void)PRINTF(KERN_DEBUG "\t\t"level": %s() "fmt" ... %s@%d\r\n", __FUNCTION__, ##args, __FILE__, __LINE__ )); \
    }while(0)

    
#else
#define myPRN(level, fmt, args...)
#define myDBG(level, fmt, args...)
#define myKDBG(level, fmt, args...)
#endif

#define myWAIT(waitCnt, waitStep, run) \
    { \
        int cnt=0; \
        while (cnt++ < waitCnt) \
        { \
            MDELAY(waitStep); \
            run;\
        } \
    }



#define hPRN(fmt, args...)       myPRN("\t\t   ", fmt, ##args)
#define hDBG(fmt, args...)       myDBG("\t\tDBG:", fmt, ##args)
#define hINF(fmt, args...)       myPRN("\t\tINF", fmt, ##args)
#define hERR(fmt, args...)       myDBG("\t\tERR", fmt, ##args)



/*************************************************************************/
//#define __HSCHOI_DEBUG_ION__ 

#if 0
#define d0PRN(fmt, args...) myPRN("\td0", fmt, ##args)
#define d0DBG(fmt, args...) myDBG("\td0", fmt, ##args) 
#define d0WAIT(waitCnt) myWAIT(waitCnt, 10, d0DBG(""))
    
#else
#define d0PRN(fmt, args...)
#define d0DBG(fmt, args...)
#define d0WAIT(waitCnt)
#endif


/*************************************************************************/
#define __HSCHOI_DEBUG_DISPLAY__ 

#if 1
#define d1PRN(fmt, args...) myPRN("\td1", fmt, ##args)
#define d1DBG(fmt, args...) myDBG("\td1", fmt, ##args) 
#define d1WAIT(waitCnt) myWAIT(waitCnt, 10, d1DBG(""))
    
#else
#define d1PRN(fmt, args...)
#define d1DBG(fmt, args...)
#define d1WAIT(waitCnt)
#endif



