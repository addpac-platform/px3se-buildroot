#ifndef BASEWIDGET_H
#define BASEWIDGET_H

#include <QObject>
#include <QWidget>

class BaseWidget : public QWidget
{
    Q_OBJECT
public:
    explicit BaseWidget(QWidget *parent = 0);
    void setBackgroundColor(int rValue, int gValue, int bValue);
    void setTextColorWhite();
    void setTextColorBlack();

    static void setWidgetFontBold(QWidget *widget);
    static void setWidgetFontSize(QWidget *widget, int size);

protected:
    virtual void paintEvent(QPaintEvent *);
    virtual void mousePressEvent(QMouseEvent *);
    virtual void mouseMoveEvent(QMouseEvent*);
    virtual void mouseReleaseEvent(QMouseEvent *);

private:
    int m_objectId;
};


#if 1
#define tDBG
#define qMyDbg
#define DBG
#else
#include <time.h>

extern "C" int usleep(unsigned int usec);

#define qMyDbg  QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO, "APP_VIDEO ").mydbg

#define DBG     \
    printf("|\n|\n|\n|\n|\n"); \
    qMyDbg

#define tDBG(__VA_ARGS__)     \
    printf("========================\r\n"); \
    printf(__VA_ARGS__); \
    printf("|\n|\n|\n|\n|\n"); \
    qMyDbg(""); \
    usleep(3000000)
#endif    
    

#endif // BASEWIDGET_H
