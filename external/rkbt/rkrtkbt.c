#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <string.h>
#include <sys/time.h>
#include <stdlib.h>

int main (int argc, char **argv) {
	int ret;
	char cmd[512]={0};

	usleep(2*1000*1000);
	system("echo 1 > /sys/class/rfkill/rfkill0/state");
	usleep(1*1000*1000);
	if(argv[1] != NULL) {
		fprintf(stderr,"rkbt argv %s\n",argv[1]);
		sprintf(cmd,"brcm_patchram_plus --enable_hci --no2bytes --tosleep 200000 --baudrate 1500000 --patchram /system/etc/firmware/bcm43438a1.hcd /dev/%s &",argv[1]);
		system(cmd);
	} else {
		system("brcm_patchram_plus --enable_hci --no2bytes --tosleep 200000 --baudrate 1500000 --patchram /system/etc/firmware/bcm43438a1.hcd /dev/ttyS0 &");
	}
	usleep(5*1000*1000);
	system("/usr/libexec/bluetooth/bluetoothd --compat -n &");
	system("sdptool add A2SNK");
	system("hciconfig hci0 up");
	system("hciconfig hci0 piscan");
	system("pulseaudio --start -v");
    	usleep(2000*1000);
    	system("pulseaudio --start -v");//try again,confirm pulseaudio is start
}
