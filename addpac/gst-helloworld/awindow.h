#ifndef __A_WINDOW_H__
#define __A_WINDOW_H__

#include "test.h"

#include "asurface.h"
#include "surfaceformat.h"

class AWindow : public ASurface
{
public:
    enum Visibility {
        Hidden = 0,
        AutomaticVisibility,
        Windowed,
        Minimized,
        Maximized,
        FullScreen
    };
    

    enum AncestorMode {
        ExcludeTransients,
        IncludeTransients
    };    

    
    explicit AWindow(AWindow *screen = Q_NULLPTR);
    //explicit QWindow(AWindow *parent);
    virtual ~AWindow();
    SurfaceFormat requestedFormat() const;

    
private:
    SurfaceFormat reqFormat;

    PlatformSurface *surfaceHandle() const override;

};


#endif // __A_WINDOW_H__

