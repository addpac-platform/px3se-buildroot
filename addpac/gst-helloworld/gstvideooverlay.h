#ifndef __ADDPAC_GST_VIDEO_OVERLAY_H__
#define __ADDPAC_GST_VIDEO_OVERLAY_H__


#include "gstutils.h"
#include "gstbushelper.h"
#include "gstbufferprobe.h"

class apGstVideoOverlay
        : public GstSyncMessageFilter
        , public GstBusMessageFilter
        , private GstBufferProbe
{
public:
    explicit apGstVideoOverlay(char *);
    virtual ~apGstVideoOverlay();

    GstElement *videoSink() const;
    QSize nativeVideoSize() const;

    void setWindowHandle(WId id);
    void expose();
    void setRenderRectangle(const QRect &rect);

    bool isActive() const;

    AspectRatioMode aspectRatioMode() const;
    void setAspectRatioMode(AspectRatioMode mode);

    int brightness() const;
    void setBrightness(int brightness);

    int contrast() const;
    void setContrast(int contrast);

    int hue() const;
    void setHue(int hue);

    int saturation() const;
    void setSaturation(int saturation);

    bool processSyncMessage(const apGstMessage &message) override;
    bool processBusMessage(const apGstMessage &message) override;

#if 0
Q_SIGNALS:
    void nativeVideoSizeChanged();
    void activeChanged();
    void brightnessChanged(int brightness);
    void contrastChanged(int contrast);
    void hueChanged(int hue);
    void saturationChanged(int saturation);
#endif

private:
    GstElement *findBestVideoSink() const;
    void setWindowHandle_helper(WId id);
    void updateIsActive();
    void probeCaps(GstCaps *caps) override;
    static void showPrerollFrameChanged(GObject *, GParamSpec *, apGstVideoOverlay *);

    GstElement *m_videoSink;
    QSize m_nativeVideoSize;
    bool m_isActive;

    bool m_hasForceAspectRatio;
    bool m_hasBrightness;
    bool m_hasContrast;
    bool m_hasHue;
    bool m_hasSaturation;
    bool m_hasShowPrerollFrame;

    bool m_hasDisplayRatio; //gstreamer1-rockchip-extra

    WId m_windowId;
    AspectRatioMode m_aspectRatioMode;
    int m_brightness;
    int m_contrast;
    int m_hue;
    int m_saturation;
};


#endif //__ADDPAC_GST_VIDEO_OVERLAY_H__
