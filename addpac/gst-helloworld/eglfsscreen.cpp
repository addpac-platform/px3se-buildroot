#include "eglfsscreen.h"


EglFSScreen::EglFSScreen(EGLDisplay dpy)
    : m_dpy(dpy),
      m_surface(EGL_NO_SURFACE)
//    , m_cursor(0)
{
  //m_cursor = qt_egl_device_integration()->createCursor(this);
}

EglFSScreen::~EglFSScreen()
{

//#ifndef QT_NO_OPENGL
//    QOpenGLCompositor::destroy();
//#endif

}



QRect EglFSScreen::geometry() const
{
    
    QRect r = rawGeometry();

    static int rotation = 0; //qEnvironmentVariableIntValue("QT_QPA_EGLFS_ROTATION");
    switch (rotation) {
    case 0:
    case 180:
    case -180:
        break;
    case 90:
    case -90: {
        int h = r.height();
        r.setHeight(r.width());
        r.setWidth(h);
        break;
    }
    default:
        hDBG("Invalid rotation %d specified in QT_QPA_EGLFS_ROTATION", rotation);
        break;
    }

    return r;
}

QRect EglFSScreen::rawGeometry() const
{
    return QRect(0,0,0,0); //QPoint(0, 0), qt_egl_device_integration()->screenSize());
}


