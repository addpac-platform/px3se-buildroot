#include "openglcompositor.h"
#include "openglframebufferobject.h"
#include "openglcontext.h"

static OpenGLCompositor *compositor = 0;


OpenGLCompositor::OpenGLCompositor()
    : m_context(0)
    , m_targetWindow(0)
    , m_rotation(0)
{
    assert(!compositor);
    hDBG("TODO: register timeout event ..");
}


OpenGLCompositor::~OpenGLCompositor()
{
    assert(compositor == this);

   // m_blitter.destroy();
    compositor = 0;
}

void OpenGLCompositor::setTarget(OpenGLContext *context, AWindow *targetWindow,
                                  const QRect &nativeTargetGeometry)
{
    m_context = context;
    m_targetWindow = targetWindow;
    m_nativeTargetGeometry = nativeTargetGeometry;
}


OpenGLCompositor *OpenGLCompositor::instance()
{
    if (!compositor)
        compositor = new OpenGLCompositor;
    return compositor;
}


void OpenGLCompositor::destroy()
{
    delete compositor;
    compositor = 0;
}

#define TEST_OPENGL

#ifdef TEST_OPENGL
#include "stb_image.h"

void renderOpenGL()
{
    int width, height, nrChannels;
    unsigned char *data = stbi_load("container.jpg", &width, &height, &nrChannels, 0);


    stbi_image_free(data);

}
#endif


extern int flag;
void OpenGLCompositor::renderAll(OpenGLFramebufferObject *fbo)
{
    static int i=0;
    //if (fbo)
//        fbo->bind();
    
    #if 1
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);   
    
    glViewport(0, 0, m_nativeTargetGeometry.width(), m_nativeTargetGeometry.height());
    #else
    //if(flag > 300)
    {
        printf("%d:gClear\r\n", flag);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);   
    float v= 0.01 *(i++& 0xFF);       
    glClearColor(0.15, v, 0.5, 0.10);
    glViewport(0, 0, m_nativeTargetGeometry.width(), m_nativeTargetGeometry.height());
    }
    //glViewport(100, 100, 800, 600);
    #endif
    //if (!m_blitter.isCreated())
    //    m_blitter.create();

    //m_blitter.bind();
#if 0
    for (int i = 0; i < m_windows.size(); ++i)
        m_windows.at(i)->beginCompositing();

    qMyDbg("window# : %d", m_windows.size());
    
    for (int i = 0; i < m_windows.size(); ++i)
        render(m_windows.at(i));

    m_blitter.release();
    if (!fbo){
                qMyDbg("swapBuffers");
        m_context->swapBuffers(m_targetWindow);
    }
    else
        fbo->release();

    for (int i = 0; i < m_windows.size(); ++i)
        m_windows.at(i)->endCompositing();

    #else
    assert(m_context);
    hDBG("%p,%p", m_context, m_targetWindow);
    m_context->swapBuffers(m_targetWindow);
    #endif


    
    flag++;
}







