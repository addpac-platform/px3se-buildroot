#ifndef __EGLFS_INTEGRATION_H__
#define __EGLFS_INTEGRATION_H__



#include "kmsdevice.h"
#include "eglfskmsgbmdevice.h"

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <stdint.h>

#include "platformwindow.h"
#include "eglfswindow.h"

#include "surfaceformat.h"

void eglfs_initialize();

EGLConfig chooseConfig(EGLDisplay display, const SurfaceFormat &format);


EGLNativeWindowType createNativeWindow(
    PlatformScreen *ps,
    const QSize &size,
    const SurfaceFormat &format);

EGLNativeWindowType createNativeOffscreenWindow(const SurfaceFormat &format);
void destroyNativeWindow(EGLNativeWindowType window);

#endif // __EGLFS_INTEGRATION_H__

