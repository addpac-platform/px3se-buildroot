#ifndef __OFFSCREEN_SURFACE_H__
#define __OFFSCREEN_SURFACE_H__

#include "test.h"
#include "awindow.h"
#include "ascreen.h"
#include "asurface.h"
#include "platformoffscreensurface.h"
#include "platformsurface.h"

class PlatformOffscreenSurface;
class PlatformSurface;

class OffscreenSurface : public ASurface
{
public:

    explicit OffscreenSurface(AScreen *screen = Q_NULLPTR);
    virtual ~OffscreenSurface();

    void create();
    void destroy();
    PlatformScreen *screen(){ return m_screen; }
    void setFormat(SurfaceFormat fmt){ m_requestedFormat = fmt; }
    SurfaceFormat requestedFormat() const { return m_requestedFormat; }

    void setScreen(PlatformScreen *newScreen);

    PlatformSurface *surfaceHandle() const override;


private:
    //AScreen *screen;
    PlatformScreen *m_screen; 


    ASurface::SurfaceType surfaceType;
    PlatformOffscreenSurface *platformOffscreenSurface;
    AWindow *offscreenWindow;
    SurfaceFormat m_requestedFormat;


};


#endif // __OFFSCREEN_SURFACE_H__
