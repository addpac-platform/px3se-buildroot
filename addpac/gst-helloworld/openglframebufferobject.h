// openglframebufferobject.h
// OPENGLFRAMEBUFFEROBJECT

#ifndef __OPENGLFRAMEBUFFEROBJECT_H__
#define __OPENGLFRAMEBUFFEROBJECT_H__

#include "test.h"
#include "gstutils.h"
#include <GLES2/gl2.h>

#define ENABLE_COLORATTACH

class OpenGLFramebufferObjectFormat;
class OpenGLSharedResourceGuard;
class OpenGLContext;

class OpenGLFramebufferObject
{
    
public:
    enum Attachment {
        NoAttachment,
        CombinedDepthStencil,
        Depth
    };

    explicit OpenGLFramebufferObject(const QSize &size, const OpenGLFramebufferObjectFormat &format);
    //OpenGLFramebufferObject(int width, int height, GLenum target = GL_TEXTURE_2D);
    virtual ~OpenGLFramebufferObject();

#ifdef ENABLE_COLORATTACH
    struct ColorAttachment {
        ColorAttachment() : internalFormat(0), guard(0) { }
        ColorAttachment(const QSize &size, GLenum internalFormat)
            : size(size), internalFormat(internalFormat), guard(0) { }
        QSize size;
        GLenum internalFormat;
        OpenGLSharedResourceGuard *guard;
    };
    
    vector<ColorAttachment> colorAttachments;
#endif

    static bool bindDefault();


    void initTexture(int idx);
    bool checkFramebufferStatus(OpenGLContext *ctx) const;

    


private:
    void init(const QSize &size);

    OpenGLFramebufferObjectFormat *m_format;

    OpenGLSharedResourceGuard *fbo_guard;
    
    QSize dsSize;
    GLenum target;
    bool valid;
};




class OpenGLFramebufferObjectFormat
{
public:
    OpenGLFramebufferObjectFormat();
    ~OpenGLFramebufferObjectFormat();




public:
    int samples;
    OpenGLFramebufferObject::Attachment attachment;
    GLenum target;
    GLenum internal_format;
    bool mipmap;



};


#endif // __OPENGLFRAMEBUFFEROBJECT_H__

