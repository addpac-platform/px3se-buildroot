#ifndef __A_SCREEN_H__
#define __A_SCREEN_H__

// abstract screen class

class PlatformScreen;

class AScreen 
{
public:
    ~AScreen();
    explicit AScreen(PlatformScreen *screen);
    
    PlatformScreen *handle() const;

private:

    PlatformScreen *platformScreen;

};

#endif //__A_SCREEN_H__


