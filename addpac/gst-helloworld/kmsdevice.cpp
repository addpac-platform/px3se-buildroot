
#include "kmsdevice.h"
#include <algorithm>

#include <libkms/libkms.h>

//#define qDebug()
#define ARRAY_LENGTH(a) (sizeof (a) / sizeof (a)[0])

#define qCDebug(x) qDebug()

enum OutputConfiguration {
    OutputConfigOff,
    OutputConfigPreferred,
    OutputConfigCurrent,
    OutputConfigMode,
    OutputConfigModeline
};

int KmsDevice::crtcForConnector(drmModeResPtr resources, drmModeConnectorPtr connector)
{
    for (int i = 0; i < connector->count_encoders; i++) {
        drmModeEncoderPtr encoder = drmModeGetEncoder(m_dri_fd, connector->encoders[i]);
        if (!encoder) {
            qWarning() << "Failed to get encoder";
            continue;
        }

        quint32 possibleCrtcs = encoder->possible_crtcs;
        drmModeFreeEncoder(encoder);

        for (int j = 0; j < resources->count_crtcs; j++) {
            bool isPossible = possibleCrtcs & (1 << j);
            bool isAvailable = !(m_crtc_allocator & 1 << resources->crtcs[j]);

            if (isPossible && isAvailable)
                return j;
        }
    }

    return -1;
}

static const char * const connector_type_names[] = { // must match DRM_MODE_CONNECTOR_*
    "None",
    "VGA",
    "DVI",
    "DVI",
    "DVI",
    "Composite",
    "TV",
    "LVDS",
    "CTV",
    "DIN",
    "DP",
    "HDMI",
    "HDMI",
    "TV",
    "eDP",
    "Virtual",
    "DSI"
};


static string nameForConnector(const drmModeConnectorPtr connector)
{
    string connectorName("UNKNOWN");

    if (connector->connector_type < ARRAY_LENGTH(connector_type_names))
        connectorName = connector_type_names[connector->connector_type];

    connectorName += std::to_string(connector->connector_type_id);

    return connectorName;
}


static bool parseModeline(const string &text, drmModeModeInfoPtr mode)
{
    char hsync[16];
    char vsync[16];
    float fclock;

    mode->type = DRM_MODE_TYPE_USERDEF;
    mode->hskew = 0;
    mode->vscan = 0;
    mode->vrefresh = 0;
    mode->flags = 0;

    if (sscanf(text.c_str(), "%f %hd %hd %hd %hd %hd %hd %hd %hd %15s %15s",
               &fclock,
               &mode->hdisplay,
               &mode->hsync_start,
               &mode->hsync_end,
               &mode->htotal,
               &mode->vdisplay,
               &mode->vsync_start,
               &mode->vsync_end,
               &mode->vtotal, hsync, vsync) != 11)
        return false;

    mode->clock = fclock * 1000;

    if (strcmp(hsync, "+hsync") == 0)
        mode->flags |= DRM_MODE_FLAG_PHSYNC;
    else if (strcmp(hsync, "-hsync") == 0)
        mode->flags |= DRM_MODE_FLAG_NHSYNC;
    else
        return false;

    if (strcmp(vsync, "+vsync") == 0)
        mode->flags |= DRM_MODE_FLAG_PVSYNC;
    else if (strcmp(vsync, "-vsync") == 0)
        mode->flags |= DRM_MODE_FLAG_NVSYNC;
    else
        return false;

    return true;
}

#if 1
PlatformScreen *KmsDevice::createScreenForConnector(drmModeResPtr resources,
                                                      drmModeConnectorPtr connector,
                                                      VirtualDesktopInfo *vinfo)
{
    const string connectorName = nameForConnector(connector);

    const int crtc = crtcForConnector(resources, connector);
    if (crtc < 0) {
        qWarning() << "No usable crtc/encoder pair for connector" << connectorName;
        return Q_NULLPTR;
    }

    OutputConfiguration configuration;
    QSize configurationSize;
    drmModeModeInfo configurationModeline;
    #ifdef DISABLE_CONFIGURE
    configuration = OutputConfigPreferred;
    #else
    auto userConfig = m_screenConfig->outputSettings();
    auto userConnectorConfig = userConfig.value(QString::fromUtf8(connectorName));
    // default to the preferred mode unless overridden in the config
    const QByteArray mode = userConnectorConfig.value(QStringLiteral("mode"), QStringLiteral("preferred"))
        .toByteArray().toLower();
    if (mode == "off") {
        configuration = OutputConfigOff;
    } else if (mode == "preferred") {
        configuration = OutputConfigPreferred;
    } else if (mode == "current") {
        configuration = OutputConfigCurrent;
    } else if (sscanf(mode.constData(), "%dx%d", &configurationSize.rwidth(), &configurationSize.rheight()) == 2) {
        configuration = OutputConfigMode;
    } else if (parseModeline(mode, &configurationModeline)) {
        configuration = OutputConfigModeline;
    } else {
        qWarning("Invalid mode \"%s\" for output %s", mode.constData(), connectorName.constData());
        configuration = OutputConfigPreferred;
    }
    #endif

    
    if (vinfo) {
        *vinfo = VirtualDesktopInfo();
        #ifdef DISABLE_CONFIGURE
        vinfo->virtualIndex = INT_MAX;
        vinfo->virtualPos = QPoint(0,0);
        vinfo->isPrimary = false;
        #else
        vinfo->virtualIndex = userConnectorConfig.value(QStringLiteral("virtualIndex"), INT_MAX).toInt();
        if (userConnectorConfig.contains(QStringLiteral("virtualPos"))) {
            const QByteArray vpos = userConnectorConfig.value(QStringLiteral("virtualPos")).toByteArray();
            const QByteArrayList vposComp = vpos.split(',');
            if (vposComp.count() == 2)
                vinfo->virtualPos = QPoint(vposComp[0].trimmed().toInt(), vposComp[1].trimmed().toInt());
        }
        if (userConnectorConfig.value(QStringLiteral("primary")).toBool())
            vinfo->isPrimary = true;
        #endif
    }

    const uint32_t crtc_id = resources->crtcs[crtc];

    if (configuration == OutputConfigOff) {
        qDebug() << "Turning off output" << connectorName;
        drmModeSetCrtc(m_dri_fd, crtc_id, 0, 0, 0, 0, 0, Q_NULLPTR);
        return Q_NULLPTR;
    }

    // Skip disconnected output
    if (configuration == OutputConfigPreferred && connector->connection == DRM_MODE_DISCONNECTED) {
        qDebug() << "Skipping disconnected output" << connectorName;
        return Q_NULLPTR;
    }
    // Get the current mode on the current crtc
    drmModeModeInfo crtc_mode;
    memset(&crtc_mode, 0, sizeof crtc_mode);
    if (drmModeEncoderPtr encoder = drmModeGetEncoder(m_dri_fd, connector->connector_id)) {
        drmModeCrtcPtr crtc = drmModeGetCrtc(m_dri_fd, encoder->crtc_id);
        drmModeFreeEncoder(encoder);

        if (!crtc)
            return Q_NULLPTR;

        if (crtc->mode_valid)
            crtc_mode = crtc->mode;

        drmModeFreeCrtc(crtc);
    }


    vector<drmModeModeInfo> modes;
    modes.reserve(connector->count_modes);
    qCDebug(qLcKmsDebug) << connectorName << "mode count:" << connector->count_modes << endl;
    for (int i = 0; i < connector->count_modes; i++) {
        const drmModeModeInfo &mode = connector->modes[i];
        qCDebug(qLcKmsDebug) << "mode" << i << mode.hdisplay << "x" << mode.vdisplay
                                  << '@' << mode.vrefresh << "hz" <<endl;
       modes.push_back(connector->modes[i]);
    }

    int preferred = -1;
    int current = -1;
    int configured = -1;
    int best = -1;

    for (int i = modes.size() - 1; i >= 0; i--) {
        const drmModeModeInfo &m = modes.at(i);

        if (configuration == OutputConfigMode &&
                m.hdisplay == configurationSize.width() &&
                m.vdisplay == configurationSize.height()) {
            configured = i;
        }

        if (!memcmp(&crtc_mode, &m, sizeof m))
            current = i;

        if (m.type & DRM_MODE_TYPE_PREFERRED)
            preferred = i;

        best = i;
    }

    if (configuration == OutputConfigModeline) {
        modes.push_back(configurationModeline);
        configured = modes.size() - 1;
    }

    if (current < 0 && crtc_mode.clock != 0) {
        modes.push_back(crtc_mode);
        current = modes.size() - 1;
    }

    if (configuration == OutputConfigCurrent)
        configured = current;

    int selected_mode = -1;

    if (configured >= 0)
        selected_mode = configured;
    else if (preferred >= 0)
        selected_mode = preferred;
    else if (current >= 0)
        selected_mode = current;
    else if (best >= 0)
        selected_mode = best;

    if (selected_mode < 0) {
        qWarning() << "No modes available for output" << connectorName << endl;
        return Q_NULLPTR;
    } else {
        int width = modes[selected_mode].hdisplay;
        int height = modes[selected_mode].vdisplay;
        int refresh = modes[selected_mode].vrefresh;
        qCDebug(qLcKmsDebug) << "Selected mode" << selected_mode << ":" << width << "x" << height
                                  << '@' << refresh << "hz for output" << connectorName << endl;
    }
    #ifdef DISABLE_CONFIGURE
    QSize physSize(0,0);
    #else
    // physical size from connector < config values < env vars
    int pwidth = qEnvironmentVariableIntValue("QT_QPA_EGLFS_PHYSICAL_WIDTH");
    if (!pwidth)
        pwidth = qEnvironmentVariableIntValue("QT_QPA_PHYSICAL_WIDTH");
    int pheight = qEnvironmentVariableIntValue("QT_QPA_EGLFS_PHYSICAL_HEIGHT");
    if (!pheight)
        pheight = qEnvironmentVariableIntValue("QT_QPA_PHYSICAL_HEIGHT");
    QSizeF physSize(pwidth, pheight);
    if (physSize.isEmpty()) {
        physSize = QSize(userConnectorConfig.value(QStringLiteral("physicalWidth")).toInt(),
                         userConnectorConfig.value(QStringLiteral("physicalHeight")).toInt());
        if (physSize.isEmpty()) {
            physSize.setWidth(connector->mmWidth);
            physSize.setHeight(connector->mmHeight);
        }
    }
    #endif
    qCDebug(qLcKmsDebug) << "Physical size is" << physSize.width() << ","
         << physSize.height() << " mm" << "for output" << connectorName << endl;

    KmsOutput output = {
        connectorName,
        connector->connector_id,
        crtc_id,
        physSize,
        selected_mode,
        false,
        drmModeGetCrtc(m_dri_fd, crtc_id),
        modes,
        connector->subpixel,
        connectorProperty(connector, "DPMS"),
        false,
        0,
        false
    };

    #ifndef DISABLE_CONFIGURE
    bool ok;
    int idx = qEnvironmentVariableIntValue("QT_QPA_EGLFS_KMS_PLANE_INDEX", &ok);
    if (ok) {
        drmModePlaneRes *planeResources = drmModeGetPlaneResources(m_dri_fd);
        if (planeResources) {
            if (idx >= 0 && idx < int(planeResources->count_planes)) {
                drmModePlane *plane = drmModeGetPlane(m_dri_fd, planeResources->planes[idx]);
                if (plane) {
                    output.wants_plane = true;
                    output.plane_id = plane->plane_id;
                    qCDebug(qLcKmsDebug, "Forcing plane index %d, plane id %u (belongs to crtc id %u)",
                            idx, plane->plane_id, plane->crtc_id);
                    drmModeFreePlane(plane);
                }
            } else {
                qWarning("Invalid plane index %d, must be between 0 and %u", idx, planeResources->count_planes - 1);
            }
        }
    }
    #endif

    m_crtc_allocator |= (1 << output.crtc_id);
    m_connector_allocator |= (1 << output.connector_id);

    return createScreen(output);
    
}


drmModePropertyPtr KmsDevice::connectorProperty(drmModeConnectorPtr connector, const string &name)
{
    drmModePropertyPtr prop;

    for (int i = 0; i < connector->count_props; i++) {
        prop = drmModeGetProperty(m_dri_fd, connector->props[i]);
        if (!prop)
            continue;
        if (strcmp(prop->name, name.c_str()) == 0)
            return prop;
        drmModeFreeProperty(prop);
    }

    return Q_NULLPTR;
}

#endif



KmsDevice::KmsDevice(KmsScreenConfig *screenConfig, const string &path)
    : m_screenConfig(screenConfig)
    , m_path(path)
    , m_dri_fd(-1)
    , m_crtc_allocator(0)
    , m_connector_allocator(0)
{
#if 0    
    if (m_path.isEmpty()) {
        m_path = m_screenConfig->devicePath();
        qCDebug(qLcKmsDebug, "Using DRM device %s specified in config file", qPrintable(m_path));
        if (m_path.isEmpty())
            qFatal("No DRM device given");
    } else {
        qCDebug(qLcKmsDebug, "Using backend-provided DRM device %s", qPrintable(m_path));
    }
    #endif
    hDBG("%s", m_path.c_str());

}

KmsDevice::~KmsDevice()
{
}

struct OrderedScreen
{
    OrderedScreen() : screen(nullptr) { }
    OrderedScreen(PlatformScreen *screen, const KmsDevice::VirtualDesktopInfo &vinfo)
        : screen(screen), vinfo(vinfo) { }
    PlatformScreen *screen;
    KmsDevice::VirtualDesktopInfo vinfo;
};

#if 1
std::ostream& operator<<(std::ostream& dbg, const OrderedScreen &s)
{    
    dbg << "OrderedScreen(QPlatformScreen=" << s.screen << " (" << s.screen->name() << ") : "
                  << s.vinfo.virtualIndex
//                  << " / " << s.vinfo.virtualPos
//                  << " / primary: " << s.vinfo.isPrimary
                  << ")";
    return dbg;
}

#if 0
std::ostream& operator<<(std::ostream& dbg, const string &s)
{    
    dbg << s;
    return dbg;
}
#endif 
#endif

static bool orderedScreenLessThan(const OrderedScreen &a, const OrderedScreen &b)
{
    return a.vinfo.virtualIndex < b.vinfo.virtualIndex;
}

void KmsDevice::createScreens()
{

    drmModeResPtr resources = drmModeGetResources(m_dri_fd);
    if (!resources) {
        qWarning() <<"drmModeGetResources failed";
        return;
    }

    vector<OrderedScreen> screens;

    for (int i = 0; i < resources->count_connectors; i++) {

        drmModeConnectorPtr connector = drmModeGetConnector(m_dri_fd, resources->connectors[i]);
        if (!connector)
            continue;

        VirtualDesktopInfo vinfo;
        PlatformScreen *screen = createScreenForConnector(resources, connector, &vinfo);
        if (screen){
            hDBG("append screen:");
            screens.push_back(OrderedScreen(screen, vinfo));
        }

        drmModeFreeConnector(connector);
    }

    drmModeFreeResources(resources);


    // Use stable sort to preserve the original (DRM connector) order
    // for outputs with unspecified indices.
    std::stable_sort(screens.begin(), screens.end(), orderedScreenLessThan);
    qCDebug(qLcKmsDebug) << "Sorted screen list:" << screens.size();

    QPoint pos(0, 0);
    vector<PlatformScreen *> siblings;
    vector<QPoint> virtualPositions;
    int primarySiblingIdx = -1;

    for (const OrderedScreen &orderedScreen : screens) {
        PlatformScreen *s = orderedScreen.screen;
        QPoint virtualPos(0, 0);

        // set up a horizontal or vertical virtual desktop
        if (orderedScreen.vinfo.virtualPos.isNull()) {
            virtualPos = pos;
            
            if (m_screenConfig && m_screenConfig->virtualDesktopLayout() == KmsScreenConfig::VirtualDesktopLayoutVertical)
                pos.ry() += s->geometry().height();
            else
                pos.rx() += s->geometry().width();
            
            qDebug() << "\n" <<s->geometry().width() << "," << s->geometry().height() << endl;
        } else {
            virtualPos = orderedScreen.vinfo.virtualPos;
        }

       
        qCDebug(qLcKmsDebug) << "Adding QPlatformScren " << s << "(" << s->name() << ")"
                             << "to QPA with geometry" << s->geometry().width() << "," \
                             << s->geometry().height() \
                             << " and isPrimary=" << orderedScreen.vinfo.isPrimary << endl;
        // The order in qguiapp's screens list will match the order set by
        // virtualIndex. This is not only handy but also required since for instance
        // evdevtouch relies on it when performing touch device - screen mapping.
        if ( !m_screenConfig || (m_screenConfig && !m_screenConfig->separateScreens())) {
            siblings.push_back(s);
            virtualPositions.push_back(virtualPos);
            if (orderedScreen.vinfo.isPrimary)
                primarySiblingIdx = siblings.size() - 1;
        } else {
            hDBG(" NOT implemented !! : TODO: registerScreen");
            // TODO: registerScreen(s, orderedScreen.vinfo.isPrimary, virtualPos, QList<PlatformScreen *>() << s);
        }
    }


    if ( !m_screenConfig || (m_screenConfig && !m_screenConfig->separateScreens())) {
        // enable the virtual desktop
        for (int i = 0; i < siblings.size(); ++i)
            registerScreen(siblings[i], i == primarySiblingIdx, virtualPositions[i], siblings);
    }




    
}


int KmsDevice::fd() const
{
    return m_dri_fd;
}

string KmsDevice::devicePath() const
{
    return m_path;
}

void KmsDevice::setFd(int fd)
{
    m_dri_fd = fd;
}


void KmsOutput::restoreMode(KmsDevice *device)
{
    if (mode_set && saved_crtc) {
        drmModeSetCrtc(device->fd(),
                       saved_crtc->crtc_id,
                       saved_crtc->buffer_id,
                       0, 0,
                       &connector_id, 1,
                       &saved_crtc->mode);
        mode_set = false;
    }
}

void KmsOutput::cleanup(KmsDevice *device)
{
    if (dpms_prop) {
        drmModeFreeProperty(dpms_prop);
        dpms_prop = nullptr;
    }

    restoreMode(device);

    if (saved_crtc) {
        drmModeFreeCrtc(saved_crtc);
        saved_crtc = nullptr;
    }
}

PlatformScreen::SubpixelAntialiasingType KmsOutput::subpixelAntialiasingTypeHint() const
{
    switch (subpixel) {
    default:
    case DRM_MODE_SUBPIXEL_UNKNOWN:
    case DRM_MODE_SUBPIXEL_NONE:
        return PlatformScreen::Subpixel_None;
    case DRM_MODE_SUBPIXEL_HORIZONTAL_RGB:
        return PlatformScreen::Subpixel_RGB;
    case DRM_MODE_SUBPIXEL_HORIZONTAL_BGR:
        return PlatformScreen::Subpixel_BGR;
    case DRM_MODE_SUBPIXEL_VERTICAL_RGB:
        return PlatformScreen::Subpixel_VRGB;
    case DRM_MODE_SUBPIXEL_VERTICAL_BGR:
        return PlatformScreen::Subpixel_VBGR;
    }
}

void KmsOutput::setPowerState(KmsDevice *device, PlatformScreen::PowerState state)
{
    if (dpms_prop)
        drmModeConnectorSetProperty(device->fd(), connector_id,
                                    dpms_prop->prop_id, (int) state);
}

