#ifndef __ADDPAC_GSTUTILS_H__
#define __ADDPAC_GSTUTILS_H__

#include <ctype.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>


#include <gst/gst.h>
#include <gst/video/video.h>


#include <iostream>
#include <thread>
#include <mutex>
#include <map>
#include <list>
#include <vector>
using namespace std;


#include "test.h"


#if GST_CHECK_VERSION(1,0,0)
# define QT_GSTREAMER_PLAYBIN_ELEMENT_NAME "playbin"
# define QT_GSTREAMER_CAMERABIN_ELEMENT_NAME "camerabin"
# define QT_GSTREAMER_COLORCONVERSION_ELEMENT_NAME "videoconvert"
# define QT_GSTREAMER_RAW_AUDIO_MIME "audio/x-raw"
# define QT_GSTREAMER_VIDEOOVERLAY_INTERFACE_NAME "GstVideoOverlay"
#else
# define QT_GSTREAMER_PLAYBIN_ELEMENT_NAME "playbin2"
# define QT_GSTREAMER_CAMERABIN_ELEMENT_NAME "camerabin2"
# define QT_GSTREAMER_COLORCONVERSION_ELEMENT_NAME "ffmpegcolorspace"
# define QT_GSTREAMER_RAW_AUDIO_MIME "audio/x-raw-int"
# define QT_GSTREAMER_VIDEOOVERLAY_INTERFACE_NAME "GstXOverlay"
#endif



void initializeGst();






void qt_gst_object_ref_sink(gpointer object);
GstCaps *qt_gst_pad_get_current_caps(GstPad *pad);
GstCaps *qt_gst_pad_get_caps(GstPad *pad);
GstStructure *qt_gst_structure_new_empty(const char *name);
gboolean qt_gst_element_query_position(GstElement *element, GstFormat format, gint64 *cur);
gboolean qt_gst_element_query_duration(GstElement *element, GstFormat format, gint64 *cur);
GstCaps *qt_gst_caps_normalize(GstCaps *caps);
const gchar *qt_gst_element_get_factory_name(GstElement *element);
gboolean qt_gst_caps_can_intersect(const GstCaps * caps1, const GstCaps * caps2);
GList *qt_gst_video_sinks();
void qt_gst_util_double_to_fraction(gdouble src, gint *dest_n, gint *dest_d);


// utils
int qstrcmp(const char *str1, const char *str2);


#define Q_UNUSED


// mutex
#    define Q_DECL_NOTHROW  throw()

class QMutexLocker
{
public:
    inline explicit QMutexLocker(mutex *m) Q_DECL_NOTHROW {
        m_p = m;
        m->lock();
        }
    inline ~QMutexLocker() Q_DECL_NOTHROW {
        if(m_p) m_p->unlock();
        }

//    inline void unlock() Q_DECL_NOTHROW {}
//    void relock() Q_DECL_NOTHROW {}
//    inline QMutex *mutex() const Q_DECL_NOTHROW { return NULL; }

private:
    mutex *m_p ;
};



/*---------------------------------------
    QSize
*/
class QSize
{
public :
    inline QSize() {wd=-1; ht=-1;}

    inline QSize(int w, int h)  : wd(w), ht(h) {}

    
    ~QSize() {}
    inline bool operator!=(const QSize &s1)
    { return s1.wd != wd || s1.ht != ht; }

    inline int &rwidth() 
    { return wd; }

    inline int &rheight() 
    { return ht; }

    inline bool isNull() const 
    { return wd==0 && ht==0; }

    inline bool isEmpty() const 
    { return wd<1 || ht<1; }

    inline bool isValid() const 
    { return wd>=0 && ht>=0; }

    inline int width() const 
    { return wd; }

    inline int height() const 
    { return ht; }

    inline void setWidth(int w) 
    { wd = w; }

    inline void setHeight(int h) 
    { ht = h; }

    friend std::ostream& operator<< (std::ostream &out, const QSize &point);


private :
    int wd;
    int ht;
};




class QPoint
{
public:    
    QPoint() : xp(0), yp(0) {}
    QPoint(int xpos, int ypos) : xp(xpos), yp(ypos) {}    
    ~QPoint() {}


    inline int x() const Q_DECL_NOTHROW
    { return xp; }
    inline int y() const Q_DECL_NOTHROW
    { return yp; }

    inline bool isNull() const
        { return xp == 0 && yp == 0; }
    inline int &rx() { return xp; }

    inline int &ry() { return yp; }


private:
    int xp;
    int yp;
};

/*---------------------------------------
    QRect
*/


class QRect
{
public :
    QRect() {x1=y1=0; x2=y2=-1;}
    QRect(int aleft, int atop, int awidth, int aheight) 
        : x1(aleft), y1(atop), x2(aleft + awidth - 1), y2(atop + aheight - 1) {}

    QRect(const QPoint &atopLeft, const QSize &asize) 
        : x1(atopLeft.x()), y1(atopLeft.y()), x2(atopLeft.x()+asize.width() - 1), y2(atopLeft.y()+asize.height() - 1) {}

    
    ~QRect() {}


    inline bool isEmpty() const 
    { return x1 > x2 || y1 > y2; }

    inline int left() const Q_DECL_NOTHROW
    { return x1; }

    inline int top() const Q_DECL_NOTHROW
    { return y1; }

    inline int right() const Q_DECL_NOTHROW
    { return x2; }

    inline int bottom() const Q_DECL_NOTHROW
    { return y2; }

    inline int x() const Q_DECL_NOTHROW
    { return x1; }

    inline int y() const Q_DECL_NOTHROW
    { return y1; }

    inline int width() const Q_DECL_NOTHROW
    { return  x2 - x1 + 1; }

    inline int height() const Q_DECL_NOTHROW
    { return  y2 - y1 + 1; }

    inline QSize size() const Q_DECL_NOTHROW
    { return QSize(width(), height()); }


    inline void setWidth(int w) Q_DECL_NOTHROW
    { x2 = (x1 + w - 1); }

    inline void setHeight(int h) Q_DECL_NOTHROW
    { y2 = (y1 + h - 1); }

    
private:
    int x1;
    int y1;
    int x2;
    int y2;
};





typedef int WId;


enum AspectRatioMode{
    IgnoreAspectRatio =0,   //	The size is scaled freely. The aspect ratio is not preserved.
    KeepAspectRatio =1,     // The size is scaled to a rectangle as large as possible inside a given rectangle, preserving the aspect ratio.
    KeepAspectRatioByExpanding=2  // The size is scaled to a rectangle as small as possible outside a given rectangle, preserving the aspect ratio.
};



namespace QGstUtils {
    QSize capsResolution(const GstCaps *caps);
    QSize capsCorrectedResolution(const GstCaps *caps);
    QSize structureResolution(const GstStructure *s);
    
}


#endif //__ADDPAC_GSTUTILS_H__

