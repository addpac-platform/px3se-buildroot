#ifndef __PLATFORM_OPENGL_CONTEXT_H__
#define __PLATFORM_OPENGL_CONTEXT_H__

#include "test.h"

#include "openglcontext.h"


#define TODO_ENABLE_INTERFACE
//#define TODO_PART1


class PlatformOpenGLContext
{
public:
    PlatformOpenGLContext();
    virtual ~PlatformOpenGLContext();

    virtual void initialize() = 0;
    virtual bool makeCurrent(PlatformSurface *surface) = 0;
    virtual void swapBuffers(PlatformSurface *surface) = 0;


    OpenGLContext *context() const;
    void setContext(OpenGLContext *context);

private:

    OpenGLContext *m_context;
};

#endif// __PLATFORM_OPENGL_CONTEXT_H__

