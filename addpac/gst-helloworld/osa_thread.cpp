

#include <osa_thread.h>

#ifdef HSCHOI_DEBUG_TASK
typedef struct _myParam
{
    OSA_ThrEntryFunc entryFunc;
    void *prm;
    char *strEntryFunc;
    int line;
    int priority;
    int status; // 0: init, 1: run started.
}myParam;

#include <asm/unistd.h>
#include <sys/types.h>

static pid_t gettid(void)
{
    return syscall(__NR_gettid);
}   

extern int avsys_add_taskInfo(int tid, int pri, char *pStrTskFunc);
static void myEntry(myParam *prm)
{
    printf("%s()@%d creates thread id(%d),\r\n", 
        prm->strEntryFunc, prm->line, gettid());

    
    
    avsys_add_taskInfo(gettid(), prm->priority, prm->strEntryFunc);
    
    prm->status = 1;
    prm->entryFunc(prm->prm);
    // prm will be invalid in this point.
    // (prm is the local variable in caller.)
    
}
#endif




#ifdef HSCHOI_DEBUG_TASK
int _OSA_thrCreate(char *pStrEntryFunc, int line, OSA_ThrHndl *hndl, OSA_ThrEntryFunc entryFunc, Uint32 pri, Uint32 stackSize, void *prm)
#else
int OSA_thrCreate(OSA_ThrHndl *hndl, OSA_ThrEntryFunc entryFunc, Uint32 pri, Uint32 stackSize, void *prm)
#endif
{
  int status=OSA_SOK;
  pthread_attr_t thread_attr;
  struct sched_param schedprm;
#ifdef HSCHOI_DEBUG_TASK
    myParam my_prm;    
#endif
  // initialize thread attributes structure
  status = pthread_attr_init(&thread_attr);
  
  if(status!=OSA_SOK) {
    OSA_ERROR("OSA_thrCreate() - Could not initialize thread attributes\n");
    return status;
  }
  
  if(stackSize!=OSA_THR_STACK_SIZE_DEFAULT)
    pthread_attr_setstacksize(&thread_attr, stackSize);

  status |= pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
  status |= pthread_attr_setschedpolicy(&thread_attr, SCHED_FIFO);
    
  if(pri>OSA_THR_PRI_MAX)   
    pri=OSA_THR_PRI_MAX;
  else 
  if(pri<OSA_THR_PRI_MIN)   
    pri=OSA_THR_PRI_MIN;

#if 0//def HSCHOI_DEBUG_TASK
    if(pri == 2)  // mcfw ipc tasks
        pri = 90;
#endif

  schedprm.sched_priority = pri;
  status |= pthread_attr_setschedparam(&thread_attr, &schedprm);

  if(status!=OSA_SOK) {
    OSA_ERROR("OSA_thrCreate() - Could not initialize thread attributes\n");
    goto error_exit;
  }


  #ifdef HSCHOI_DEBUG_TASK
    memset(&my_prm, 0, sizeof(my_prm));
    my_prm.entryFunc = entryFunc;
    my_prm.prm = prm;
    my_prm.strEntryFunc = pStrEntryFunc;
    my_prm.line = line;
    my_prm.priority = pri;
    status = pthread_create(&hndl->hndl, &thread_attr, myEntry, &my_prm);
    while(my_prm.status == 0) OSA_waitMsecs(1);
    
  #else
  status = pthread_create(&hndl->hndl, &thread_attr, entryFunc, prm);
  #endif
  
  if(status != OSA_SOK) {
    OSA_ERROR("OSA_thrCreate() - Could not create thread [%d]\n", status);
    OSA_assert(status == OSA_SOK);
  }
  


error_exit:  
  pthread_attr_destroy(&thread_attr);

  return status;
}

int OSA_thrJoin(OSA_ThrHndl *hndl)
{
  int status=OSA_SOK;
  void *returnVal;
    
  status = pthread_join(hndl->hndl, &returnVal); 
  
  return status;    
}

int OSA_thrDelete(OSA_ThrHndl *hndl)
{
  int status = OSA_SOK;

  if(&hndl->hndl != NULL)
  {
      status = pthread_cancel(hndl->hndl); 
      status = OSA_thrJoin(hndl);
  }
  
  return status;    
}

int OSA_thrChangePri(OSA_ThrHndl *hndl, Uint32 pri)
{
  int status = OSA_SOK;
  struct sched_param schedprm;  

  if(pri>OSA_THR_PRI_MAX)   
    pri=OSA_THR_PRI_MAX;
  else 
  if(pri<OSA_THR_PRI_MIN)   
    pri=OSA_THR_PRI_MIN;
  
  schedprm.sched_priority = pri;  
  status |= pthread_setschedparam(hndl->hndl, SCHED_FIFO, &schedprm);
  
  return status;
}

int OSA_thrExit(void *returnVal)
{
  pthread_exit(returnVal);
  return OSA_SOK;
}


