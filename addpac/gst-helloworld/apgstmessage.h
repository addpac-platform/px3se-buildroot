#ifndef __AP_GST_MESSAGE_H__
#define __AP_GST_MESSAGE_H__

#include "gstutils.h"



class apGstMessage
{
public:
    apGstMessage();
    apGstMessage(GstMessage* message);
    apGstMessage(apGstMessage const& m);
    ~apGstMessage();

    GstMessage* rawMessage() const;

    apGstMessage& operator=(apGstMessage const& rhs);

private:
    GstMessage* m_message;
};




#endif // __AP_GST_MESSAGE_H__


