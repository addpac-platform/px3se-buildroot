
#include "awindow.h"

AWindow::AWindow(AWindow *parent)
    : ASurface(ASurface::Window)
//    , reqFormat(0)
{
    
}

AWindow::~AWindow()
{
}

SurfaceFormat AWindow::requestedFormat() const
{

    return reqFormat;
}

#include "platformwindow.h"
extern PlatformWindow *platformWindow;

PlatformSurface *AWindow::surfaceHandle() const
{
    // from QWindowPrivate::create
    
    hDBG("TODO: set surface from createPlatformWindow()\n");
    return static_cast<PlatformWindow*>(platformWindow);
}

