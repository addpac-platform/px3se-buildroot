#include "platformopenglcontext.h"

PlatformOpenGLContext::PlatformOpenGLContext()
    : m_context(0)
{
}

PlatformOpenGLContext::~PlatformOpenGLContext()
{
}

OpenGLContext *PlatformOpenGLContext::context() const
{
    
    return m_context;
}

void PlatformOpenGLContext::setContext(OpenGLContext *context)
{
    m_context = context;
}

