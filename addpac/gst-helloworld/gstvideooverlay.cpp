



#include "gstvideooverlay.h"

#if !GST_CHECK_VERSION(1,0,0)
#include <gst/interfaces/xoverlay.h>
#else
#include <gst/video/videooverlay.h>
#endif


#define TODO_LATER



apGstVideoOverlay::apGstVideoOverlay(char *elementName)
    : 
    GstBufferProbe(GstBufferProbe::ProbeCaps)
    , m_videoSink(0)
    , m_isActive(false)
    , m_hasForceAspectRatio(false)
    , m_hasBrightness(false)
    , m_hasContrast(false)
    , m_hasHue(false)
    , m_hasSaturation(false)
    , m_hasShowPrerollFrame(false)
    , m_windowId(0)
    , m_aspectRatioMode(KeepAspectRatio)
    , m_brightness(0)
    , m_contrast(0)
    , m_hue(0)
    , m_saturation(0)
{

    assert (elementName != NULL);
    
    m_videoSink = gst_element_factory_make(elementName, NULL);
    
        

    if (m_videoSink) {
        qt_gst_object_ref_sink(GST_OBJECT(m_videoSink)); //Take ownership

        GstPad *pad = gst_element_get_static_pad(m_videoSink, "sink");
        addProbeToPad(pad);
        gst_object_unref(GST_OBJECT(pad));

        m_hasForceAspectRatio = g_object_class_find_property(G_OBJECT_GET_CLASS(m_videoSink), "force-aspect-ratio");
        m_hasBrightness = g_object_class_find_property(G_OBJECT_GET_CLASS(m_videoSink), "brightness");
        m_hasContrast = g_object_class_find_property(G_OBJECT_GET_CLASS(m_videoSink), "contrast");
        m_hasHue = g_object_class_find_property(G_OBJECT_GET_CLASS(m_videoSink), "hue");
        m_hasSaturation = g_object_class_find_property(G_OBJECT_GET_CLASS(m_videoSink), "saturation");
        m_hasShowPrerollFrame = g_object_class_find_property(G_OBJECT_GET_CLASS(m_videoSink), "show-preroll-frame");


        // kmssink from gstreamer-rockchip-extra
        m_hasDisplayRatio = g_object_class_find_property(G_OBJECT_GET_CLASS(m_videoSink), "display-ratio");

        gDBG("m_hasForceAspectRatio(%d), m_hasShowPrerollFrame(%d), m_hasDisplayRatio(%d) \n", 
            m_hasForceAspectRatio, m_hasShowPrerollFrame, m_hasDisplayRatio);
        
        #ifdef TEST_GSTREAMER

        #endif

        if (m_hasShowPrerollFrame) {
            g_signal_connect(m_videoSink, "notify::show-preroll-frame",
                             G_CALLBACK(showPrerollFrameChanged), this);
        }
    }
}

apGstVideoOverlay::~apGstVideoOverlay()
{
    if (m_videoSink) {
        GstPad *pad = gst_element_get_static_pad(m_videoSink, "sink");
        removeProbeFromPad(pad);
        gst_object_unref(GST_OBJECT(pad));
        gst_object_unref(GST_OBJECT(m_videoSink));
    }
}


bool apGstVideoOverlay::isActive() const
{
    return m_isActive;
}

void apGstVideoOverlay::updateIsActive()
{
    if (!m_videoSink)
        return;

    GstState state = GST_STATE(m_videoSink);
    gboolean showPreroll = true;

    if (m_hasShowPrerollFrame)
        g_object_get(G_OBJECT(m_videoSink), "show-preroll-frame", &showPreroll, NULL);

    bool newIsActive = (state == GST_STATE_PLAYING || (state == GST_STATE_PAUSED && showPreroll));

    if (newIsActive != m_isActive) {
        m_isActive = newIsActive;
        #ifdef TODO_LATER
        niPRN("");
        #else
        emit activeChanged();
        #endif
    }
}


void apGstVideoOverlay::showPrerollFrameChanged(GObject *, GParamSpec *, apGstVideoOverlay *overlay)
{
    overlay->updateIsActive();
}


GstElement *apGstVideoOverlay::videoSink() const
{
    return m_videoSink;
}

QSize apGstVideoOverlay::nativeVideoSize() const
{
    return m_nativeVideoSize;
}

void apGstVideoOverlay::setWindowHandle(WId id)
{
    
    m_windowId = id;

    if (isActive())
        setWindowHandle_helper(id);

}



void apGstVideoOverlay::setWindowHandle_helper(WId id)
{
    
#if GST_CHECK_VERSION(1,0,0)
    if (m_videoSink && GST_IS_VIDEO_OVERLAY(m_videoSink)) {
        gst_video_overlay_set_window_handle(GST_VIDEO_OVERLAY(m_videoSink), id);
#else
    if (m_videoSink && GST_IS_X_OVERLAY(m_videoSink)) {
# if GST_CHECK_VERSION(0,10,31)
        gst_x_overlay_set_window_handle(GST_X_OVERLAY(m_videoSink), id);
# else
        gst_x_overlay_set_xwindow_id(GST_X_OVERLAY(m_videoSink), id);
# endif
#endif

        // Properties need to be reset when changing the winId.
        setAspectRatioMode(m_aspectRatioMode);
        setBrightness(m_brightness);
        setContrast(m_contrast);
        setHue(m_hue);
        setSaturation(m_saturation);
    }
}


void apGstVideoOverlay::expose()
{
    if (!isActive())
        return;

#if !GST_CHECK_VERSION(1,0,0)
    if (m_videoSink && GST_IS_X_OVERLAY(m_videoSink))
        gst_x_overlay_expose(GST_X_OVERLAY(m_videoSink));
#else
    if (m_videoSink && GST_IS_VIDEO_OVERLAY(m_videoSink)) {
        gst_video_overlay_expose(GST_VIDEO_OVERLAY(m_videoSink));
    }
#endif
}

void apGstVideoOverlay::setRenderRectangle(const QRect &rect)
{
    int x = -1;
    int y = -1;
    int w = -1;
    int h = -1;

    if (!rect.isEmpty()) {
        x = rect.x();
        y = rect.y();
        w = rect.width();
        h = rect.height();
    }

#if GST_CHECK_VERSION(1,0,0)
    if (m_videoSink && GST_IS_VIDEO_OVERLAY(m_videoSink)){
        gst_video_overlay_set_render_rectangle(GST_VIDEO_OVERLAY(m_videoSink), x, y, w, h);

    }
#elif GST_CHECK_VERSION(0, 10, 29)
    if (m_videoSink && GST_IS_X_OVERLAY(m_videoSink))
        gst_x_overlay_set_render_rectangle(GST_X_OVERLAY(m_videoSink), x, y , w , h);
#else
    Q_UNUSED(x)
    Q_UNUSED(y)
    Q_UNUSED(w)
    Q_UNUSED(h)
#endif
}




bool apGstVideoOverlay::processSyncMessage(const apGstMessage &message)
{ 
    GstMessage* gm = message.rawMessage();

#if !GST_CHECK_VERSION(1,0,0)
    if (gm && (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_ELEMENT) &&
            gst_structure_has_name(gm->structure, "prepare-xwindow-id")) {
#else
      if (gm && (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_ELEMENT) &&
              gst_structure_has_name(gst_message_get_structure(gm), "prepare-window-handle")) {
#endif
        setWindowHandle_helper(m_windowId);
        return true;
    }

    return false;
}

bool apGstVideoOverlay::processBusMessage(const apGstMessage &message)
{ 
    GstMessage* gm = message.rawMessage();

    if (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_STATE_CHANGED &&
            GST_MESSAGE_SRC(gm) == GST_OBJECT_CAST(m_videoSink)) {
        updateIsActive();
    }

    return false;
}

void apGstVideoOverlay::probeCaps(GstCaps *caps)
{
    QSize size = QGstUtils::capsCorrectedResolution(caps);
    if (size != m_nativeVideoSize) {
        m_nativeVideoSize = size;
        #ifdef TODO_LATER
        niPRN("");
        #else
        emit nativeVideoSizeChanged();
        #endif
    }

}




AspectRatioMode apGstVideoOverlay::aspectRatioMode() const
{
    AspectRatioMode mode = KeepAspectRatio;

    if (m_hasForceAspectRatio) {
        gboolean forceAR = false;
        g_object_get(G_OBJECT(m_videoSink), "force-aspect-ratio", &forceAR, NULL);
        if (!forceAR)
            mode = IgnoreAspectRatio;
    }

    return mode;
}

void apGstVideoOverlay::setAspectRatioMode(AspectRatioMode mode)
{
    if (m_hasForceAspectRatio) {

        gDBG(" set force-aspect (%d)\n", mode);
        g_object_set(G_OBJECT(m_videoSink),
                     "force-aspect-ratio",
                     (mode == KeepAspectRatio),
                     (const char*)NULL);
    }

    if (m_hasDisplayRatio) {
        gboolean display_ratio= false;
        g_object_get(G_OBJECT(m_videoSink), "display-ratio", &display_ratio, NULL);


        gDBG(" set display-ratio (%d->%d)\n", display_ratio, (mode == KeepAspectRatio));
        
        g_object_set(G_OBJECT(m_videoSink),
                     "display-ratio",
                     (mode == KeepAspectRatio),
                     (const char*)NULL);
        
    }
    

    m_aspectRatioMode = mode;
}

int apGstVideoOverlay::brightness() const
{
    int brightness = 0;

    if (m_hasBrightness)
        g_object_get(G_OBJECT(m_videoSink), "brightness", &brightness, NULL);

    return brightness / 10;
}

void apGstVideoOverlay::setBrightness(int brightness)
{
    if (m_hasBrightness) {
        g_object_set(G_OBJECT(m_videoSink), "brightness", brightness * 10, NULL);
        #ifdef TODO_LATER
        niPRN("");
        #else
        emit brightnessChanged(brightness);
        #endif
    }

    m_brightness = brightness;
}

int apGstVideoOverlay::contrast() const
{
    int contrast = 0;

    if (m_hasContrast)
        g_object_get(G_OBJECT(m_videoSink), "contrast", &contrast, NULL);

    return contrast / 10;
}

void apGstVideoOverlay::setContrast(int contrast)
{
    if (m_hasContrast) {
        g_object_set(G_OBJECT(m_videoSink), "contrast", contrast * 10, NULL);
        #ifdef TODO_LATER
        niPRN("");
        #else        
        emit contrastChanged(contrast);
        #endif
    }

    m_contrast = contrast;
}

int apGstVideoOverlay::hue() const
{
    int hue = 0;

    if (m_hasHue)
        g_object_get(G_OBJECT(m_videoSink), "hue", &hue, NULL);

    return hue / 10;
}

void apGstVideoOverlay::setHue(int hue)
{
    if (m_hasHue) {
        g_object_set(G_OBJECT(m_videoSink), "hue", hue * 10, NULL);
        #ifdef TODO_LATER
        niPRN("");
        #else        
        emit hueChanged(hue);
        #endif
    }

    m_hue = hue;
}

int apGstVideoOverlay::saturation() const
{
    int saturation = 0;

    if (m_hasSaturation)
        g_object_get(G_OBJECT(m_videoSink), "saturation", &saturation, NULL);

    return saturation / 10;
}

void apGstVideoOverlay::setSaturation(int saturation)
{
    if (m_hasSaturation) {
        g_object_set(G_OBJECT(m_videoSink), "saturation", saturation * 10, NULL);
        #ifdef TODO_LATER
        niPRN("");
        #else
        emit saturationChanged(saturation);
        #endif
    }

    m_saturation = saturation;
}

