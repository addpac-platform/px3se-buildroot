

#include "test.h"

#include <stdio.h>

void test(){
	printf("%s\n", __FUNCTION__);
}


#include <gst/gst.h>

#include <gst/gstvalue.h>
#include <gst/base/gstbasesrc.h>

#include "gstutils.h"


#include "gstplayersession.h"

#include "gstvideowindow.h"

#include <iostream>
#include <map>
using namespace std;








#include <gst/gst.h>

/* Structure to contain all our information, so we can pass it around */
typedef struct _CustomData {
  GstElement *playbin;  /* Our one and only element */

  gint n_video;          /* Number of embedded video streams */
  gint n_audio;          /* Number of embedded audio streams */
  gint n_text;           /* Number of embedded subtitle streams */

  gint current_video;    /* Currently playing video stream */
  gint current_audio;    /* Currently playing audio stream */
  gint current_text;     /* Currently playing subtitle stream */

  GMainLoop *main_loop;  /* GLib's Main Loop */
} CustomData;

/* playbin flags */
typedef enum {
  GST_PLAY_FLAG_VIDEO         = (1 << 0), /* We want video output */
  GST_PLAY_FLAG_AUDIO         = (1 << 1), /* We want audio output */
  GST_PLAY_FLAG_TEXT          = (1 << 2)  /* We want subtitle output */
} GstPlayFlags;

/* Forward definition for the message and keyboard processing functions */
static gboolean handle_message (GstBus *bus, GstMessage *msg, CustomData *data);
static gboolean handle_keyboard (GIOChannel *source, GIOCondition cond, CustomData *data);

int main_gst_tutorial_playbin(int argc, char *argv[]) {
  CustomData data;
  GstBus *bus;
  GstStateChangeReturn ret;
  gint flags;
  GIOChannel *io_stdin;

  /* Initialize GStreamer */
  gst_init (&argc, &argv);

  /* Create the elements */
  data.playbin = gst_element_factory_make ("playbin", "playbin");

  if (!data.playbin) {
    g_printerr ("Not all elements could be created.\n");
    return -1;
  }

  /* Set the URI to play */
  //g_object_set (data.playbin, "uri", "https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_cropped_multilingual.webm", NULL);
  g_object_set (data.playbin, "uri", "file:///opt/dragon.mp4", NULL);
  //g_object_set (data.playbin, "uri", "rtsp://172.16.7.94/v1", NULL);
  

  /* Set flags to show Audio and Video but ignore Subtitles */
  g_object_get (data.playbin, "flags", &flags, NULL);
  flags |= GST_PLAY_FLAG_VIDEO | GST_PLAY_FLAG_AUDIO;
  flags &= ~GST_PLAY_FLAG_TEXT;
  g_object_set (data.playbin, "flags", flags, NULL);

  /* Set connection speed. This will affect some internal decisions of playbin */
  g_object_set (data.playbin, "connection-speed", 56, NULL);

  /* Add a bus watch, so we get notified when a message arrives */
  bus = gst_element_get_bus (data.playbin);
  #ifdef TEST_MESSAGE_HANDLER
  gst_bus_add_watch_full(bus, G_PRIORITY_DEFAULT, (GstBusFunc)handle_message, &data, NULL);
  #else
  gst_bus_add_watch (bus, (GstBusFunc)handle_message, &data);
  #endif

  /* Add a keyboard watch so we get notified of keystrokes */
#ifdef G_OS_WIN32
  io_stdin = g_io_channel_win32_new_fd (fileno (stdin));
#else
  io_stdin = g_io_channel_unix_new (fileno (stdin));
#endif
  g_io_add_watch (io_stdin, G_IO_IN, (GIOFunc)handle_keyboard, &data);

  /* Start playing */
  ret = gst_element_set_state (data.playbin, GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    g_printerr ("Unable to set the pipeline to the playing state.\n");
    gst_object_unref (data.playbin);
    return -1;
  }

  /* Create a GLib Main Loop and set it to run */
  data.main_loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (data.main_loop);

  /* Free resources */
  g_main_loop_unref (data.main_loop);
  g_io_channel_unref (io_stdin);
  gst_object_unref (bus);
  gst_element_set_state (data.playbin, GST_STATE_NULL);
  gst_object_unref (data.playbin);
  return 0;
}

/* Extract some metadata from the streams and print it on the screen */
static void analyze_streams (CustomData *data) {
  gint i;
  GstTagList *tags;
  gchar *str;
  guint rate;

  /* Read some properties */
  g_object_get (data->playbin, "n-video", &data->n_video, NULL);
  g_object_get (data->playbin, "n-audio", &data->n_audio, NULL);
  g_object_get (data->playbin, "n-text", &data->n_text, NULL);

  g_print ("%d video stream(s), %d audio stream(s), %d text stream(s)\n",
    data->n_video, data->n_audio, data->n_text);

  g_print ("\n");
  for (i = 0; i < data->n_video; i++) {
    tags = NULL;
    /* Retrieve the stream's video tags */
    g_signal_emit_by_name (data->playbin, "get-video-tags", i, &tags);
    if (tags) {
      g_print ("video stream %d:\n", i);
      gst_tag_list_get_string (tags, GST_TAG_VIDEO_CODEC, &str);
      g_print ("  codec: %s\n", str ? str : "unknown");
      g_free (str);
      gst_tag_list_free (tags);
    }
  }

  g_print ("\n");
  for (i = 0; i < data->n_audio; i++) {
    tags = NULL;
    /* Retrieve the stream's audio tags */
    g_signal_emit_by_name (data->playbin, "get-audio-tags", i, &tags);
    if (tags) {
      g_print ("audio stream %d:\n", i);
      if (gst_tag_list_get_string (tags, GST_TAG_AUDIO_CODEC, &str)) {
        g_print ("  codec: %s\n", str);
        g_free (str);
      }
      if (gst_tag_list_get_string (tags, GST_TAG_LANGUAGE_CODE, &str)) {
        g_print ("  language: %s\n", str);
        g_free (str);
      }
      if (gst_tag_list_get_uint (tags, GST_TAG_BITRATE, &rate)) {
        g_print ("  bitrate: %d\n", rate);
      }
      gst_tag_list_free (tags);
    }
  }

  g_print ("\n");
  for (i = 0; i < data->n_text; i++) {
    tags = NULL;
    /* Retrieve the stream's subtitle tags */
    g_signal_emit_by_name (data->playbin, "get-text-tags", i, &tags);
    if (tags) {
      g_print ("subtitle stream %d:\n", i);
      if (gst_tag_list_get_string (tags, GST_TAG_LANGUAGE_CODE, &str)) {
        g_print ("  language: %s\n", str);
        g_free (str);
      }
      gst_tag_list_free (tags);
    }
  }

  g_object_get (data->playbin, "current-video", &data->current_video, NULL);
  g_object_get (data->playbin, "current-audio", &data->current_audio, NULL);
  g_object_get (data->playbin, "current-text", &data->current_text, NULL);

  g_print ("\n");
  g_print ("Currently playing video stream %d, audio stream %d and text stream %d\n",
    data->current_video, data->current_audio, data->current_text);
  g_print ("Type any number and hit ENTER to select a different audio stream\n");
}

/* Process messages from GStreamer */
static gboolean handle_message (GstBus *bus, GstMessage *msg, CustomData *data) {
  GError *err;
  gchar *debug_info;

  printf(" ----------\n");

  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_ERROR:
      gst_message_parse_error (msg, &err, &debug_info);
      g_printerr ("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
      g_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
      g_clear_error (&err);
      g_free (debug_info);
      g_main_loop_quit (data->main_loop);
      break;
    case GST_MESSAGE_EOS:
      g_print ("End-Of-Stream reached.\n");
      g_main_loop_quit (data->main_loop);
      break;
    case GST_MESSAGE_STATE_CHANGED: {
      GstState old_state, new_state, pending_state;
      gst_message_parse_state_changed (msg, &old_state, &new_state, &pending_state);
      if (GST_MESSAGE_SRC (msg) == GST_OBJECT (data->playbin)) {
        if (new_state == GST_STATE_PLAYING) {
          /* Once we are in the playing state, analyze the streams */
          analyze_streams (data);
        }
      }
    } break;
  }

  /* We want to keep receiving messages */
  return TRUE;
}

/* Process keyboard input */
static gboolean handle_keyboard (GIOChannel *source, GIOCondition cond, CustomData *data) {
  gchar *str = NULL;

  if (g_io_channel_read_line (source, &str, NULL, NULL, NULL) == G_IO_STATUS_NORMAL) {
    g_print("%s\n", str);
    int index = g_ascii_strtoull (str, NULL, 0);
    if (index < 0 || index >= data->n_audio) {
      g_printerr ("Index out of bounds\n");
    } else {
      /* If the input was a valid audio stream index, set the current audio stream */
      g_print ("Setting current audio stream to %d\n", index);
      g_object_set (data->playbin, "current-audio", index, NULL);
    }
  }
  g_free (str);
  return TRUE;
}





 
//template <class T>
static int AindexOf(vector<int> *d, int t, int from)
{
//    if (from < 0)
//        from = qMax((int)(from + d->size()), 0);
    if (from < d->size()) {
        int idx=0;
		for (vector<int>::iterator it = d->begin(); it != d->end(); it++, idx++)        
            if (*it == t)
                return idx;
    }
    return -1;
}

template <typename T>
int TindexOf(vector<T> *d, T t, int from)
{
//    if (from < 0)
//        from = qMax((int)(from + d->size()), 0);
    int idx=0;
    for (typename vector<T>::iterator it = d->begin(); it != d->end(); it++, idx++)        
        if (*it == t)
            return idx;
        
    return -1;
}
template <class Iterator>
typename std::iterator_traits<Iterator>::value_type sum(Iterator first, Iterator last)
{
    using U = typename std::iterator_traits<Iterator>::value_type;
    U sum = U{};
    for (auto it = first; it != last; ++it) {
        sum += *it;
    }
    return sum;
}
#if 1
#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

#endif




//#include<bits/stdc++.h> 
#include <algorithm>

void test_vector()
{
    vector<int> v;
    v.push_back(2);
    v.push_back(3);
    v.push_back(1);
    v.push_back(2);
    int elem = 1;
    printf("indexOf elem (%d) : %d\n", elem , TindexOf(&v, elem , 0));
    elem =2;
    printf("indexOf elem (%d) : %d\n", elem , AindexOf(&v, elem , 0));

    elem =3;
    std::vector<int>::iterator it; 
    it = std::find(v.begin(), v.end(), elem);
    printf("indexOf elem (%d) : %d\n", elem , it - v.begin());

    printf("indexOf elem (%d) : %d\n", elem , indexOf(v , elem));

    printf("sum : %d\n", sum(v.begin(), v.end()));


    list<float> fl;
    fl.push_back(1.0);
    fl.push_back(2.0);

    printf("sum : %f\n", sum(fl.begin(), fl.end()));
    
}
    








void test_map()
{
    map<int, int> m;

    m.insert(pair<int, int>(5, 100));    // 임의 pair insert()
    m.insert(pair<int, int>(3, 100));

    pair<int, int> p(9, 50);        
    m.insert(p);                // pair 객체 생성 insert()

    m[11] = 200;            // key/value 삽입
    m[12] = 200;
    m[13] = 40;

    map<int, int>::iterator iter;
    for (iter = m.begin(); iter != m.end(); ++iter)
        cout << "(" << (*iter).first << "," << (*iter).second << ")" << " ";
    cout << endl;


    m[13] = 140;        // key/value 갱신

    // -> 연산자 호출
    for (iter = m.begin(); iter != m.end(); ++iter)
        cout << "(" << iter->first << "," << iter->second << ")" << " ";
    cout << endl;



    pair<map<int, int>::iterator, bool > pr;

    // insert 결과 성공
    pr = m.insert(pair<int, int>(10, 30));
    if (true == pr.second)
        cout << "key : " << pr.first->first << ", value : " << pr.first->second << " 저장 완료!" << endl;
    else
        cout << "key 10가 이미 m에 있습니다." << endl;

    for (iter = m.begin(); iter != m.end(); ++iter)
        cout << "(" << iter->first << "," << iter->second << ")" << " ";
    cout << endl;

    // insert 결과 실패 (중복 key 삽입)
    pr = m.insert(pair<int, int>(10, 40));
    if (true == pr.second)
        cout << "key : " << pr.first->first << ", value : " << pr.first->second << " 저장 완료!" << endl;
    else
        cout << "key 10가 이미 m에 있습니다." << endl;

    
}


void test_filter(gpointer user_data)
{
    map<string, string> *data = reinterpret_cast<map<string, string>* >(user_data);

    //GstSyncMessageFilter *syncFilter = qobject_cast<QGstreamerSyncMessageFilter*>(filter);

}

void test_filter_main()
{
    map<string, string> data;
    test_filter(&data);

    
}

#define TEST_CNT 3
#include "osa_sem.h"
#include "osa_thread.h"


#include <thread>
#include <chrono>
#include <queue>

class busMsg{

private:
    OSA_SemHndl sem;

public :
    busMsg(){
        OSA_semCreate(&sem, 1, 0);
    }
    ~busMsg(){
        OSA_semDelete(&sem);
    }

    void signal(){
        OSA_semSignal(&sem);
    }
    
    bool runflag = true;
    void run(){
        int cnt = 0;
        while(runflag){
            //cout << "test:" << cnt++;
            OSA_semWait(&sem, OSA_TIMEOUT_FOREVER);
            
            printf("test:%d\n", cnt++);
            //std::this_thread::sleep_for(1);
            
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));

            if(cnt>TEST_CNT) runflag = false;
        }
    }
};



typedef struct {
    OSA_ThrHndl wrThrHndl;
    OSA_SemHndl wrSem;

    bool runflag;

    busMsg *pbus;
}MyThread;


MyThread myThread;

void *run_thread(void *pParam){
    MyThread *p = (MyThread *)pParam;
    int cnt = 0;
    while(p->runflag){
        //cout << "test:" << cnt++;
        
        printf("run_thread: test:%d\n", cnt++);
        //std::this_thread::sleep_for(1);

        if(p->pbus)
            p->pbus->signal();

        
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        if(cnt>TEST_CNT) p->runflag = false;
    }
}

void test_my_thread()
{
    Int32 status;


    myThread.runflag = true;

    status = OSA_semCreate(&myThread.wrSem, 1, 0);
    OSA_assert(status==OSA_SOK);


    status = OSA_thrCreate(
        &myThread.wrThrHndl,
        run_thread,
        OSA_THR_PRI_DEFAULT,
        0,
        &myThread
        );

    OSA_assert(status==OSA_SOK);

    printf(" ... done.\n");

}




    
char *test_strings[] = {
    "A", "B" };
void test_string()
{
    
    string a("unknown");

    a= test_strings[0];

    a += std::to_string(10);

    cout << a;

    // parseModeline
    

    cout << endl;

    char *teststr="EGL_KHR_image EGL_KHR_image_base EGL_KHR_image_pixmap EGL_KHR_gl_texture_2D_image EGL_KHR_gl_texture_cubemap_image EGL_KHR_gl_renderbuffer_image EGL_KHR_reusable_sync EGL_KHR_fence_sync EGL_KHR_lock_surface EGL_KHR_lock_surface2 EGL_EXT_create_context_robustness EGL_ANDROID_blob_cache EGL_KHR_create_context EGL_KHR_partial_update EGL_KHR_create_context_no_error";

    string str(teststr);  
    cout << "strfind: " << str.find("EGL_KHR_image_base") << endl;
    cout << "strfind: " << str.find("xxx") << endl;
        
}


class Point
{
private:
    double m_x, m_y, m_z;
 
public:
    Point(double x=0.0, double y=0.0, double z=0.0): m_x(x), m_y(y), m_z(z)
    {
    }
 
    friend std::ostream& operator<< (std::ostream &out, const Point &point);
};

std::ostream& operator<< (std::ostream &out, const Point &point)
{
    // Since operator<< is a friend of the Point class, we can access Point's members directly.
    out << "Point(" << point.m_x << ", " << point.m_y << ", " << point.m_z << ")";
 
    return out;
}


std::ostream& operator<< (std::ostream &out, const QSize &size)
{
    // Since operator<< is a friend of the Point class, we can access Point's members directly.
    out << "size(" << size.width()<< ", " << size.height()<< ")";
 
    return out;
}


class Rect
{
private:
    int m_x, m_y;
public :
    Rect(int x, int y):m_x(x), m_y(y){}   
    friend std::ostream& operator<< (std::ostream &out, const Rect &rect);
};
 
std::ostream& operator<< (std::ostream &out, const Rect &rect)
{
    // Since operator<< is a friend of the Point class, we can access Point's members directly.
    out << "rect(" << rect.m_x << ", " << rect.m_y << ", " << ")";
 
    return out;
}

 
int test_overide_operation_main()
{
    Point point1(2.0, 3.5, 4.0);
    Point point2(6.0, 7.5, 8.0);

    std::cout << point1 << " " << point2 << '\n';

    vector<Point> points;
    points.push_back(point1);
    points.push_back(point2);


    Rect rect(10,11);
    std::cout << points[0] << rect;
 

 
    return 0;
}



#include "eglfsintegration.h"

#if 0

#undef EGL_KHR_platform_gbm
 #include <stdlib.h>
    #include <string.h>

    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>

    #include <EGL/egl.h>
    #include <gbm.h>

    struct my_display {
        struct gbm_device *gbm;
        EGLDisplay egl;
    };

    struct my_config {
        struct my_display dpy;
        EGLConfig egl;
    };

    struct my_window {
        struct my_config config;
        struct gbm_surface *gbm;
        EGLSurface egl;
    };

    static void
    check_extensions(void)
    {
    #ifdef EGL_KHR_platform_gbm
        const char *client_extensions = eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS);

        if (!client_extensions) {
            // No client extensions string available
            abort();
        }
        if (!strstr(client_extensions, "EGL_KHR_platform_gbm")) {
            abort();
        }
    #endif
    }

    static struct my_display
    get_display(void)
    {
        struct my_display dpy;

        int fd = open("/dev/dri/card0", O_RDWR | FD_CLOEXEC);
        if (fd < 0) {
            abort();
        }

        dpy.gbm = gbm_create_device(fd);
        if (!dpy.gbm) {
            abort();
        }


    #ifdef EGL_KHR_platform_gbm
        dpy.egl = eglGetPlatformDisplay(EGL_PLATFORM_GBM_KHR, dpy.gbm, NULL);
    #else
        dpy.egl = eglGetDisplay(dpy.gbm);
    #endif

        if (dpy.egl == EGL_NO_DISPLAY) {
            abort();
        }

        EGLint major, minor;
        if (!eglInitialize(dpy.egl, &major, &minor)) {
            abort();
        }

        return dpy;
    }

    static struct my_config
    get_config(struct my_display dpy)
    {
        struct my_config config = {
            .dpy = dpy,
        };

        EGLint egl_config_attribs[] = {
            EGL_BUFFER_SIZE,        32,
            EGL_DEPTH_SIZE,         EGL_DONT_CARE,
            EGL_STENCIL_SIZE,       EGL_DONT_CARE,
            EGL_RENDERABLE_TYPE,    EGL_OPENGL_ES2_BIT,
            EGL_SURFACE_TYPE,       EGL_WINDOW_BIT,
            EGL_NONE,
        };

        EGLint num_configs;
        if (!eglGetConfigs(dpy.egl, NULL, 0, &num_configs)) {
            abort();
        }

        EGLConfig *configs = (EGLConfig *)malloc(num_configs * sizeof(EGLConfig));
        if (!eglChooseConfig(dpy.egl, egl_config_attribs,
                             configs, num_configs, &num_configs)) {
            abort();
        }
        if (num_configs == 0) {
            abort();
        }

        // Find a config whose native visual ID is the desired GBM format.
        for (int i = 0; i < num_configs; ++i) {
            EGLint gbm_format;

            if (!eglGetConfigAttrib(dpy.egl, configs[i],
                                    EGL_NATIVE_VISUAL_ID, &gbm_format)) {
                abort();
            }

            if (gbm_format == GBM_FORMAT_XRGB8888) {
                config.egl = configs[i];
                free(configs);
                return config;
            }
        }

        // Failed to find a config with matching GBM format.
        abort();
    }

    static struct my_window
    get_window(struct my_config config)
    {
        struct my_window window = {
            .config = config,
        };

        window.gbm = gbm_surface_create(config.dpy.gbm,
                                        256, 256,
                                        GBM_FORMAT_XRGB8888,
                                        GBM_BO_USE_RENDERING);
        if (!window.gbm) {
            abort();
        }

    #ifdef EGL_KHR_platform_gbm
        window.egl = eglCreatePlatformWindowSurface(config.dpy.egl,
                                                    config.egl,
                                                    window.gbm,
                                                    NULL);
    #else
        window.egl = eglCreateWindowSurface(config.dpy.egl,
                                            config.egl,
                                            window.gbm,
                                            NULL);
    #endif

        if (window.egl == EGL_NO_SURFACE) {
            abort();
        }

        return window;
    }

    int
    test_egl(void)
    {
        check_extensions();

        struct my_display dpy = get_display();
        struct my_config config = get_config(dpy);
        struct my_window window = get_window(config);

        return 0;
    }
#endif // test-egl

extern void test_drm_opengles();
extern void test_drm_dumb_kms();
extern void test_kms_pageflip();
extern void renderAll();
extern void draw(uint32_t);

extern void makeCurrent();

    
void test_gst (int *argc, char **argv[])
{
    #ifdef ENABLE_TEST_FB
   test_drm_opengles(); // ok
   // test_drm_dumb_kms();  // failed
   test_kms_pageflip(); //
    #endif

    
    test_string();
    test_vector(); 

    test_overide_operation_main();
        
    //test_map();
    //return;

    gst_init (argc, argv);


  //  test_egl(); goto PLAY_GST;

    // QGstreamerPlayerServicePlugin::create
    /*
        m_session = new QGstreamerPlayerSession(this);
    m_control = new QGstreamerPlayerControl(m_session, this);
    m_metaData = new QGstreamerMetaDataProvider(m_session, this);
    m_streamsControl = new QGstreamerStreamsControl(m_session,this);
    m_availabilityControl = new QGStreamerAvailabilityControl(m_control->resources(), this);

#if QT_CONFIG(mirclient) && defined (__arm__)
    m_videoRenderer = new QGstreamerMirTextureRenderer(this, m_session);
#else
    m_videoRenderer = new QGstreamerVideoRenderer(this);
#endif

    m_videoWindow = new QGstreamerVideoWindow(this);

*/

    eglfs_initialize();
#ifdef TEST_ONLY_MODESET
while(1){
    hPRN(" ----------- just mode set\n");
    OSA_waitMsecs(10000);
}
#endif

PLAY_GST:
    GstPlayerSession player_session;

    GstVideoWindow videoWindow;

   // If the GStreamer video sink is not available, don't provide the video window control since
    // it won't work anyway.
    #if 1
    if (!videoWindow.videoSink()) {
        printf("================ error .\n");    
        return ;
    }    
    #endif
    


    player_session.setVideoRenderer(&videoWindow);

    //
    videoWindow.setWinId(1);
    
    player_session.updateVideoRenderer();

    hPRN(" ---------- setAspectRatioMode\n");
    videoWindow.setAspectRatioMode( IgnoreAspectRatio); 
                            //KeepAspectRatio );
    //QRect rect(900, 500, 1920/2,1080/2);
    QRect rect(1920/2, 1080/2, 800, 600);
    videoWindow.setDisplayRect(rect);

    //OSA_waitMsecs(5000);
    hPRN(" ----------- done size\n");

#if 0
    // run threads
    busMsg bus_msg;
    std::thread proc_bus_msg_thread(&busMsg::run, &bus_msg);
    myThread.pbus = &bus_msg;
    test_my_thread();

    printf("================ done.\n");
    qWarning() << "test: proc_bus_msg_thread\n";

    if(proc_bus_msg_thread.joinable() == true) {
        printf("wait thread join\n");
        proc_bus_msg_thread.join();
    }

#endif
    // ********************************
    // set media : QGstreamerPlayerControl::setMedia
    
    player_session.showPrerollFrames(false); // do not show prerolled frames until pause() or play() explicitly called

#if 1
    // play
    //player_session.loadFromUri("file:///opt/dragon.mp4");

    // fail (first frame is decoded...) : DEBUG
    player_session.loadFromUri("rtsp://172.16.7.94/v1");

    // ok // gst-launch-1.0 playbin uri=rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov
    //player_session.loadFromUri("rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov");

    // ok
    // hikvision
    //player_session.loadFromUri("rtsp://admin:admin**admin@172.16.10.56:554/Streaming/Channels/101?transportmode=unicast&profile=Profile_1");

    // ok
    // samsung snp-6320RH/ 1080p30, h264
    //player_session.loadFromUri("rtsp://admin:admin**admin@172.16.54.189:554/onvif/profile2/media.smp");

    // ok
    // h265 full hd 확인: wisenet xnp-6330rh 
    //player_session.loadFromUri("rtsp://172.16.99.131:554/onvif/profile3/media.smp");

    //////////// plugin : soup ?? 
    // 
    // ok
    //http://samples.mplayerhq.hu/V-codecs/h264/interlaced_crop.mp4
    //player_session.loadFromUri("http://samples.mplayerhq.hu/V-codecs/h264/interlaced_crop.mp4");
    
    // default fail 
    // ok with souphttpsrc ssl-strict=false 
    //player_session.loadFromUri("https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm");
    //player_session.loadFromUri("https://thumbs.gfycat.com/PettyAmpleBats-mobile.mp4");

    
    // gst-launch-1.0 souphttpsrc ssl-strict=false location=https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm ! decodebin ! videoconvert ! autovideosink
#else
    player_session.stop();


    // ok
    //player_session.loadFromUri("file:///opt/dragon.mp4");

    // need gst1-plugins-base/good : tcp/udp/rtsp/rtp
    player_session.loadFromUri("rtsp://172.16.7.94/v1");

    
    //player_session.loadFromUri("https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm");


    player_session.pause();
    
#define NO_WAIT


#ifndef NO_WAIT
    OSA_waitMsecs(10000);
#endif

    printf(" ======== puase  ... after 5sec\n ");

#ifndef NO_WAIT
    OSA_waitMsecs(3000);
#endif
    player_session.pause();
    player_session.showPrerollFrames(true);
    player_session.seek(0);
#ifndef NO_WAIT    
    OSA_waitMsecs(3000);
#endif
#endif
    player_session.play();

    OSA_waitMsecs(5000);
    
    int cnt =0;

    //makeCurrent();
    while(cnt ++< 10000){
        printf(" ======== renderAll  ... after 5sec\n ");    


                
        //OSA_waitMsecs(5000);
        renderAll();
    }
    
    printf(" ======== exit  ... after 30sec\n ");        
    OSA_waitMsecs(30000);


    
    
}



void test_glib_mainloop_in_gst(int *argc, char **argv[])
{

    GMainLoop *loop;

    loop = g_main_loop_new (NULL, FALSE);

    gst_init (argc, argv);
    eglfs_initialize();

    GstPlayerSession player_session;

    GstVideoWindow videoWindow;

   // If the GStreamer video sink is not available, don't provide the video window control since
    // it won't work anyway.
    #if 1
    if (!videoWindow.videoSink()) {
        printf("================ error .\n");    
        return ;
    }    
    #endif


    player_session.setVideoRenderer(&videoWindow);

    //
    videoWindow.setWinId(1);
    
    player_session.updateVideoRenderer();

    
    videoWindow.setAspectRatioMode( IgnoreAspectRatio) ;//KeepAspectRatio );
    //QRect rect(900, 500, 1920/2,1080/2);
    QRect rect(128, 128, 1920-128*2, 1080-128*2);
    videoWindow.setDisplayRect(rect);



    // ********************************
    // set media : QGstreamerPlayerControl::setMedia
    
    player_session.showPrerollFrames(false); // do not show prerolled frames until pause() or play() explicitly called


    player_session.stop();



    player_session.loadFromUri("file:///opt/dragon.mp4");


    player_session.pause();
    

    player_session.showPrerollFrames(true);
    player_session.seek(0);



    // PLAY 
    
    player_session.play();
    //  - loop until calling g_main_loop_quit (loop);
    g_main_loop_run (loop);



    #if 0
    int cnt =0;

    //makeCurrent();
    while(cnt ++< 10000){
        printf(" ======== renderAll  ... after 5sec\n ");    


                
        //OSA_waitMsecs(5000);
        renderAll();
    }
    #endif
    
    printf(" ======== exit  ... after 30sec\n ");        
    OSA_waitMsecs(30000);    
}

