#ifndef __PLATFORM_WINDOW_H__
#define __PLATFORM_WINDOW_H__

#include "awindow.h"
#include "platformsurface.h"


class PlatformWindow : public PlatformSurface
{
public:
    explicit PlatformWindow(AWindow *window);
    virtual ~PlatformWindow();
    
    AWindow *window() const;

};



#endif //__PLATFORM_WINDOW_H__

