#ifndef __PLATFORM_SCREEN_H__
#define __PLATFORM_SCREEN_H__

#include "test.h"
#include "gstutils.h"

class PlatformScreen
{
public:
    enum SubpixelAntialiasingType { // copied from qfontengine_p.h since we can't include private headers
        Subpixel_None,
        Subpixel_RGB,
        Subpixel_BGR,
        Subpixel_VRGB,
        Subpixel_VBGR
    };

    enum PowerState {
        PowerStateOn,
        PowerStateStandby,
        PowerStateSuspend,
        PowerStateOff
    };

    struct Mode {
        QSize size;
        //qreal refreshRate;
    };

    PlatformScreen();
    virtual ~PlatformScreen();


    virtual QRect geometry() const = 0;

    virtual string name() const { return string(); }


};


#endif //__PLATFORM_SCREEN_H__

