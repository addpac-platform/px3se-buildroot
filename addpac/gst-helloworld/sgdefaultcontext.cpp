// sgdefaultcontext.cpp
#include "sgdefaultcontext.h"
#include "sgdefaultrendercontext.h"

SGDefaultContext::SGDefaultContext()
{
    
}

SGDefaultContext::~SGDefaultContext()
{

}

SGRenderContext *SGDefaultContext::createRenderContext()
{
    return new SGDefaultRenderContext(this);
}

