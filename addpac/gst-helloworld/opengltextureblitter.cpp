// opengltextureblitter.cpp

#include "opengltextureblitter.h"
#include <GLES2/gl2.h>
#include "openglcontext.h"


static const char vertex_shader150[] =
    "#version 150 core\n"
    "in vec3 vertexCoord;"
    "in vec2 textureCoord;"
    "out vec2 uv;"
    "uniform mat4 vertexTransform;"
    "uniform mat3 textureTransform;"
    "void main() {"
    "   uv = (textureTransform * vec3(textureCoord,1.0)).xy;"
    "   gl_Position = vertexTransform * vec4(vertexCoord,1.0);"
    "}";

static const char fragment_shader150[] =
    "#version 150 core\n"
    "in vec2 uv;"
    "out vec4 fragcolor;"
    "uniform sampler2D textureSampler;"
    "uniform bool swizzle;"
    "uniform float opacity;"
    "void main() {"
    "   vec4 tmpFragColor = texture(textureSampler, uv);"
    "   tmpFragColor.a *= opacity;"
    "   fragcolor = swizzle ? tmpFragColor.bgra : tmpFragColor;"
    "}";

static const char vertex_shader[] =
    "attribute highp vec3 vertexCoord;"
    "attribute highp vec2 textureCoord;"
    "varying highp vec2 uv;"
    "uniform highp mat4 vertexTransform;"
    "uniform highp mat3 textureTransform;"
    "void main() {"
    "   uv = (textureTransform * vec3(textureCoord,1.0)).xy;"
    "   gl_Position = vertexTransform * vec4(vertexCoord,1.0);"
    "}";

static const char fragment_shader[] =
    "varying highp vec2 uv;"
    "uniform sampler2D textureSampler;"
    "uniform bool swizzle;"
    "uniform highp float opacity;"
    "void main() {"
    "   highp vec4 tmpFragColor = texture2D(textureSampler,uv);"
    "   tmpFragColor.a *= opacity;"
    "   gl_FragColor = swizzle ? tmpFragColor.bgra : tmpFragColor;"
    "}";

static const char fragment_shader_external_oes[] =
    "#extension GL_OES_EGL_image_external : require\n"
    "varying highp vec2 uv;"
    "uniform samplerExternalOES textureSampler;\n"
    "uniform bool swizzle;"
    "uniform highp float opacity;"
    "void main() {"
    "   highp vec4 tmpFragColor = texture2D(textureSampler, uv);"
    "   tmpFragColor.a *= opacity;"
    "   gl_FragColor = swizzle ? tmpFragColor.bgra : tmpFragColor;"
    "}";

static const GLfloat vertex_buffer_data[] = {
        -1,-1, 0,
        -1, 1, 0,
         1,-1, 0,
        -1, 1, 0,
         1,-1, 0,
         1, 1, 0
};

static const GLfloat texture_buffer_data[] = {
        0, 0,
        0, 1,
        1, 0,
        0, 1,
        1, 0,
        1, 1
};

class TextureBinder
{
public:
    TextureBinder(GLenum target, GLuint textureId) : m_target(target)
    {
        //OpenGLContext::currentContext()->functions()->
        glBindTexture(m_target, textureId);
    }
    ~TextureBinder()
    {
        //OpenGLContext::currentContext()->functions()->
        glBindTexture(m_target, 0);
    }

private:
    GLenum m_target;
};

class OpenGLTextureBlitterPrivate
{
public:
    enum TextureMatrixUniform {
        User,
        Identity,
        IdentityFlipped
    };

    enum ProgramIndex {
        TEXTURE_2D,
        TEXTURE_EXTERNAL_OES
    };

    OpenGLTextureBlitterPrivate() :
        swizzle(false),
        opacity(1.0f),
        //vao(new QOpenGLVertexArrayObject),
        currentTarget(TEXTURE_2D)
    { }

    bool buildProgram(ProgramIndex idx, const char *vs, const char *fs);

   // void blit(GLuint texture, const QMatrix4x4 &vertexTransform, const QMatrix3x3 &textureTransform);
//    void blit(GLuint texture, const QMatrix4x4 &vertexTransform, QOpenGLTextureBlitter::Origin origin);

//    void prepareProgram(const QMatrix4x4 &vertexTransform);

//    QOpenGLBuffer vertexBuffer;
//    QOpenGLBuffer textureBuffer;
    struct Program {
        Program() :
            vertexCoordAttribPos(0),
            vertexTransformUniformPos(0),
            textureCoordAttribPos(0),
            textureTransformUniformPos(0),
            swizzleUniformPos(0),
            opacityUniformPos(0),
            swizzle(false),
            opacity(0.0f),
            textureMatrixUniformState(User)
        { }
        //QScopedPointer<QOpenGLShaderProgram> glProgram;
        GLuint vertexCoordAttribPos;
        GLuint vertexTransformUniformPos;
        GLuint textureCoordAttribPos;
        GLuint textureTransformUniformPos;
        GLuint swizzleUniformPos;
        GLuint opacityUniformPos;
        bool swizzle;
        float opacity;
        TextureMatrixUniform textureMatrixUniformState;
    } programs[2];
    bool swizzle;
    float opacity;
    //QScopedPointer<QOpenGLVertexArrayObject> vao;
    GLenum currentTarget;
};



OpenGLTextureBlitter::OpenGLTextureBlitter()
    : d(new OpenGLTextureBlitterPrivate)
{
    
}


OpenGLTextureBlitter::~OpenGLTextureBlitter()
{
   // destroy();
}


bool OpenGLTextureBlitter::create()
{
    hDBG("not impleteded..");
#if 0
    
    OpenGLContext *currentContext = OpenGLContext::currentContext();
    if (!currentContext)
        return false;


    if (d->programs[OpenGLTextureBlitterPrivate::TEXTURE_2D].glProgram)
        return true;

    SurfaceFormat format = currentContext->format();
    if (format.profile() == SurfaceFormat::CoreProfile && format.version() >= qMakePair(3,2)) {
        if (!d->buildProgram(OpenGLTextureBlitterPrivate::TEXTURE_2D, vertex_shader150, fragment_shader150))
            return false;
    } else {
        if (!d->buildProgram(OpenGLTextureBlitterPrivate::TEXTURE_2D, vertex_shader, fragment_shader))
            return false;
        if (supportsExternalOESTarget())
            if (!d->buildProgram(OpenGLTextureBlitterPrivate::TEXTURE_EXTERNAL_OES, vertex_shader, fragment_shader_external_oes))
                return false;
    }

    // Create and bind the VAO, if supported.
    OpenGLVertexArrayObject::Binder vaoBinder(d->vao.data());

    d->vertexBuffer.create();
    d->vertexBuffer.bind();
    d->vertexBuffer.allocate(vertex_buffer_data, sizeof(vertex_buffer_data));
    d->vertexBuffer.release();

    d->textureBuffer.create();
    d->textureBuffer.bind();
    d->textureBuffer.allocate(texture_buffer_data, sizeof(texture_buffer_data));
    d->textureBuffer.release();
#endif    
    return true;
}

