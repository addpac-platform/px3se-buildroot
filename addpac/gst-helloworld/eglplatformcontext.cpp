#include <GLES2/gl2.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "eglplatformcontext.h"




#define TODO_LATER
EGLPlatformContext::EGLPlatformContext(
    const SurfaceFormat &format, 
    PlatformOpenGLContext *share, 
    EGLDisplay display, EGLConfig *config, const EGLNativeContext &nativeHandle, int flags)
    : m_eglDisplay(display)
    , m_swapInterval(-1)
    , m_swapIntervalEnvChecked(false)
    , m_swapIntervalFromEnv(-1)
    , m_flags(flags)
{
    if (nativeHandle.isNull()) {
        m_eglConfig = config ? *config : q_configFromGLFormat(display, format);
        m_ownsContext = true;
        init(format, share);
    } else {
        m_ownsContext = false;
        adopt(nativeHandle, share);
    }
}

EGLPlatformContext::~EGLPlatformContext()
{
}


#define append push_back
#define qWarning hWARN
#define qMyDbg  hDBG

void EGLPlatformContext::init(const SurfaceFormat &format, PlatformOpenGLContext *share)
{
    hDBG("");
    
    m_format = q_glFormatFromConfig(m_eglDisplay, m_eglConfig);
    // m_format now has the renderableType() resolved (it cannot be Default anymore)
    // but does not yet contain version, profile, options.
    m_shareContext = share ? static_cast<EGLPlatformContext *>(share)->m_eglContext : 0;

    vector<EGLint> contextAttrs;


    contextAttrs.append(EGL_CONTEXT_CLIENT_VERSION);
    contextAttrs.append(format.majorVersion);
    const bool hasKHRCreateContext = q_hasEglExtension(m_eglDisplay, "EGL_KHR_create_context");

    if (hasKHRCreateContext) {
        contextAttrs.append(EGL_CONTEXT_MINOR_VERSION_KHR);
        contextAttrs.append(format.minorVersion);
        int flags = 0;
        
        
        // The debug bit is supported both for OpenGL and OpenGL ES.
        //if (format.testOption(SurfaceFormat::DebugContext))
        //    flags |= EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR;
        
        #if (EGL_MAJOR_VER>2)                 
        // The fwdcompat bit is only for OpenGL 3.0+.
        if (m_format.renderableType == SurfaceFormat::OpenGL
            && format.majorVersion >= 3
            && !format.testOption(SurfaceFormat::DeprecatedFunctions))
            flags |= EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE_BIT_KHR;
        #endif
        
        if (flags) {
            contextAttrs.append(EGL_CONTEXT_FLAGS_KHR);
            contextAttrs.append(flags);
        }
        // Profiles are OpenGL only and mandatory in 3.2+. The value is silently ignored for < 3.2.
        if (m_format.renderableType == SurfaceFormat::OpenGL) {
            contextAttrs.append(EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR);
            contextAttrs.append(format.profile == SurfaceFormat::CoreProfile
                                ? EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR
                                : EGL_CONTEXT_OPENGL_COMPATIBILITY_PROFILE_BIT_KHR);
        }
    }

    // Special Options for OpenVG surfaces
    if (m_format.renderableType == SurfaceFormat::OpenVG) {
        contextAttrs.append(EGL_ALPHA_MASK_SIZE);
        contextAttrs.append(8);
    }

    contextAttrs.append(EGL_NONE);
    m_contextAttrs = contextAttrs;

    switch (m_format.renderableType) {
    case SurfaceFormat::OpenVG:
        m_api = EGL_OPENVG_API;
        break;
#ifdef EGL_VERSION_1_4
    case SurfaceFormat::OpenGL:
        m_api = EGL_OPENGL_API;
        break;
#endif // EGL_VERSION_1_4
    default:
        m_api = EGL_OPENGL_ES_API;
        break;
    }

    eglBindAPI(m_api);
    m_eglContext = eglCreateContext(m_eglDisplay, m_eglConfig, m_shareContext, contextAttrs.data());
    if (m_eglContext == EGL_NO_CONTEXT && m_shareContext != EGL_NO_CONTEXT) {
        m_shareContext = 0;
        m_eglContext = eglCreateContext(m_eglDisplay, m_eglConfig, 0, contextAttrs.data());
    }

    if (m_eglContext == EGL_NO_CONTEXT) {
        hWARN("QEGLPlatformContext: Failed to create context: %x", eglGetError());
        return;
    }

    //static const bool printConfig = qEnvironmentVariableIntValue("QT_QPA_EGLFS_DEBUG");
    //if (printConfig) 
    {
        qDebug() << "Created context for format" << format << "with config:\n";
        q_printEglConfig(m_eglDisplay, m_eglConfig);
    }
    // Cannot just call updateFormatFromGL() since it relies on virtuals. Defer it to initialize().

}


void EGLPlatformContext::adopt(const EGLNativeContext &handle, PlatformOpenGLContext *share)
{

    EGLContext context = handle.context();
    if (!context) {
        qWarning("QEGLPlatformContext: No EGLContext given");
        return;
    }

    // A context belonging to a given EGLDisplay cannot be used with another one.
    if (handle.display() != m_eglDisplay) {
        qWarning("QEGLPlatformContext: Cannot adopt context from different display");
        return;
    }

    // Figure out the EGLConfig.
    EGLint value = 0;
    eglQueryContext(m_eglDisplay, context, EGL_CONFIG_ID, &value);
    EGLint n = 0;
    EGLConfig cfg;
    const EGLint attribs[] = { EGL_CONFIG_ID, value, EGL_NONE };
    if (eglChooseConfig(m_eglDisplay, attribs, &cfg, 1, &n) && n == 1) {
        m_eglConfig = cfg;
        m_format = q_glFormatFromConfig(m_eglDisplay, m_eglConfig);
    } else {
        qWarning("QEGLPlatformContext: Failed to get framebuffer configuration for context");
    }

    // Fetch client API type.
    value = 0;
    eglQueryContext(m_eglDisplay, context, EGL_CONTEXT_CLIENT_TYPE, &value);
    if (value == EGL_OPENGL_API || value == EGL_OPENGL_ES_API) {
        m_api = value;
        eglBindAPI(m_api);
    } else {
        qWarning("QEGLPlatformContext: Failed to get client API type");
        m_api = EGL_OPENGL_ES_API;
    }

    m_eglContext = context;
    m_shareContext = share ? static_cast<EGLPlatformContext *>(share)->m_eglContext : 0;
    updateFormatFromGL();
    
}

void EGLPlatformContext::initialize()
{
    qMyDbg("");
    
    if (m_eglContext != EGL_NO_CONTEXT)
        updateFormatFromGL();
    qMyDbg("done");
    
}


// Base implementation for pbuffers. Subclasses will handle the specialized cases for
// platforms without pbuffers.
EGLSurface EGLPlatformContext::createTemporaryOffscreenSurface()
{
    qMyDbg("");
    // Make the context current to ensure the GL version query works. This needs a surface too.
    const EGLint pbufferAttributes[] = {
        EGL_WIDTH, 1,
        EGL_HEIGHT, 1,
        EGL_LARGEST_PBUFFER, EGL_FALSE,
        EGL_NONE
    };

    // Cannot just pass m_eglConfig because it may not be suitable for pbuffers. Instead,
    // do what QEGLPbuffer would do: request a config with the same attributes but with
    // PBUFFER_BIT set.
    EGLConfig config = q_configFromGLFormat(m_eglDisplay, m_format, false, EGL_PBUFFER_BIT);

    EGLSurface surf = eglCreatePbufferSurface(m_eglDisplay, config, pbufferAttributes);
    qMyDbg("done");

    return surf;
}

void EGLPlatformContext::destroyTemporaryOffscreenSurface(EGLSurface surface)
{
    eglDestroySurface(m_eglDisplay, surface);
}

void EGLPlatformContext::runGLChecks()
{
    // Nothing to do here, subclasses may override in order to perform OpenGL
    // queries needing a context.
}

void EGLPlatformContext::updateFormatFromGL()
{
      // Have to save & restore to prevent QOpenGLContext::currentContext() from becoming
    // inconsistent after QOpenGLContext::create().
    EGLDisplay prevDisplay = eglGetCurrentDisplay();
    if (prevDisplay == EGL_NO_DISPLAY) // when no context is current
        prevDisplay = m_eglDisplay;
    EGLContext prevContext = eglGetCurrentContext();
    EGLSurface prevSurfaceDraw = eglGetCurrentSurface(EGL_DRAW);
    EGLSurface prevSurfaceRead = eglGetCurrentSurface(EGL_READ);

    // Rely on the surfaceless extension, if available. This is beneficial since we can
    // avoid creating an extra pbuffer surface which is apparently troublesome with some
    // drivers (Mesa) when certain attributes are present (multisampling).
    EGLSurface tempSurface = EGL_NO_SURFACE;
    EGLContext tempContext = EGL_NO_CONTEXT;
    if (m_flags == NoSurfaceless || !q_hasEglExtension(m_eglDisplay, "EGL_KHR_surfaceless_context")){
        hDBG("createTemporaryOffscreenSurface");
        tempSurface = createTemporaryOffscreenSurface();
    }

    EGLBoolean ok = eglMakeCurrent(m_eglDisplay, tempSurface, tempSurface, m_eglContext);
    if (!ok) {
        EGLConfig config = q_configFromGLFormat(m_eglDisplay, m_format, false, EGL_PBUFFER_BIT);
        tempContext = eglCreateContext(m_eglDisplay, config, 0, m_contextAttrs.data());
        if (tempContext != EGL_NO_CONTEXT)
            ok = eglMakeCurrent(m_eglDisplay, tempSurface, tempSurface, tempContext);
    }
    if (ok) {
        if (m_format.renderableType == SurfaceFormat::OpenGL
            || m_format.renderableType == SurfaceFormat::OpenGLES) {
            const GLubyte *s = glGetString(GL_VERSION);
            if (s) {
                hDBG("gl version: %s", s);
#if (EGL_MAJOR_VER!=2)                
                QByteArray version = QByteArray(reinterpret_cast<const char *>(s));
                int major, minor;
                if (PlatformOpenGLContext::parseOpenGLVersion(version, major, minor)) {
#ifdef Q_OS_ANDROID
                    // Some Android 4.2.2 devices report OpenGL ES 3.0 without the functions being available.
                    static int apiLevel = QtAndroidPrivate::androidSdkVersion();
                    if (apiLevel <= 17 && major >= 3) {
                        major = 2;
                        minor = 0;
                    }
#endif
                    m_format.setMajorVersion(major);
                    m_format.setMinorVersion(minor);
                    
                }
#endif
                
            }
            m_format.profile = (SurfaceFormat::NoProfile);
            #ifndef TODO_LATER
            m_format.setOptions(SurfaceFormat::FormatOptions());
            #endif
#ifndef SUPPORTED_SURFACE_TYPE_EGLFSv2
            if (m_format.renderableType() == SurfaceFormat::OpenGL) {
                // Check profile and options.
                if (m_format.majorVersion() < 3) {
                    m_format.setOption(QSurfaceFormat::DeprecatedFunctions);
                } else {
                    GLint value = 0;
                    glGetIntegerv(GL_CONTEXT_FLAGS, &value);
                    if (!(value & GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT))
                        m_format.setOption(QSurfaceFormat::DeprecatedFunctions);
                    if (value & GL_CONTEXT_FLAG_DEBUG_BIT)
                        m_format.setOption(QSurfaceFormat::DebugContext);
                    if (m_format.version() >= qMakePair(3, 2)) {
                        value = 0;
                        glGetIntegerv(GL_CONTEXT_PROFILE_MASK, &value);
                        if (value & GL_CONTEXT_CORE_PROFILE_BIT)
                            m_format.setProfile(QSurfaceFormat::CoreProfile);
                        else if (value & GL_CONTEXT_COMPATIBILITY_PROFILE_BIT)
                            m_format.setProfile(QSurfaceFormat::CompatibilityProfile);
                    }
                }
            }
#endif
            
        }
        runGLChecks();
        eglMakeCurrent(prevDisplay, prevSurfaceDraw, prevSurfaceRead, prevContext);
    }
    else {
        qWarning("QEGLPlatformContext: "
            "Failed to make temporary surface current, format not updated (%x)", eglGetError());
    }
    
    if (tempSurface != EGL_NO_SURFACE)
        destroyTemporaryOffscreenSurface(tempSurface);
    if (tempContext != EGL_NO_CONTEXT)
        eglDestroyContext(m_eglDisplay, tempContext);
  
}



bool EGLPlatformContext::makeCurrent(PlatformSurface *surface)
{
    eglBindAPI(m_api);

    EGLSurface eglSurface = eglSurfaceForPlatformSurface(surface);
    if (eglGetCurrentContext() == m_eglContext &&
        eglGetCurrentDisplay() == m_eglDisplay &&
        eglGetCurrentSurface(EGL_READ) == eglSurface &&
        eglGetCurrentSurface(EGL_DRAW) == eglSurface) {
        hDBG(" error ??");
        return true;
    }

    const bool ok = eglMakeCurrent(m_eglDisplay, eglSurface, eglSurface, m_eglContext);
    if (ok) {

        // read env.

        m_swapInterval = 1;
        if (eglSurface != EGL_NO_SURFACE) // skip if using surfaceless context
        {
            qMyDbg("m_swapInterval:%d", m_swapInterval);
            eglSwapInterval(eglDisplay(), m_swapInterval);
        }
    }else{
        hERR("EGLPlatformContext: eglMakeCurrent failed: %x", eglGetError());
        assert(0); // not yet
    }        
    
    return ok;
}




void EGLPlatformContext::swapBuffers(PlatformSurface *surface)
{
    hDBG("");
    
    eglBindAPI(m_api);
    EGLSurface eglSurface = eglSurfaceForPlatformSurface(surface);
    printf("eglSurface:%p", eglSurface);
    if (eglSurface != EGL_NO_SURFACE) { // skip if using surfaceless context
        bool ok = eglSwapBuffers(m_eglDisplay, eglSurface);
        if (!ok)
            hWARN("QEGLPlatformContext: eglSwapBuffers failed: %x", eglGetError());
    }
    hDBG("done.");
    
}


SurfaceFormat EGLPlatformContext::format() const
{
    return m_format;
}

EGLContext EGLPlatformContext::eglContext() const
{
    return m_eglContext;
}

EGLDisplay EGLPlatformContext::eglDisplay() const
{
    return m_eglDisplay;
}

EGLConfig EGLPlatformContext::eglConfig() const
{
    return m_eglConfig;
}

