
#ifndef __ADDPAC_GST_PLAYER_SESSION_H__
#define __ADDPAC_GST_PLAYER_SESSION_H__

#include <gst/gst.h>
#include <gst/gstvalue.h>
#include <gst/base/gstbasesrc.h>

#include "gstutils.h"
#include "gstbushelper.h"

#include "gstvideowindow.h"


typedef enum {
  GST_AUTOPLUG_SELECT_TRY,
  GST_AUTOPLUG_SELECT_EXPOSE,
  GST_AUTOPLUG_SELECT_SKIP
} GstAutoplugSelectResult;


typedef enum 
{
    StoppedState,
    PlayingState,
    PausedState
}GstPlayState;


class GstPlayerSession 
    #if 1 //def ADD_BUS_HELPER
    : public GstBusMessageFilter
    #endif
{

public:
    GstPlayerSession();
    virtual ~GstPlayerSession();
#if 1//def ADD_BUS_HELPER
    bool processBusMessage(const apGstMessage &message) override;
#endif

    void setVideoRenderer(GstVideoWindow *renderer);
    void updateVideoRenderer();


    bool play();
    bool pause();
    void stop();
    void showPrerollFrames(bool enabled);
    void loadFromUri(const string &str);
    bool seek(long long ms);
    
private:

    void finishVideoOutputChange();

    
    static void playbinNotifySource(GObject *o, GParamSpec *p, gpointer d);
    static void handleElementAdded(GstBin *bin, GstElement *element, GstPlayerSession *session);
    static void handleStreamsChange(GstBin *bin, gpointer user_data);

    void flushVideoProbes();
    void removeVideoBufferProbe();
    void addVideoBufferProbe();
    void resumeVideoProbes();


    GstPlayState m_state;
    GstPlayState m_pendingState;
    GstBusHelper *m_busHelper;

    
    GstElement* m_playbin;

    GstElement* m_videoSink;

    GstElement* m_videoOutputBin;
    GstElement* m_videoIdentity;
#if !GST_CHECK_VERSION(1,0,0)
    GstElement* m_colorSpace;
    bool m_usingColorspaceElement;
#endif
    GstElement* m_pendingVideoSink;
    GstElement* m_nullVideoSink;

    GstElement* m_audioSink;
    GstElement* m_volumeElement;

    GstBus* m_bus;
    
    GstVideoWindow *m_videoOutput;
    GstVideoRendererInterface *m_renderer;


    void setSeekable(bool);
    void getStreamsInfo();
    
    enum StreamType { UnknownStream, VideoStream, AudioStream, SubPictureStream, DataStream };
    map<StreamType, int> m_playbin2StreamOffset;
    list<StreamType> m_streamTypes;



    double m_playbackRate;

    bool m_audioAvailable;
    bool m_videoAvailable;
    bool m_seekable;
    
    mutable long long m_lastPosition;
    long long m_duration;


    bool m_displayPrerolledFrame;

    string m_request_str;






    enum SourceType
    {
        UnknownSrc,
        SoupHTTPSrc,
        UDPSrc,
        MMSSrc,
        RTSPSrc,
    };
    SourceType m_sourceType;
    bool m_everPlayed;
    bool m_isLiveSource;

    gulong pad_probe_id;

};


#endif //__ADDPAC_GST_PLAYER_SESSION_H__

