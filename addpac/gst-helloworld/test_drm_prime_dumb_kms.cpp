// This will works on Embedded GPU that implements .gem_prime_mmap like Rockchip ones.
// This will fail on most DRM drivers for GPU with dedicated memory as they tend to NOT implement .gem_prime_mmap.
#include <stdio.h>
#include <libdrm/drm.h>

#include <stdint.h>

#include <sys/mman.h>

#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
// O_CLOEXEC
//#include <asm-generic/fcntl.h>

#include <unistd.h>

#include <sys/ioctl.h>
#include <errno.h>


// rand
#include <stdlib.h>

