// sgdefaultrendercontext.cpp
#include "sgdefaultrendercontext.h"
#include "sgbatchrenderer.h"

SGDefaultRenderContext::SGDefaultRenderContext(SGContext *context)
    : SGRenderContext(context)
    , m_gl(nullptr)
{

}


void SGDefaultRenderContext::initialize(void *context)
{
    OpenGLContext *openglContext = static_cast<OpenGLContext *>(context);

    m_gl = openglContext;

    hDBG("done.");
}



SGRenderer *SGDefaultRenderContext::createRenderer()
{
    
    hDBG("");
    
    return new Renderer(this);    

    //return nullptr;
}


void SGDefaultRenderContext::renderNextFrame(SGRenderer *renderer, unsigned int fboId)
{

    hDBG("");
//    if (m_serializedRender)
//        qsg_framerender_mutex.lock();

    renderer->renderScene(fboId);

//    if (m_serializedRender)
//        qsg_framerender_mutex.unlock();
}


