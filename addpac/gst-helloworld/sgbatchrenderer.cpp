// sgbatchrenderer.cpp

#include "sgbatchrenderer.h"
#include "sgdefaultrendercontext.h"

Renderer::Renderer(SGDefaultRenderContext *ctx)
    : SGRenderer(ctx)
    , m_context(ctx)
#if 0    
    , m_opaqueRenderList(64)
    , m_alphaRenderList(64)
    , m_nextRenderOrder(0)
    , m_partialRebuild(false)
    , m_partialRebuildRoot(0)
    , m_useDepthBuffer(true)
    , m_opaqueBatches(16)
    , m_alphaBatches(16)
    , m_batchPool(16)
    , m_elementsToDelete(64)
    , m_tmpAlphaElements(16)
    , m_tmpOpaqueElements(16)
    , m_rebuild(FullRebuild)
    , m_zRange(0)
    , m_renderOrderRebuildLower(-1)
    , m_renderOrderRebuildUpper(-1)
    , m_currentMaterial(0)
    , m_currentShader(0)
    , m_currentStencilValue(0)
    , m_clipMatrixId(0)
    , m_currentClip(0)
    , m_currentClipType(NoClip)
    , m_vertexUploadPool(256)
#ifdef QSG_SEPARATE_INDEX_BUFFER
    , m_indexUploadPool(64)
#endif
    , m_vao(0)
    , m_visualizeMode(VisualizeNothing)
    #endif 
{

}



Renderer::~Renderer()
{
    #if 0
    if (OpenGLContext::currentContext()) {
        // Clean up batches and buffers
        for (int i=0; i<m_opaqueBatches.size(); ++i) qsg_wipeBatch(m_opaqueBatches.at(i), this);
        for (int i=0; i<m_alphaBatches.size(); ++i) qsg_wipeBatch(m_alphaBatches.at(i), this);
        for (int i=0; i<m_batchPool.size(); ++i) qsg_wipeBatch(m_batchPool.at(i), this);
    }

    for (Node *n : qAsConst(m_nodes))
        m_nodeAllocator.release(n);

    // Remaining elements...
    for (int i=0; i<m_elementsToDelete.size(); ++i) {
        Element *e = m_elementsToDelete.at(i);
        if (e->isRenderNode)
            delete static_cast<RenderNodeElement *>(e);
        else
            m_elementAllocator.release(e);
    }
    #endif
}

void Renderer::render()
{
    hDBG(" TODO : renderer ...");

    // draw opengl ?? batch jobs ?
    // QSGBatchRenderer::Renderer::render

    

}
    
