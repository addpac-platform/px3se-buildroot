#include "ap_global.h"

#include "eglfscontext.h"
#include "openglcontext.h"
#include "eglfsscreen.h"
#include "eglfsintegration.h"
#include "offscreensurface.h"
#include "eglfsoffscreenwindow.h"

AppPrivate::AppPrivate()
{
}


AppPrivate::~AppPrivate()
{
}



void AppPrivate::addScreen(PlatformScreen *s, bool isPrimary){
    //qMyDbg("ps:%p, screen: %p", ps, screen);
    if (isPrimary) {
        screen_list.at(0)=s;
    } else {
        screen_list.push_back(s);
    }

    #if 0
    emit qGuiApp->screenAdded(screen);

    if (isPrimary)
        emit qGuiApp->primaryScreenChanged(screen);
    #endif
    
}


PlatformScreen *AppPrivate::getTopScreen()
{
    if(screen_list.size() >0)
        return screen_list.at(0);
    else
        return NULL;
}



SurfaceFormat AppPrivate::surfaceFormatFor(const SurfaceFormat  &inputFormat)
{
    SurfaceFormat format(inputFormat);
    format.renderableType=(SurfaceFormat::OpenGLES);
    format.swapBehavior=(SurfaceFormat::DoubleBuffer);
    format.redBufferSize=(8);
    format.greenBufferSize=(8);
    format.blueBufferSize=(8);
    format.alphaBufferSize = 8;
    return format;
}



PlatformOpenGLContext *AppPrivate::createPlatformOpenGLContext(OpenGLContext *context)
{

    hDBG("");
    
    EGLDisplay dpy = static_cast<EglFSScreen *>(context->screen())->display();
    assert(dpy);

    PlatformOpenGLContext *share = (PlatformOpenGLContext *)context->shareHandle();
    EGLNativeContext nativeHandle = context->nativeHandle();

    EglFSContext *ctx;
    SurfaceFormat adjustedFormat = surfaceFormatFor(context->format());
    hDBG("%p,%p, %d", nativeHandle.display(), nativeHandle.context(), nativeHandle.isNull());

    
    if (nativeHandle.isNull()) {
        hDBG("");
        EGLConfig config = chooseConfig(dpy, adjustedFormat);
        ctx = new EglFSContext(adjustedFormat, share, dpy, &config, nativeHandle);
    } else {
        hDBG("");
        ctx = new EglFSContext(adjustedFormat, share, dpy, 0, nativeHandle);
    }
    
    hDBG("%p,%p", ctx->eglContext(), dpy);
        //QVariant::fromValue<QEGLNativeContext>(QEGLNativeContext(ctx->eglContext(), dpy));

    context->setNativeHandle(EGLNativeContext(ctx->eglContext(), dpy) );

    return ctx;
}



PlatformOffscreenSurface *AppPrivate::createPlatformOffscreenSurface(OffscreenSurface *surface)
{
    EGLDisplay dpy = static_cast<EglFSScreen *>(surface->screen())->display();
    assert(dpy);

    SurfaceFormat fmt = surfaceFormatFor(surface->requestedFormat());
    #if 0
    if (qt_egl_device_integration()->supportsPBuffers()) {
        QEGLPlatformContext::Flags flags = 0;
        if (!qt_egl_device_integration()->supportsSurfacelessContexts())
            flags |= QEGLPlatformContext::NoSurfaceless;
        return new QEGLPbuffer(dpy, fmt, surface, flags);
    } else
    #endif
    {
        return new EglFSOffscreenWindow(dpy, fmt, surface);
    }    
}

