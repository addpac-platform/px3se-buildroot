// sgbatchrenderer SGBATCHRENDERER
#ifndef __SGBATCHRENDERER_H__
#define __SGBATCHRENDERER_H__

#include "sgrenderer.h"

class SGDefaultRenderContext;

class Renderer : public SGRenderer
{
public:
    Renderer(SGDefaultRenderContext *);
    ~Renderer();

    enum VisualizeMode {
        VisualizeNothing,
        VisualizeBatches,
        VisualizeClipping,
        VisualizeChanges,
        VisualizeOverdraw
    };


protected:
    void render() override;

private:

    enum ClipTypeBit
    {
        NoClip = 0x00,
        ScissorClip = 0x01,
        StencilClip = 0x02
    };    

    enum RebuildFlag {
        BuildRenderListsForTaggedRoots      = 0x0001,
        BuildRenderLists                    = 0x0002,
        BuildBatches                        = 0x0004,
        FullRebuild                         = 0xffff
    };

    SGDefaultRenderContext *m_context;
    

};


#endif // __SGBATCHRENDERER_H__

