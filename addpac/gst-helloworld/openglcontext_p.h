// openglcontext_p OPENGLCONTEXT_P
#ifndef __OPENGLCONTEXT_P_H__
#define __OPENGLCONTEXT_P_H__

#include "test.h"
#include "gstutils.h"
#include <GLES2/gl2.h>


class OpenGLContext;

class OpenGLSharedResourceGuard //: public OpenGLSharedResource
{
public:
    typedef void (*FreeResourceFunc)(GLuint id);
    OpenGLSharedResourceGuard(OpenGLContext *context, GLuint id, FreeResourceFunc func)
        //: OpenGLSharedResource(context->shareGroup())
        : m_id(id)
        , m_func(func)
    {
    }

    GLuint id() const { return m_id; }
#if 0
protected:
    void invalidateResource() override
    {
        m_id = 0;
    }

    void freeResource(QOpenGLContext *context) override;
#endif 

private:
    GLuint m_id;
    FreeResourceFunc m_func;
};





#endif // __OPENGLCONTEXT_P_H__
