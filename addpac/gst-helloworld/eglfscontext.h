#ifndef __EGLFS_CONTEXT_H__
#define __EGLFS_CONTEXT_H__

#include "eglplatformcontext.h"
class PlatformSurface;

class EglFSContext : public EGLPlatformContext
{
public:
    EglFSContext(const SurfaceFormat &format, PlatformOpenGLContext *share, 
        EGLDisplay display, EGLConfig *config, const EGLNativeContext &nativeHandle );

#ifdef TODO_ENABLE_INTERFACE
    EGLSurface eglSurfaceForPlatformSurface(PlatformSurface *surface) override;
    #if 0 //ndef TODO_PART1
    EGLSurface createTemporaryOffscreenSurface() override;
    void destroyTemporaryOffscreenSurface(EGLSurface surface) override;
    void runGLChecks() override;
    void swapBuffers(QPlatformSurface *surface) override;
    #endif
#endif 

private:
    EGLNativeWindowType m_tempWindow;
};

#endif //__EGLFS_CONTEXT_H__
