#ifndef __EGLFS_WINDOW_H__
#define __EGLFS_WINDOW_H__

#include "platformwindow.h"
#include "eglfsintegration.h"
#include "surfaceformat.h"

class PlatformScreen; 
class EglFSScreen;

class EglFSWindow : public PlatformWindow  // public QOpenGLCompositorWindow
{

public:
    EglFSWindow(AWindow *w);
    ~EglFSWindow();

    void create(PlatformScreen *s);

    //EglFSScreen *screen() const;

    void resetSurface();

    EGLSurface surface() const { return m_surface; }


private:
    EglFSScreen *m_screen; 


protected:

    EGLSurface m_surface;
    EGLNativeWindowType m_window;

    EGLConfig m_config;
    SurfaceFormat m_format;


//debug
//public:
//    friend std::ostream& operator<< (std::ostream &out, const EGLConfig &rect);

};

#endif // __EGLFS_WINDOW_H__

