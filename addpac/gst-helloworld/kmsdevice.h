
#ifndef __KMSDEVICE_H__
#define __KMSDEVICE_H__



//#include <qpa/qplatformscreen.h>
//#include <QtCore/QMap>
//#include <QtCore/QVariant>
#include "gstutils.h"
#include "platformscreen.h"

#include <stdint.h>


#include <xf86drm.h>
#include <xf86drmMode.h>

//QT_BEGIN_NAMESPACE


#define DISABLE_CONFIGURE

class KmsDevice;

class KmsScreenConfig
{
public:
    enum VirtualDesktopLayout {
        VirtualDesktopLayoutHorizontal,
        VirtualDesktopLayoutVertical
    };

    KmsScreenConfig();

    string devicePath() const { return m_devicePath; }
    VirtualDesktopLayout virtualDesktopLayout() const { return m_virtualDesktopLayout; }
    bool separateScreens() const { return m_separateScreens; }

#if 0
    bool hwCursor() const { return m_hwCursor; }
    bool supportsPBuffers() const { return m_pbuffers; }

    QMap<QString, QVariantMap> outputSettings() const { return m_outputSettings; }
#endif
private:
    void loadConfig();

    string m_devicePath;
    bool m_hwCursor;
    bool m_separateScreens;
    bool m_pbuffers;
    VirtualDesktopLayout m_virtualDesktopLayout;
    //QMap<QString, QVariantMap> m_outputSettings;
};


struct KmsOutput
{
    string name;
    

    uint32_t connector_id;
    uint32_t crtc_id;
    QSize physical_size;
    int mode; // index of selected mode in list below
    bool mode_set;
    drmModeCrtcPtr saved_crtc;
    vector<drmModeModeInfo> modes;
    int subpixel;
    drmModePropertyPtr dpms_prop;
    bool wants_plane;
    uint32_t plane_id;
    bool plane_set;

    #if 1
    void restoreMode(KmsDevice *device);
    void cleanup(KmsDevice *device);
    PlatformScreen::SubpixelAntialiasingType subpixelAntialiasingTypeHint() const;
    void setPowerState(KmsDevice *device, PlatformScreen::PowerState state);
    #endif
};

class KmsDevice
{
public:
    #if 1
    struct VirtualDesktopInfo {
    VirtualDesktopInfo() : virtualIndex(0), isPrimary(false) { }
        int virtualIndex;
        QPoint virtualPos;
        bool isPrimary;
    };
    #endif
    
    KmsDevice(KmsScreenConfig *screenConfig, const string &path = 0);
    virtual ~KmsDevice();
    virtual bool open() = 0;
    string devicePath() const;
    int fd() const;
    virtual void *nativeDisplay() const = 0;
    void createScreens();

#if 0
    virtual void close() = 0;




    QKmsScreenConfig *screenConfig() const;

protected:
    virtual QPlatformScreen *createScreen(const QKmsOutput &output) = 0;





#endif
protected:
    void setFd(int fd);

    virtual PlatformScreen *createScreen(const KmsOutput &output) = 0;

    PlatformScreen *createScreenForConnector(drmModeResPtr resources,
                                              drmModeConnectorPtr connector,
                                              VirtualDesktopInfo *vinfo);
    drmModePropertyPtr connectorProperty(drmModeConnectorPtr connector, const string &name);
    
    int crtcForConnector(drmModeResPtr resources, drmModeConnectorPtr connector);

    virtual void registerScreen(PlatformScreen *screen,
                                bool isPrimary,
                                const QPoint &virtualPos,
                                const vector<PlatformScreen *> &virtualSiblings) = 0;



    
    KmsScreenConfig *m_screenConfig;
    string m_path;
    int m_dri_fd;

    int m_crtc_allocator;
    unsigned int m_connector_allocator;

};





#endif

