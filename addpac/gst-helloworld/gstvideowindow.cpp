
#include "gstvideowindow.h"

#define TODO_LATER

GstVideoWindow::GstVideoWindow()
    : m_videoOverlay("kmssink") ////qgetenv("QT_GSTREAMER_WINDOW_VIDEOSINK"))
    , m_windowId(0)
{

}

GstVideoWindow::~GstVideoWindow()
{
}

WId GstVideoWindow::winId() const
{

    return m_windowId;
}

void GstVideoWindow::setWinId(WId id)
{
    if (m_windowId == id)
        return;

    WId oldId = m_windowId;
    m_videoOverlay.setWindowHandle(m_windowId = id);

#ifdef TODO_LATER
    niPRN("");
    //m_videoOverlay.updateVideoRenderer();
#else
    
    if (!oldId)
        emit readyChanged(true);

    if (!id)
        emit readyChanged(false);
#endif
}


bool GstVideoWindow::processSyncMessage(const apGstMessage &message)
{ //qMyDbg("");
  return m_videoOverlay.processSyncMessage(message);
  //return false;
}

bool GstVideoWindow::processBusMessage(const apGstMessage &message)
{ //qMyDbg("");
    return m_videoOverlay.processBusMessage(message);
    //return false;
}
