#include "offscreensurface.h"

#include "ap_global.h"

extern AppPrivate app;


OffscreenSurface::OffscreenSurface(AScreen *targetScreen)
    : ASurface(Offscreen)
{
    m_screen = app.getTopScreen();

    //if your applications aborts here, then chances are your creating a QOffscreenSurface before
    //the screen list is populated.
    assert(m_screen);

    #if 0
    connect(d->screen, SIGNAL(destroyed(QObject*)), this, SLOT(screenDestroyed(QObject*)));
    
    if(targetScreen)
        qMyDbg("%d,%d", targetScreen->size().width(), targetScreen->size().height());
    else
        qMyDbg("");
    #endif
    
}


/*!
    Destroys the offscreen surface.
*/
OffscreenSurface::~OffscreenSurface()
{
    //destroy();
}


void OffscreenSurface::create()
{
    platformOffscreenSurface = AppPrivate::createPlatformOffscreenSurface(this);
    assert(platformOffscreenSurface);

    hDBG("platformOffscreenSurface: %p", platformOffscreenSurface);

}


void OffscreenSurface::setScreen(PlatformScreen *newScreen)
{
    #if 0 // TODO: 
    if (!newScreen)
        newScreen = QGuiApplication::primaryScreen();
    if (newScreen != d->screen) {
        const bool wasCreated = d->platformOffscreenSurface != 0 || d->offscreenWindow != 0;
        if (wasCreated)
            destroy();
        if (d->screen)
            disconnect(d->screen, SIGNAL(destroyed(QObject*)), this, SLOT(screenDestroyed(QObject*)));
        d->screen = newScreen;
        if (newScreen) {
            connect(d->screen, SIGNAL(destroyed(QObject*)), this, SLOT(screenDestroyed(QObject*)));
            if (wasCreated)
                create();
        }
        emit screenChanged(newScreen);
    }
        
    #endif
}


PlatformSurface *OffscreenSurface::surfaceHandle() const
{
    return platformOffscreenSurface;
}

