#include "eglfsoffscreenwindow.h"
#include "eglfsintegration.h"
#include "eglconvenience.h"


EglFSOffscreenWindow::EglFSOffscreenWindow(EGLDisplay display, const SurfaceFormat &format, OffscreenSurface *offscreenSurface)
    : PlatformOffscreenSurface(offscreenSurface)
    , m_format(format)
    , m_display(display)
    , m_surface(EGL_NO_SURFACE)
    , m_window(0)
{
    
    //qMyDbg(" createNativeOffscreenWindow ");
    m_window = createNativeOffscreenWindow(format);
    if (!m_window) {
        hWARN("QEglFSOffscreenWindow: Failed to create native window");
        return;
    }
    EGLConfig config = q_configFromGLFormat(m_display, m_format);
    
    m_surface = eglCreateWindowSurface(m_display, config, m_window, 0);
    if (m_surface != EGL_NO_SURFACE)
        m_format = q_glFormatFromConfig(m_display, config);


    hDBG("%p,%p", m_window, m_surface); 

}

EglFSOffscreenWindow::~EglFSOffscreenWindow()
{
    if (m_surface != EGL_NO_SURFACE)
        eglDestroySurface(m_display, m_surface);
    if (m_window)
        destroyNativeWindow(m_window);

}



