#ifndef __EGLCONVENIENCE_H__
#define __EGLCONVENIENCE_H__

#include "test.h"
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "surfaceformat.h"


SurfaceFormat q_glFormatFromConfig(EGLDisplay display, const EGLConfig config, 
    const SurfaceFormat &referenceFormat = SurfaceFormat());
EGLConfig q_configFromGLFormat(EGLDisplay display, const SurfaceFormat &format, 
    bool highestPixelFormat = false, int surfaceType = EGL_WINDOW_BIT);

bool q_hasEglExtension(EGLDisplay display, const char* extensionName);
void q_printEglConfig(EGLDisplay display, EGLConfig config);


class EglConfigChooser
{
public:
    EglConfigChooser(EGLDisplay display);
    virtual ~EglConfigChooser();

    EGLDisplay display() const { return m_display; }

    void setSurfaceType(EGLint surfaceType) { m_surfaceType = surfaceType; }
    EGLint surfaceType() const { return m_surfaceType; }

    void setSurfaceFormat(const SurfaceFormat &format) { m_format = format; }
    SurfaceFormat surfaceFormat() const { return m_format; }

    void setIgnoreColorChannels(bool ignore) { m_ignore = ignore; }
    bool ignoreColorChannels() const { return m_ignore; }

    EGLConfig chooseConfig();

protected:
    virtual bool filterConfig(EGLConfig config) const;

    SurfaceFormat m_format;
    EGLDisplay m_display;
    EGLint m_surfaceType;
    bool m_ignore;

    int m_confAttrRed;
    int m_confAttrGreen;
    int m_confAttrBlue;
    int m_confAttrAlpha;
};



struct EGLNativeContext
{
    EGLNativeContext()
        : m_context(0),
          m_display(0)
    { }

    EGLNativeContext(EGLContext ctx, EGLDisplay dpy)
        : m_context(ctx),
          m_display(dpy)
    { }

    EGLContext context() const { return m_context; }
    EGLDisplay display() const { return m_display; }
    bool isNull() const {return !(m_context || m_display); }
    EGLNativeContext& operator= ( const EGLNativeContext  &r){
        m_context = r.m_context;
        m_display = r.m_display;
        return *this;
    }
    
private:
    EGLContext m_context;
    EGLDisplay m_display;
};



#endif // __EGLCONVENIENCE_H__
