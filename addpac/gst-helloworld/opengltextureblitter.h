// opengltextureblitter.h
#ifndef __OPENGLTEXTUREBLITTER_H__
#define __OPENGLTEXTUREBLITTER_H__


class OpenGLTextureBlitterPrivate;

class OpenGLTextureBlitter
{
public:
    OpenGLTextureBlitter();
    ~OpenGLTextureBlitter();

    enum Origin {
        OriginBottomLeft,
        OriginTopLeft
    };

    bool create();
private:
    
    OpenGLTextureBlitterPrivate* d;
};

#endif //__OPENGLTEXTUREBLITTER_H__

