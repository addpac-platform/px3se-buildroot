#ifndef __EGL_PLATFORM_CONTEXT_H__
#define __EGL_PLATFORM_CONTEXT_H__

#include "test.h"
#include "platformopenglcontext.h"
#include "surfaceformat.h"
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "eglconvenience.h"

//#include "platformsurface.h"

class EGLPlatformContext : public PlatformOpenGLContext
{
public:
    enum Flag {
        NoSurfaceless = 0x01
    };
    
    EGLPlatformContext(const SurfaceFormat &format, PlatformOpenGLContext *share, EGLDisplay display,
                    EGLConfig *config = 0, const EGLNativeContext &nativeHandle = EGLNativeContext() ,
                    int flags = 0);
    ~EGLPlatformContext();

    void initialize() override;
    
    bool makeCurrent(PlatformSurface *surface) override;

    SurfaceFormat format() const;
    void swapBuffers(PlatformSurface *surface) override;
    
    EGLContext eglContext() const;
    EGLDisplay eglDisplay() const;
    EGLConfig eglConfig() const;



protected:
    #ifdef TODO_ENABLE_INTERFACE
    virtual EGLSurface eglSurfaceForPlatformSurface(PlatformSurface *surface) = 0;
    #endif
    virtual EGLSurface createTemporaryOffscreenSurface();
    virtual void destroyTemporaryOffscreenSurface(EGLSurface surface);
    virtual void runGLChecks();

private:
    void init(const SurfaceFormat &format, PlatformOpenGLContext *share);
    void adopt(const EGLNativeContext &nativeHandle, PlatformOpenGLContext *share);
    void updateFormatFromGL();
    
    EGLContext m_eglContext;
    EGLContext m_shareContext;
    EGLDisplay m_eglDisplay;
    EGLConfig m_eglConfig;
    SurfaceFormat m_format;
    EGLenum m_api;
    int m_swapInterval;
    bool m_swapIntervalEnvChecked;
    int m_swapIntervalFromEnv;
    int m_flags;
    bool m_ownsContext;

    vector<EGLint> m_contextAttrs;

};

#endif //__EGL_PLATFORM_CONTEXT_H__

