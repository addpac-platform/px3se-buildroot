#include "platformwindow.h"



PlatformWindow::PlatformWindow(AWindow *window)
    : PlatformSurface(window)
{
        
}


PlatformWindow::~PlatformWindow()
{
}


AWindow *PlatformWindow::window() const
{
    hDBG("m_surface:%p", m_surface);
    return static_cast<AWindow *>(m_surface);
}


