#ifndef __EGLFS_OFFSCREEN_WINDOW_H__
#define __EGLFS_OFFSCREEN_WINDOW_H__


#include "test.h"
#include "platformoffscreensurface.h"
#include "offscreensurface.h"

#include <EGL/egl.h>
#include <EGL/eglext.h>


class EglFSOffscreenWindow : public PlatformOffscreenSurface
{
public :
    EglFSOffscreenWindow(EGLDisplay display, const SurfaceFormat &format, OffscreenSurface *offscreenSurface);
    ~EglFSOffscreenWindow();


//    SurfaceFormat format() const override { return m_format; }
//    bool isValid() const override { return m_surface != EGL_NO_SURFACE; }

    EGLSurface surface() const { return m_surface; }

private:
    SurfaceFormat m_format;
    EGLDisplay m_display;
    EGLSurface m_surface;
    EGLNativeWindowType m_window;


};

#endif //__EGLFS_OFFSCREEN_WINDOW_H__

