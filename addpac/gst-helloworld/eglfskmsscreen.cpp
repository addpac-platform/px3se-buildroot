
#include "eglfskmsscreen.h"


EglFSKmsScreen::EglFSKmsScreen(KmsDevice *device, const KmsOutput &output)
    : EglFSScreen(eglGetDisplay((EGLNativeDisplayType) device->nativeDisplay()))
    , m_device(device)
    , m_output(output)
    , m_powerState(PowerStateOn)
   // , m_interruptHandler(new QEglFSKmsInterruptHandler(this))
{
    hDBG("");
    //m_siblings << this; // gets overridden later
}

EglFSKmsScreen::~EglFSKmsScreen()
{
    m_output.cleanup(m_device);
    //delete m_interruptHandler;
}



void EglFSKmsScreen::setVirtualPosition(const QPoint &pos)
{
    m_pos = pos;
}


QRect EglFSKmsScreen::rawGeometry() const
{

    const int mode = m_output.mode;
    return QRect(m_pos.x(), m_pos.y(),
                 m_output.modes[mode].hdisplay,
                 m_output.modes[mode].vdisplay);
}



string EglFSKmsScreen::name() const
{
    return m_output.name;
}


void EglFSKmsScreen::setPowerState(PlatformScreen::PowerState state)
{
    m_output.setPowerState(m_device, state);
    m_powerState = state;
}


