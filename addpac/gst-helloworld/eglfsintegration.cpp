
#include "eglconvenience.h"
#include "eglfskmsgbmscreen.h"
#include "openglcontext.h"
#include "offscreensurface.h"
#include "eglfsintegration.h"
#include "quickrendercontrol.h"
#include "openglframebufferobject.h"
#include "openglcompositor.h"

class OpenGLFramebufferObjectFormat;

KmsDevice *m_device;
EGLDisplay m_display;

PlatformWindow *platformWindow;




#include "ap_global.h"

AppPrivate app;


KmsDevice *createDevice()
{
    return new EglFSKmsGbmDevice(NULL, "/dev/dri/card0");
}


EGLNativeWindowType createNativeWindow(
    PlatformScreen *ps,
    const QSize &size,
    const SurfaceFormat &format)
{
    Q_UNUSED(size);
    Q_UNUSED(format);
    hDBG("Creating window\n");

    EglFSKmsGbmScreen *screen = static_cast<EglFSKmsGbmScreen *>(ps);
    if (screen->surface()) {
        hWARN("Only single window per screen supported!\n");
        return 0;
    }

    return reinterpret_cast<EGLNativeWindowType>(screen->createSurface());
}

EGLNativeWindowType createNativeOffscreenWindow(const SurfaceFormat &format)
{
    Q_UNUSED(format);


    hDBG("Creating native off screen window");
    gbm_surface *surface = gbm_surface_create(
        static_cast<EglFSKmsGbmDevice *>(m_device)->gbmDevice(),
        1, 1,
        GBM_FORMAT_XRGB8888,
        GBM_BO_USE_RENDERING);

    return reinterpret_cast<EGLNativeWindowType>(surface);
}

void destroyNativeWindow(EGLNativeWindowType window)
{
    gbm_surface *surface = reinterpret_cast<gbm_surface *>(window);
    gbm_surface_destroy(surface);
}



void platform_init()
{
    printf("platformInit: Opening DRM device\n");
    m_device = createDevice();
    m_device->open();
}

EGLNativeDisplayType platformDisplay()
{
    assert(m_device);
    return (EGLNativeDisplayType) m_device->nativeDisplay();
}



void createDisplay()
{    
    m_display = eglGetDisplay(platformDisplay());
    assert(m_display != EGL_NO_DISPLAY);
    printf("\ncreate egl display\n");
}


void waitForVSync(PlatformSurface *surface) 
{
    #if 0
    hDBG("todo later..."); // QEglFSKmsEglDeviceScreen::waitForFlip()
    
    #else
    PlatformScreen *s = app.getTopScreen();
    
    EglFSKmsScreen *screen = static_cast<EglFSKmsScreen *>(s);

    screen->waitForFlip();
    #endif
}

void presentBuffer(PlatformSurface *surface) 
{
    PlatformScreen *s = app.getTopScreen();
    
    EglFSKmsScreen *screen = static_cast<EglFSKmsScreen *>(s);

    screen->flip();
}


void screenInit()
{
    m_device->createScreens();
}


EGLConfig chooseConfig(EGLDisplay display, const SurfaceFormat &format)
{
    #if 1
    class Chooser : public EglConfigChooser {
    public:
        Chooser(EGLDisplay display)
            : EglConfigChooser(display) { }
        bool filterConfig(EGLConfig config) const override {
            return true  //qt_egl_device_integration()->filterConfig(display(), config)
                    && EglConfigChooser::filterConfig(config);
        }
    };

    Chooser chooser(display);
    chooser.setSurfaceType(EGL_WINDOW_BIT);
    chooser.setSurfaceFormat(format);
    return chooser.chooseConfig();
    #else
    EGLConfig config;
    return config;
    #endif
}



PlatformWindow * createPlatformWindow(AWindow *input_win)
{
    EglFSWindow *w = new EglFSWindow(input_win);
    // get topmost screen ?? 
    PlatformScreen *s = app.getTopScreen();
    assert(s!=NULL);

    // TODO:    w->create();
    w->create(s);

    return w;
    
}




//void QQuickWidgetPrivate::createContext()

OffscreenSurface *g_offscreenSurface = NULL;
QuickRenderControl *g_renderControl= NULL;

OpenGLFramebufferObject *g_fbo= NULL;
OpenGLFramebufferObject *g_resolvedFbo= NULL;

void render(bool needsSync)
{
    if(needsSync){
        g_renderControl->sync();
    }

    g_renderControl->render();

    hDBG("done.");
    
}


void renderAll()
{
    OpenGLCompositor *comp = OpenGLCompositor::instance();

    // TODO: makeCurrent

    comp->renderAll();

}
AWindow *g_w = NULL;
void makeCurrent()
{
    OpenGLContext *context = qt_gl_global_share_context(); // get context from EglFSWindow::create()
    if(!context || !g_w){
        hDBG("todo: create context ...\n");
        assert(1);
    }
    
    context->makeCurrent(g_w);
}

void createContext(AWindow *w)
{    
    hDBG("");


    g_w = w;
    OpenGLContext *context = qt_gl_global_share_context(); // get context from EglFSWindow::create()
    if(!context){
        hDBG("todo: create context ...\n");
        assert(1);
    }
    #if 0 // not yet for offscreen surface

    g_offscreenSurface = new OffscreenSurface;
    //g_offscreenSurface

    g_offscreenSurface->setFormat(context->format());
    g_offscreenSurface->setScreen(context->screen());

    g_offscreenSurface->create();

    if(context->makeCurrent(g_offscreenSurface)) {
        g_renderControl = new QuickRenderControl();

        g_renderControl->initialize(context);
        
    }

    // initialized status
    //createFramebufferObject
    OpenGLFramebufferObjectFormat format;
    
    format.attachment = OpenGLFramebufferObject::CombinedDepthStencil;
    format.samples = (0);
    
    const QSize fboSize(1920, 1080);

    if(!g_fbo){
        g_fbo = new OpenGLFramebufferObject(fboSize, format);
    }else
    {
        // not handled
        assert(0);
    }



    // QQuickWidgetPrivate::render()
    // 
    render(true);
    renderAll();

    #endif
    
#if 1
    // make current for native surface (not offscreen)
    // TODO: check : PlatformSurface *AWindow::surfaceHandle() const
    context->makeCurrent(w);
    renderAll();
#endif    

    hDBG("done.");
    
}




void eglfs_initialize()
{
    platform_init();
    createDisplay();
    printf("createDisplay ... done.\n");
    
    EGLint major, minor;
    assert( eglInitialize(m_display, &major, &minor) );
    printf( "eglinit: %d.%d\n", major, minor);

    screenInit();
    printf("screenInit ... done.\n");
    //app.addScreen();



    // create abstract windows 
    AWindow *w= new AWindow();

    // create native surface
    platformWindow = createPlatformWindow(w);
    printf("platformWindow:%p ... done.\n", platformWindow);

    // offsurface
    // rendercontrol
    createContext(w);

}


