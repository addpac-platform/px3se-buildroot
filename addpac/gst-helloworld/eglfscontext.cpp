#include "ap_global.h"
#include "eglfscontext.h"

#include "eglfswindow.h"
#include "eglfsoffscreenwindow.h"


EglFSContext::EglFSContext(const SurfaceFormat &format, PlatformOpenGLContext *share, EGLDisplay display,
                             EGLConfig *config, const EGLNativeContext &nativeHandle)
    : EGLPlatformContext(format, share, display, config, nativeHandle, 
        AppPrivate::supportsSurfacelessContexts() ? 0 : EGLPlatformContext::NoSurfaceless)
    , m_tempWindow(0)
{
}

#ifdef TODO_ENABLE_INTERFACE

EGLSurface EglFSContext::eglSurfaceForPlatformSurface(PlatformSurface *surface)
{

    hDBG("offscreenSurface(%p), type(%d)", 
        surface,
        surface->surface()->surfaceClass());

    
    if (surface->surface()->surfaceClass() == ASurface::Window)
        return static_cast<EglFSWindow *>(surface)->surface();
    else{        
        //return static_cast<QEGLPbuffer *>(surface)->pbuffer();
        return static_cast<EglFSOffscreenWindow *>(surface)->surface();
    }
}
#if 0//ndef TODO_PART1
EGLSurface QEglFSContext::createTemporaryOffscreenSurface()
{
    if (qt_egl_device_integration()->supportsPBuffers())
        return QEGLPlatformContext::createTemporaryOffscreenSurface();

    if (!m_tempWindow) {
        qMyDbg(" createNativeOffscreenWindow ");
        m_tempWindow = qt_egl_device_integration()->createNativeOffscreenWindow(format());
        if (!m_tempWindow) {
            qWarning("QEglFSContext: Failed to create temporary native window");
            return EGL_NO_SURFACE;
        }
    }
    EGLConfig config = q_configFromGLFormat(eglDisplay(), format());
    return eglCreateWindowSurface(eglDisplay(), config, m_tempWindow, 0);
}

void QEglFSContext::destroyTemporaryOffscreenSurface(EGLSurface surface)
{
    if (qt_egl_device_integration()->supportsPBuffers()) {
        QEGLPlatformContext::destroyTemporaryOffscreenSurface(surface);
    } else {
        eglDestroySurface(eglDisplay(), surface);
        qt_egl_device_integration()->destroyNativeWindow(m_tempWindow);
        m_tempWindow = 0;
    }
}

void QEglFSContext::runGLChecks()
{
    // Note that even though there is an EGL context current here,
    // QOpenGLContext and QOpenGLFunctions are not yet usable at this stage.
    const char *renderer = reinterpret_cast<const char *>(glGetString(GL_RENDERER));
    // Be nice and warn about a common source of confusion.
    if (renderer && strstr(renderer, "llvmpipe"))
        qWarning("Running on a software rasterizer (LLVMpipe), expect limited performance.");
}

void QEglFSContext::swapBuffers(QPlatformSurface *surface)
{
    // draw the cursor
    if (surface->surface()->surfaceClass() == QSurface::Window) {
        QPlatformWindow *window = static_cast<QPlatformWindow *>(surface);
        if (QEglFSCursor *cursor = qobject_cast<QEglFSCursor *>(window->screen()->cursor()))
            cursor->paintOnScreen();
    }

    qt_egl_device_integration()->waitForVSync(surface);
    QEGLPlatformContext::swapBuffers(surface);
    qt_egl_device_integration()->presentBuffer(surface);
}
#endif // TODO_PART1
#endif
