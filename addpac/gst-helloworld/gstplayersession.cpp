
#include "gstplayersession.h"
#include <gst/gst.h>
#include <gst/gstvalue.h>
#include <gst/base/gstbasesrc.h>





#define TODO_LATER

//#define ENABLE_VIDEO_PROBE

#define HSCHOI_DEBUG_PLAYBIN
//#define DEBUG_PLAYBIN
typedef enum {
    GST_PLAY_FLAG_VIDEO         = 0x00000001,
    GST_PLAY_FLAG_AUDIO         = 0x00000002,
    GST_PLAY_FLAG_TEXT          = 0x00000004,
    GST_PLAY_FLAG_VIS           = 0x00000008,
    GST_PLAY_FLAG_SOFT_VOLUME   = 0x00000010,
    GST_PLAY_FLAG_NATIVE_AUDIO  = 0x00000020,
    GST_PLAY_FLAG_NATIVE_VIDEO  = 0x00000040,
    GST_PLAY_FLAG_DOWNLOAD      = 0x00000080,
    GST_PLAY_FLAG_BUFFERING     = 0x000000100
} GstPlayFlags;





// qt5.multimedia 
// QGstreamerPlayerSession::QGstreamerPlayerSession(QObject *parent)

GstPlayerSession::GstPlayerSession()
    :
    m_state(GstPlayState::StoppedState),
    m_pendingState(GstPlayState::StoppedState),
    m_busHelper(0),
    m_playbin(0),
    m_videoSink(0),
    
    m_pendingVideoSink(0),
    m_nullVideoSink(0),
    m_audioSink(0),
    m_volumeElement(0),
    m_bus(0),
    
    m_videoOutput(0),
    m_renderer(0),    
//    m_videoProbe(0),
//    m_audioProbe(0),
//    m_volume(100),
    m_playbackRate(1.0),
//    m_muted(false),
    m_audioAvailable(false),
    m_videoAvailable(false),
    m_seekable(false),
    m_lastPosition(0),
    m_duration(-1),
//    m_durationQueries(0),
    m_displayPrerolledFrame(true),
    m_sourceType(UnknownSrc),
    m_everPlayed(false),
    m_isLiveSource(false)
    
{

    #if 1
    m_playbin = gst_element_factory_make(QT_GSTREAMER_PLAYBIN_ELEMENT_NAME, NULL);
    if (m_playbin) {
        int flags = GST_PLAY_FLAG_VIDEO | GST_PLAY_FLAG_AUDIO;
        g_object_set(G_OBJECT(m_playbin), "flags", flags, NULL);

        GstElement *audioSink = gst_element_factory_make("autoaudiosink", "audiosink");
        if (audioSink) {
            m_volumeElement = gst_element_factory_make("volume", "volumeelement");
            if (m_volumeElement) {
                m_audioSink = gst_bin_new("audio-output-bin");
                gst_bin_add_many(GST_BIN(m_audioSink), m_volumeElement, audioSink, NULL);
                gst_element_link(m_volumeElement, audioSink);

                GstPad *pad = gst_element_get_static_pad(m_volumeElement, "sink");
                gst_element_add_pad(GST_ELEMENT(m_audioSink), gst_ghost_pad_new("sink", pad));
                gst_object_unref(GST_OBJECT(pad));                
            }
            g_object_set(G_OBJECT(m_playbin), "audio-sink", m_audioSink, NULL); 

            // there is no ProbePad 
            // addAudioBufferProbe(); 
        }
    }


    m_videoIdentity = gst_element_factory_make("identity", NULL); // floating ref

    m_nullVideoSink = gst_element_factory_make("fakesink", NULL);
    g_object_set(G_OBJECT(m_nullVideoSink), "sync", true, NULL);
    gst_object_ref(GST_OBJECT(m_nullVideoSink));

    m_videoOutputBin = gst_bin_new("video-output-bin");
    // might not get a parent, take ownership to avoid leak
    qt_gst_object_ref_sink(GST_OBJECT(m_videoOutputBin));
    gst_bin_add_many(GST_BIN(m_videoOutputBin), m_videoIdentity, m_nullVideoSink, NULL);
    gst_element_link(m_videoIdentity, m_nullVideoSink);

    m_videoSink = m_nullVideoSink;

    // add ghostpads
    GstPad *pad = gst_element_get_static_pad(m_videoIdentity,"sink");
    gst_element_add_pad(GST_ELEMENT(m_videoOutputBin), gst_ghost_pad_new("sink", pad));
    gst_object_unref(GST_OBJECT(pad));


    if (m_playbin != 0) {
        // Sort out messages
        m_bus = gst_element_get_bus(m_playbin);
        #ifdef ENABLE_GST_HELPER
        hDBG("install MessageFilter");
        m_busHelper = new GstBusHelper(m_bus, this);
        m_busHelper->installMessageFilter(this);
        #endif

        g_object_set(G_OBJECT(m_playbin), "video-sink", m_videoOutputBin, NULL);

        g_signal_connect(G_OBJECT(m_playbin), "notify::source", G_CALLBACK(playbinNotifySource), this);

        g_signal_connect(G_OBJECT(m_playbin), "element-added", G_CALLBACK(handleElementAdded), this);

        #ifndef TODO_LATER
        if (usePlaybinVolume()) {
            updateVolume();
            updateMuted();
            g_signal_connect(G_OBJECT(m_playbin), "notify::volume", G_CALLBACK(handleVolumeChange), this);
            g_signal_connect(G_OBJECT(m_playbin), "notify::mute", G_CALLBACK(handleMutedChange), this);
        }
        #endif


        g_signal_connect(G_OBJECT(m_playbin), "video-changed", G_CALLBACK(handleStreamsChange), this);
        g_signal_connect(G_OBJECT(m_playbin), "audio-changed", G_CALLBACK(handleStreamsChange), this);
        g_signal_connect(G_OBJECT(m_playbin), "text-changed", G_CALLBACK(handleStreamsChange), this);


#if false //QT_CONFIG(gstreamer_app)
        g_signal_connect(G_OBJECT(m_playbin), "deep-notify::source", G_CALLBACK(configureAppSrcElement), this);
#endif
    }

    
    #endif
}


GstPlayerSession::~GstPlayerSession()
{
    hDBG("");
    if (m_playbin) {
        #if 0
        niPRN("stop");
        #else
        stop();
        #endif

        removeVideoBufferProbe();
        #if 1
        niPRN("removeAudioBufferProbe");
        #else
        removeAudioBufferProbe();
        #endif
        
        delete m_busHelper;
        gst_object_unref(GST_OBJECT(m_bus));
        gst_object_unref(GST_OBJECT(m_playbin));
#if !GST_CHECK_VERSION(1,0,0)
        gst_object_unref(GST_OBJECT(m_colorSpace));
#endif
        gst_object_unref(GST_OBJECT(m_nullVideoSink));
        gst_object_unref(GST_OBJECT(m_videoOutputBin));
    }
}

void GstPlayerSession::playbinNotifySource(GObject *o, GParamSpec *p, gpointer d)
{
  //  Q_UNUSED(p);

    GstElement *source = 0;
    g_object_get(o, "source", &source, NULL);
    if (source == 0)
        return;


    qDebug() << "Playbin source added:" << G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(source));


    GstPlayerSession *self = reinterpret_cast<GstPlayerSession *>(d);


    #ifndef TODO_LATER
    // Set Headers
    const QByteArray userAgentString("User-Agent");
    #endif


    // User-Agent - special case, souphhtpsrc will always set something, even if
    // defined in extra-headers
    if (g_object_class_find_property(G_OBJECT_GET_CLASS(source), "user-agent") != 0) {
        #ifdef TODO_LATER
        niPRN("skip");
        #else
        g_object_set(G_OBJECT(source), "user-agent",
                     self->m_request.rawHeader(userAgentString).constData(), NULL);
        #endif                     
    }

    // The rest
    if (g_object_class_find_property(G_OBJECT_GET_CLASS(source), "extra-headers") != 0) {
        #ifdef TODO_LATER
        niPRN("skip");
        #else    
        GstStructure *extras = qt_gst_structure_new_empty("extras");

        const auto rawHeaderList = self->m_request.rawHeaderList();
        for (const QByteArray &rawHeader : rawHeaderList) {
            if (rawHeader == userAgentString) // Filter User-Agent
                continue;
            else {
                GValue headerValue;

                memset(&headerValue, 0, sizeof(GValue));
                g_value_init(&headerValue, G_TYPE_STRING);

                g_value_set_string(&headerValue,
                                   self->m_request.rawHeader(rawHeader).constData());

                gst_structure_set_value(extras, rawHeader.constData(), &headerValue);
            }
        }

        if (gst_structure_n_fields(extras) > 0)
            g_object_set(G_OBJECT(source), "extra-headers", extras, NULL);

        gst_structure_free(extras);
        #endif
    }

#ifndef TODO_LATER
    //set timeout property to 30 seconds
    const int timeout = 30;
#endif
    if (qstrcmp(G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(source)), "GstUDPSrc") == 0) {
        #ifdef TODO_LATER
        niPRN("skip");
        #else
        //udpsrc timeout unit = microsecond
        //The udpsrc is always a live source.
        g_object_set(G_OBJECT(source), "timeout", G_GUINT64_CONSTANT(timeout*1000000), NULL);
        self->m_sourceType = UDPSrc;
        self->m_isLiveSource = true;
        #endif
    } else if (qstrcmp(G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(source)), "GstSoupHTTPSrc") == 0) {
        #ifdef TODO_LATER
        niPRN("skip");
        bool ssl_strict;
        

        

        g_object_set(G_OBJECT(source), "ssl-strict", FALSE, NULL);

        g_object_get(G_OBJECT(source), "ssl-strict", &ssl_strict, NULL);
        gDBG("ssl_strict:%d \n", ssl_strict);

        hDBG("done.\n");
        

        
        #else    
        //souphttpsrc timeout unit = second
        g_object_set(G_OBJECT(source), "timeout", guint(timeout), NULL);
        self->m_sourceType = SoupHTTPSrc;
        //since gst_base_src_is_live is not reliable, so we check the source property directly
        gboolean isLive = false;
        g_object_get(G_OBJECT(source), "is-live", &isLive, NULL);
        self->m_isLiveSource = isLive;
        #endif
    } else if (qstrcmp(G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(source)), "GstMMSSrc") == 0) {
        #ifdef TODO_LATER
        niPRN("skip");
        #else    
        self->m_sourceType = MMSSrc;
        self->m_isLiveSource = gst_base_src_is_live(GST_BASE_SRC(source));
        g_object_set(G_OBJECT(source), "tcp-timeout", G_GUINT64_CONSTANT(timeout*1000000), NULL);
        #endif
    } else if (qstrcmp(G_OBJECT_CLASS_NAME(G_OBJECT_GET_CLASS(source)), "GstRTSPSrc") == 0) {
        #ifdef TODO_LATER
        niPRN("skip");
        #else    
        //rtspsrc acts like a live source and will therefore only generate data in the PLAYING state.
        self->m_sourceType = RTSPSrc;
        self->m_isLiveSource = true;
        g_object_set(G_OBJECT(source), "buffer-mode", 1, NULL);
        #endif
    } else {
        self->m_sourceType = UnknownSrc;
        self->m_isLiveSource = gst_base_src_is_live(GST_BASE_SRC(source));
        hDBG("unknown source");
    }

#ifdef DEBUG_PLAYBIN
    if (self->m_isLiveSource)
        qDebug() << "Current source is a live source";
    else
        qDebug() << "Current source is a non-live source";
#endif

    if (self->m_videoSink)
        g_object_set(G_OBJECT(self->m_videoSink), "sync", !self->m_isLiveSource, NULL);

    gst_object_unref(source);
}



void GstPlayerSession::handleElementAdded(GstBin *bin, GstElement *element, GstPlayerSession *session)
{
    //Q_UNUSED(bin);
    //we have to configure queue2 element to enable media downloading
    //and reporting available ranges,
    //but it's added dynamically to playbin2

    gchar *elementName = gst_element_get_name(element);

    if (g_str_has_prefix(elementName, "queue2")) {
        // Disable on-disk buffering.
        g_object_set(G_OBJECT(element), "temp-template", NULL, NULL);
    } else if (g_str_has_prefix(elementName, "uridecodebin") ||
#if GST_CHECK_VERSION(1,0,0)
        g_str_has_prefix(elementName, "decodebin")) {
#else
        g_str_has_prefix(elementName, "decodebin2")) {
        if (g_str_has_prefix(elementName, "uridecodebin")) {
            // Add video/x-surface (VAAPI) to default raw formats
            g_object_set(G_OBJECT(element), "caps", gst_static_caps_get(&static_RawCaps), NULL);
            // listen for uridecodebin autoplug-select to skip VAAPI usage when the current
            // video sink doesn't support it
            g_signal_connect(element, "autoplug-select", G_CALLBACK(handleAutoplugSelect), session);
        }
#endif
        //listen for queue2 element added to uridecodebin/decodebin2 as well.
        //Don't touch other bins since they may have unrelated queues
        g_signal_connect(element, "element-added",
                         G_CALLBACK(handleElementAdded), session);
    }

    g_free(elementName);
}



void GstPlayerSession::handleStreamsChange(GstBin *bin, gpointer user_data)
{
    //Q_UNUSED(bin);

    GstPlayerSession* session = reinterpret_cast<GstPlayerSession*>(user_data);
    session->getStreamsInfo();

    
}

void GstPlayerSession::getStreamsInfo()
{
    #ifndef TODO_LATER
    QList< QMap<QString,QVariant> > oldProperties = m_streamProperties;
    #endif
    list<StreamType> oldTypes = m_streamTypes;
    map<StreamType, int> oldOffset = m_playbin2StreamOffset;

    //check if video is available:
    bool haveAudio = false;
    bool haveVideo = false;
    #ifndef TODO_LATER
    m_streamProperties.clear();
    #endif
    m_streamTypes.clear();
    m_playbin2StreamOffset.clear();

    gint audioStreamsCount = 0;
    gint videoStreamsCount = 0;
    gint textStreamsCount = 0;

    g_object_get(G_OBJECT(m_playbin), "n-audio", &audioStreamsCount, NULL);
    g_object_get(G_OBJECT(m_playbin), "n-video", &videoStreamsCount, NULL);
    g_object_get(G_OBJECT(m_playbin), "n-text", &textStreamsCount, NULL);

    haveAudio = audioStreamsCount > 0;
    haveVideo = videoStreamsCount > 0;

    m_playbin2StreamOffset[AudioStream] = 0;
    m_playbin2StreamOffset[VideoStream] = audioStreamsCount;
    m_playbin2StreamOffset[SubPictureStream] = audioStreamsCount+videoStreamsCount;

    for (int i=0; i<audioStreamsCount; i++)
        m_streamTypes.push_back(AudioStream);

    for (int i=0; i<videoStreamsCount; i++)
        m_streamTypes.push_back(VideoStream);

    for (int i=0; i<textStreamsCount; i++)
        m_streamTypes.push_back(SubPictureStream);

    list<StreamType>::iterator iter;
 
    
    //for (int i=0; i<m_streamTypes.size(); i++) {
    int i=0;
    for(iter=m_streamTypes.begin(); iter!= m_streamTypes.end(); ++iter){
        //StreamType streamType = m_streamTypes[i];
        StreamType streamType = *iter;

        #ifndef TODO_LATER
        QMap<QString, QVariant> streamProperties;
        #endif
#if 1
        int streamIndex = i - m_playbin2StreamOffset[streamType];
        

        GstTagList *tags = 0;
        switch (streamType) {
        case AudioStream:
            g_signal_emit_by_name(G_OBJECT(m_playbin), "get-audio-tags", streamIndex, &tags);
            break;
        case VideoStream:
            g_signal_emit_by_name(G_OBJECT(m_playbin), "get-video-tags", streamIndex, &tags);
            break;
        case SubPictureStream:
            g_signal_emit_by_name(G_OBJECT(m_playbin), "get-text-tags", streamIndex, &tags);
            break;
        default:
            break;
        }
#endif
        #ifndef TODO_LATER
#if GST_CHECK_VERSION(1,0,0)
        if (tags && GST_IS_TAG_LIST(tags)) {
#else
        if (tags && gst_is_tag_list(tags)) {
#endif
            gchar *languageCode = 0;
            if (gst_tag_list_get_string(tags, GST_TAG_LANGUAGE_CODE, &languageCode))
                streamProperties[QMediaMetaData::Language] = QString::fromUtf8(languageCode);

            //qDebug() << "language for setream" << i << QString::fromUtf8(languageCode);
            g_free (languageCode);
            gst_tag_list_free(tags);
        }

        m_streamProperties.append(streamProperties);
        #endif
        i++;
    }

    bool emitAudioChanged = (haveAudio != m_audioAvailable);
    bool emitVideoChanged = (haveVideo != m_videoAvailable);

    m_audioAvailable = haveAudio;
    m_videoAvailable = haveVideo;



    #if 1
    if (emitAudioChanged) {
        
        #ifdef TODO_LATER
        niPRN("emitAudioChanged ...");
        #else
        //QThread::msleep(5000);
        emit audioAvailableChanged(m_audioAvailable);
        qMyDbg("emitAudioChanged ... done.");
        #endif
    }
    if (emitVideoChanged) {
        #ifdef TODO_LATER
        niPRN("emitAudioChanged ...");
        #else

        emit videoAvailableChanged(m_videoAvailable);
        qMyDbg("emitVideoChanged ... done.");
        #endif
    }

    if (/* oldProperties != m_streamProperties || */ 
        oldTypes != m_streamTypes || oldOffset != m_playbin2StreamOffset){
        #ifdef TODO_LATER
        niPRN("streamsChanged ...");
        #else

        qMyDbgSleep("streamsChanged ...", 5000);
        emit streamsChanged();
        qMyDbg("streamsChanged ... done.");
        #endif
    }
    #endif
}

#ifdef ADD_BUS_HELPER
bool GstPlayerSession::processBusMessage(const apGstMessage &message)
{
    GstMessage* gm = message.rawMessage();
    if (gm) {
        //tag message comes from elements inside playbin, not from playbin itself
        if (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_TAG) {
            GstTagList *tag_list;
            gst_message_parse_tag(gm, &tag_list);

            QMap<QByteArray, QVariant> newTags = QGstUtils::gstTagListToMap(tag_list);
            QMap<QByteArray, QVariant>::const_iterator it = newTags.constBegin();
            for ( ; it != newTags.constEnd(); ++it)
                m_tags.insert(it.key(), it.value()); // overwrite existing tags

            gst_tag_list_free(tag_list);

            emit tagsChanged();
        } else if (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_DURATION) {
            updateDuration();
        }

#ifdef DEBUG_PLAYBIN
        if (m_sourceType == MMSSrc && qstrcmp(GST_OBJECT_NAME(GST_MESSAGE_SRC(gm)), "source") == 0) {
            qDebug() << "Message from MMSSrc: " << GST_MESSAGE_TYPE(gm);
        } else if (m_sourceType == RTSPSrc && qstrcmp(GST_OBJECT_NAME(GST_MESSAGE_SRC(gm)), "source") == 0) {
            qDebug() << "Message from RTSPSrc: " << GST_MESSAGE_TYPE(gm);
        } else {
            qDebug() << "Message from " << GST_OBJECT_NAME(GST_MESSAGE_SRC(gm)) << ":" << GST_MESSAGE_TYPE(gm)
                << " ,"<< GST_MESSAGE_TYPE_NAME(gm);
        }
#endif

        if (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_BUFFERING) {
            int progress = 0;
            gst_message_parse_buffering(gm, &progress);
            emit bufferingProgressChanged(progress);
        }

        bool handlePlaybin2 = false;
        if (GST_MESSAGE_SRC(gm) == GST_OBJECT_CAST(m_playbin)) {
            switch (GST_MESSAGE_TYPE(gm))  {
            case GST_MESSAGE_STATE_CHANGED:
                {
                    GstState    oldState;
                    GstState    newState;
                    GstState    pending;

                    gst_message_parse_state_changed(gm, &oldState, &newState, &pending);

#ifdef DEBUG_PLAYBIN
                    QStringList states;
                    states << "GST_STATE_VOID_PENDING" <<  "GST_STATE_NULL" << "GST_STATE_READY" << "GST_STATE_PAUSED" << "GST_STATE_PLAYING";

                    qDebug() << QString("state changed: old: %1  new: %2  pending: %3") \
                            .arg(states[oldState]) \
                            .arg(states[newState]) \
                            .arg(states[pending]);
#endif

                    switch (newState) {
                    case GST_STATE_VOID_PENDING:
                    case GST_STATE_NULL:
                        setSeekable(false);
                        finishVideoOutputChange();
                        if (m_state != QMediaPlayer::StoppedState)
                            emit stateChanged(m_state = QMediaPlayer::StoppedState);
                        break;
                    case GST_STATE_READY:
                        setSeekable(false);
                        if (m_state != QMediaPlayer::StoppedState)
                            emit stateChanged(m_state = QMediaPlayer::StoppedState);
                        break;
                    case GST_STATE_PAUSED:
                    {
                        QMediaPlayer::State prevState = m_state;
                        m_state = QMediaPlayer::PausedState;

                        //check for seekable
                        if (oldState == GST_STATE_READY) {
                            if (m_sourceType == SoupHTTPSrc || m_sourceType == MMSSrc) {
                                //since udpsrc is a live source, it is not applicable here
                                m_everPlayed = true;
                            }

                            getStreamsInfo();
                            updateVideoResolutionTag();

                            //gstreamer doesn't give a reliable indication the duration
                            //information is ready, GST_MESSAGE_DURATION is not sent by most elements
                            //the duration is queried up to 5 times with increasing delay
                            m_durationQueries = 5;
                            // This should also update the seekable flag.
                            updateDuration();

                            if (!qFuzzyCompare(m_playbackRate, qreal(1.0))) {
                                qreal rate = m_playbackRate;
                                m_playbackRate = 1.0;
                                setPlaybackRate(rate);
                            }
                        }

                        if (m_state != prevState)
                            emit stateChanged(m_state);

                        break;
                    }
                    case GST_STATE_PLAYING:
                        m_everPlayed = true;
                        if (m_state != QMediaPlayer::PlayingState) {
                            emit stateChanged(m_state = QMediaPlayer::PlayingState);

                            // For rtsp streams duration information might not be available
                            // until playback starts.
                            if (m_duration <= 0) {
                                m_durationQueries = 5;
                                updateDuration();
                            }
                        }

                        break;
                    }
                }
                break;

            case GST_MESSAGE_EOS:
                emit playbackFinished();
                break;

            case GST_MESSAGE_TAG:
            case GST_MESSAGE_STREAM_STATUS:
            case GST_MESSAGE_UNKNOWN:
                break;
            case GST_MESSAGE_ERROR: {
                    GError *err;
                    gchar *debug;
                    gst_message_parse_error(gm, &err, &debug);
                    if (err->domain == GST_STREAM_ERROR && err->code == GST_STREAM_ERROR_CODEC_NOT_FOUND)
                        processInvalidMedia(QMediaPlayer::FormatError, tr("Cannot play stream of type: <unknown>"));
                    else
                        processInvalidMedia(QMediaPlayer::ResourceError, QString::fromUtf8(err->message));
                    qWarning() << "Error:" << QString::fromUtf8(err->message);
                    g_error_free(err);
                    g_free(debug);
                }
                break;
            case GST_MESSAGE_WARNING:
                {
                    GError *err;
                    gchar *debug;
                    gst_message_parse_warning (gm, &err, &debug);
                    qWarning() << "Warning:" << QString::fromUtf8(err->message);
                    g_error_free (err);
                    g_free (debug);
                }
                break;
            case GST_MESSAGE_INFO:
#ifdef DEBUG_PLAYBIN
                {
                    GError *err;
                    gchar *debug;
                    gst_message_parse_info (gm, &err, &debug);
                    qDebug() << "Info:" << QString::fromUtf8(err->message);
                    g_error_free (err);
                    g_free (debug);
                }
#endif
                break;
            case GST_MESSAGE_BUFFERING:
            case GST_MESSAGE_STATE_DIRTY:
            case GST_MESSAGE_STEP_DONE:
            case GST_MESSAGE_CLOCK_PROVIDE:
            case GST_MESSAGE_CLOCK_LOST:
            case GST_MESSAGE_NEW_CLOCK:
            case GST_MESSAGE_STRUCTURE_CHANGE:
            case GST_MESSAGE_APPLICATION:
            case GST_MESSAGE_ELEMENT:
                break;
            case GST_MESSAGE_SEGMENT_START:
                {
                    const GstStructure *structure = gst_message_get_structure(gm);
                    qint64 position = g_value_get_int64(gst_structure_get_value(structure, "position"));
                    position /= 1000000;
                    m_lastPosition = position;
                    emit positionChanged(position);
                }
                break;
            case GST_MESSAGE_SEGMENT_DONE:
                break;
            case GST_MESSAGE_LATENCY:
#if GST_CHECK_VERSION(0,10,13)
            case GST_MESSAGE_ASYNC_START:
                break;
            case GST_MESSAGE_ASYNC_DONE:
            {
                gint64      position = 0;
                if (qt_gst_element_query_position(m_playbin, GST_FORMAT_TIME, &position)) {
                    position /= 1000000;
                    m_lastPosition = position;
                    emit positionChanged(position);
                }
                break;
            }
#if GST_CHECK_VERSION(0,10,23)
            case GST_MESSAGE_REQUEST_STATE:
#endif
#endif
            case GST_MESSAGE_ANY:
                break;
            default:
                break;
            }
        } else if (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_ERROR) {
            GError *err;
            gchar *debug;
            gst_message_parse_error(gm, &err, &debug);
            // If the source has given up, so do we.
            if (qstrcmp(GST_OBJECT_NAME(GST_MESSAGE_SRC(gm)), "source") == 0) {
                bool everPlayed = m_everPlayed;
                // Try and differentiate network related resource errors from the others
                if (!m_request.url().isRelative() && m_request.url().scheme().compare(QLatin1String("file"), Qt::CaseInsensitive) != 0 ) {
                    if (everPlayed ||
                        (err->domain == GST_RESOURCE_ERROR && (
                         err->code == GST_RESOURCE_ERROR_BUSY ||
                         err->code == GST_RESOURCE_ERROR_OPEN_READ ||
                         err->code == GST_RESOURCE_ERROR_READ ||
                         err->code == GST_RESOURCE_ERROR_SEEK ||
                         err->code == GST_RESOURCE_ERROR_SYNC))) {
                        processInvalidMedia(QMediaPlayer::NetworkError, QString::fromUtf8(err->message));
                    } else {
                        processInvalidMedia(QMediaPlayer::ResourceError, QString::fromUtf8(err->message));
                    }
                }
                else
                    processInvalidMedia(QMediaPlayer::ResourceError, QString::fromUtf8(err->message));
            } else if (err->domain == GST_STREAM_ERROR
                       && (err->code == GST_STREAM_ERROR_DECRYPT || err->code == GST_STREAM_ERROR_DECRYPT_NOKEY)) {
                processInvalidMedia(QMediaPlayer::AccessDeniedError, QString::fromUtf8(err->message));
            } else {
                handlePlaybin2 = true;
            }
            if (!handlePlaybin2)
                qWarning() << "Error:" << QString::fromUtf8(err->message);
            g_error_free(err);
            g_free(debug);
        } else if (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_ELEMENT
                   && qstrcmp(GST_OBJECT_NAME(GST_MESSAGE_SRC(gm)), "source") == 0
                   && m_sourceType == UDPSrc
                   && gst_structure_has_name(gst_message_get_structure(gm), "GstUDPSrcTimeout")) {
            //since udpsrc will not generate an error for the timeout event,
            //we need to process its element message here and treat it as an error.
            processInvalidMedia(m_everPlayed ? QMediaPlayer::NetworkError : QMediaPlayer::ResourceError,
                                tr("UDP source timeout"));
        } else {
            handlePlaybin2 = true;
        }

        if (handlePlaybin2) {
            if (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_WARNING) {
                GError *err;
                gchar *debug;
                gst_message_parse_warning(gm, &err, &debug);
                if (err->domain == GST_STREAM_ERROR && err->code == GST_STREAM_ERROR_CODEC_NOT_FOUND)
                    emit error(int(QMediaPlayer::FormatError), tr("Cannot play stream of type: <unknown>"));
                // GStreamer shows warning for HTTP playlists
                if (err && err->message)
                    qWarning() << "Warning:" << QString::fromUtf8(err->message);
                g_error_free(err);
                g_free(debug);
            } else if (GST_MESSAGE_TYPE(gm) == GST_MESSAGE_ERROR) {
                GError *err;
                gchar *debug;
                gst_message_parse_error(gm, &err, &debug);

                // Nearly all errors map to ResourceError
                QMediaPlayer::Error qerror = QMediaPlayer::ResourceError;
                if (err->domain == GST_STREAM_ERROR
                           && (err->code == GST_STREAM_ERROR_DECRYPT
                               || err->code == GST_STREAM_ERROR_DECRYPT_NOKEY)) {
                    qerror = QMediaPlayer::AccessDeniedError;
                }
                processInvalidMedia(qerror, QString::fromUtf8(err->message));
                if (err && err->message)
                    qWarning() << "Error:" << QString::fromUtf8(err->message);

                g_error_free(err);
                g_free(debug);
            }
        }
    }

    return false;
}
#else
bool GstPlayerSession::processBusMessage(const apGstMessage &message)
{
    GstMessage* gm = message.rawMessage();
    if(gm){
        d1DBG("test : 0x%x", GST_MESSAGE_TYPE(gm));
    }

    return false;
}



#endif // ADD_BUS_HELPER


















#if GST_CHECK_VERSION(1,0,0)
static GstPadProbeReturn block_pad_cb(GstPad *pad, GstPadProbeInfo *info, gpointer user_data)
#else
static void block_pad_cb(GstPad *pad, gboolean blocked, gpointer user_data)
#endif
{
    Q_UNUSED(pad);
#if GST_CHECK_VERSION(1,0,0)
    Q_UNUSED(info);
    Q_UNUSED(user_data);
    return GST_PAD_PROBE_OK;
#else
#ifdef DEBUG_PLAYBIN
    qDebug() << "block_pad_cb, blocked:" << blocked;
#endif
    if (blocked && user_data) {
        GstPlayerSession *session = reinterpret_cast<GstPlayerSession*>(user_data);
        #ifdef TODO_LATER
        niPRN("");
        #else
        QMetaObject::invokeMethod(session, "finishVideoOutputChange", Qt::QueuedConnection);
        #endif
    }
#endif
}



void GstPlayerSession::updateVideoRenderer()
{
#ifdef DEBUG_PLAYBIN
    qDebug() << "Video sink has chaged, reload video output";
#endif

    if (m_videoOutput)
        setVideoRenderer(m_videoOutput);
}


void GstPlayerSession::setVideoRenderer(GstVideoWindow *videoOutput)
{
    hDBG("%p, %p", m_videoOutput, videoOutput);
#ifdef DEBUG_PLAYBIN
    qDebug() << Q_FUNC_INFO;
#endif

#if 1
    if (m_videoOutput != videoOutput) {
        if (m_videoOutput) {
            #ifdef TODO_LATER
            niPRN("");
            #else

            disconnect(m_videoOutput, SIGNAL(sinkChanged()),
                       this, SLOT(updateVideoRenderer()));
            disconnect(m_videoOutput, SIGNAL(readyChanged(bool)),
                   this, SLOT(updateVideoRenderer()));
            #endif

            m_busHelper->removeMessageFilter(m_videoOutput);
        }

        m_videoOutput = videoOutput;

        if (m_videoOutput) {
            #ifdef TODO_LATER
            niPRN("");
            #else
            
            connect(m_videoOutput, SIGNAL(sinkChanged()),
                    this, SLOT(updateVideoRenderer()));
            connect(m_videoOutput, SIGNAL(readyChanged(bool)),
                   this, SLOT(updateVideoRenderer()));
            #endif
            m_busHelper->installMessageFilter(m_videoOutput);
        }
    }

    GstVideoRendererInterface* renderer = dynamic_cast<GstVideoRendererInterface*>(videoOutput);

    hDBG("m_renderer:%p, renderer:%p", m_renderer, renderer);

    m_renderer = renderer;

#ifdef DEBUG_VO_BIN_DUMP
    gst_debug_bin_to_dot_file_with_ts(GST_BIN(m_playbin),
                                  GstDebugGraphDetails(GST_DEBUG_GRAPH_SHOW_ALL /* GST_DEBUG_GRAPH_SHOW_MEDIA_TYPE | GST_DEBUG_GRAPH_SHOW_NON_DEFAULT_PARAMS | GST_DEBUG_GRAPH_SHOW_STATES*/),
                                  "playbin_set");
#endif

    GstElement *videoSink = 0;
    if (m_renderer && m_renderer->isReady())
        videoSink = m_renderer->videoSink();

    if (!videoSink)
        videoSink = m_nullVideoSink;

#if 1//def DEBUG_PLAYBIN
    hDBG("Set video output:%p", videoOutput);
    hDBG("Current sink:%s (%p)", (m_videoSink ? GST_ELEMENT_NAME(m_videoSink) : "") ,  m_videoSink);
    hDBG("pending:%s (%p)", (m_pendingVideoSink ? GST_ELEMENT_NAME(m_pendingVideoSink) : ""), m_pendingVideoSink);
    hDBG("new sink:%s (%p)" ,(videoSink ? GST_ELEMENT_NAME(videoSink) : "") , videoSink);
#endif

    if (m_pendingVideoSink == videoSink ||
        (m_pendingVideoSink == 0 && m_videoSink == videoSink)) {
#ifdef DEBUG_PLAYBIN
        qDebug() << "Video sink has not changed, skip video output reconfiguration";
#endif
        return;
    }

#if 1//def DEBUG_PLAYBIN
    hDBG("Reconfigure video output");
#endif

    if (m_state == GstPlayState::StoppedState) {
#if 1//def DEBUG_PLAYBIN
        hDBG("The pipeline has not started yet, pending state:%d" ,m_pendingState);
#endif
        //the pipeline has not started yet
        flushVideoProbes();
        m_pendingVideoSink = 0;
        gst_element_set_state(m_videoSink, GST_STATE_NULL);
        gst_element_set_state(m_playbin, GST_STATE_NULL);

#if !GST_CHECK_VERSION(1,0,0)
        if (m_usingColorspaceElement) {
            gst_element_unlink(m_colorSpace, m_videoSink);
            gst_bin_remove(GST_BIN(m_videoOutputBin), m_colorSpace);
        } else {
            gst_element_unlink(m_videoIdentity, m_videoSink);
        }
#endif

        removeVideoBufferProbe();

        gst_bin_remove(GST_BIN(m_videoOutputBin), m_videoSink);

        m_videoSink = videoSink;

        gst_bin_add(GST_BIN(m_videoOutputBin), m_videoSink);

        bool linked = gst_element_link(m_videoIdentity, m_videoSink);
#if !GST_CHECK_VERSION(1,0,0)
        m_usingColorspaceElement = false;
        if (!linked) {
            m_usingColorspaceElement = true;
#ifdef DEBUG_PLAYBIN
            qDebug() << "Failed to connect video output, inserting the colorspace element.";
#endif
            gst_bin_add(GST_BIN(m_videoOutputBin), m_colorSpace);
            linked = gst_element_link_many(m_videoIdentity, m_colorSpace, m_videoSink, NULL);
        }
#endif

        if (!linked)
            hDBG( "Linking video output element failed");

        if (g_object_class_find_property(G_OBJECT_GET_CLASS(m_videoSink), "show-preroll-frame") != 0) {
            gboolean value = m_displayPrerolledFrame;
            g_object_set(G_OBJECT(m_videoSink), "show-preroll-frame", value, NULL);
        }

        addVideoBufferProbe();

        switch (m_pendingState) {
        case GstPlayState::PausedState:
            gst_element_set_state(m_playbin, GST_STATE_PAUSED);
            break;
        case GstPlayState::PlayingState:
            gst_element_set_state(m_playbin, GST_STATE_PLAYING);
            break;
        default:
            break;
        }

        resumeVideoProbes();

    } else {
        if (m_pendingVideoSink) {
#ifdef DEBUG_PLAYBIN
            qDebug() << "already waiting for pad to be blocked, just change the pending sink";
#endif
            m_pendingVideoSink = videoSink;
            return;
        }

        m_pendingVideoSink = videoSink;

#ifdef DEBUG_PLAYBIN
        qDebug() << "Blocking the video output pad...";
#endif

        //block pads, async to avoid locking in paused state
        GstPad *srcPad = gst_element_get_static_pad(m_videoIdentity, "src");
#if GST_CHECK_VERSION(1,0,0)
        this->pad_probe_id = gst_pad_add_probe(srcPad, (GstPadProbeType)(GST_PAD_PROBE_TYPE_BUFFER | GST_PAD_PROBE_TYPE_BLOCKING), block_pad_cb, this, NULL);
#else
        gst_pad_set_blocked_async(srcPad, true, &block_pad_cb, this);
#endif
        gst_object_unref(GST_OBJECT(srcPad));

        //Unpause the sink to avoid waiting until the buffer is processed
        //while the sink is paused. The pad will be blocked as soon as the current
        //buffer is processed.
        if (m_state == GstPlayState::PausedState) {
#ifdef DEBUG_PLAYBIN
            qDebug() << "Starting video output to avoid blocking in paused state...";
#endif
            gst_element_set_state(m_videoSink, GST_STATE_PLAYING);
        }
    }

    #endif
}


void GstPlayerSession::addVideoBufferProbe()
{
    #ifdef ENABLE_VIDEO_PROBE
    
    qMyDbg("0x%x", m_videoProbe);

    if (!m_videoProbe)
        return;

    GstPad *pad = gst_element_get_static_pad(m_videoSink, "sink");
    if (pad) {
        m_videoProbe->addProbeToPad(pad);
        gst_object_unref(GST_OBJECT(pad));
    }
    #endif
}


void GstPlayerSession::flushVideoProbes()
{
    #ifdef ENABLE_VIDEO_PROBE
    if (m_videoProbe)
        m_videoProbe->startFlushing();
    #endif
}



void GstPlayerSession::removeVideoBufferProbe()
{
    #ifdef ENABLE_VIDEO_PROBE
    if (!m_videoProbe)
        return;

    GstPad *pad = gst_element_get_static_pad(m_videoSink, "sink");
    if (pad) {
        m_videoProbe->removeProbeFromPad(pad);
        gst_object_unref(GST_OBJECT(pad));
    }
    #endif
}

void GstPlayerSession::resumeVideoProbes()
{
    
    #ifdef ENABLE_VIDEO_PROBE
    if (m_videoProbe)
        m_videoProbe->stopFlushing();
    qMyDbg("done.");
    #endif
}


void GstPlayerSession::showPrerollFrames(bool enabled)
{
#ifdef DEBUG_PLAYBIN
    qDebug() << Q_FUNC_INFO << enabled;
#endif
    if (enabled != m_displayPrerolledFrame && m_videoSink &&
            g_object_class_find_property(G_OBJECT_GET_CLASS(m_videoSink), "show-preroll-frame") != 0) {

        gboolean value = enabled;
        g_object_set(G_OBJECT(m_videoSink), "show-preroll-frame", value, NULL);
        m_displayPrerolledFrame = enabled;
    }
}



void GstPlayerSession::finishVideoOutputChange()
{
    if (!m_pendingVideoSink)
        return;

#ifdef DEBUG_PLAYBIN
    qDebug() << "finishVideoOutputChange" << m_pendingVideoSink;
#endif

    GstPad *srcPad = gst_element_get_static_pad(m_videoIdentity, "src");

    if (!gst_pad_is_blocked(srcPad)) {
        //pad is not blocked, it's possible to swap outputs only in the null state
        qWarning() << "Pad is not blocked yet, could not switch video sink";
        GstState identityElementState = GST_STATE_NULL;
        gst_element_get_state(m_videoIdentity, &identityElementState, NULL, GST_CLOCK_TIME_NONE);
        if (identityElementState != GST_STATE_NULL) {
            gst_object_unref(GST_OBJECT(srcPad));
            return; //can't change vo yet, received async call from the previous change
        }
    }

    if (m_pendingVideoSink == m_videoSink) {
        qDebug() << "Abort, no change";
        //video output was change back to the current one,
        //no need to torment the pipeline, just unblock the pad
        if (gst_pad_is_blocked(srcPad))
#if GST_CHECK_VERSION(1,0,0)
            gst_pad_remove_probe(srcPad, this->pad_probe_id);
#else
            gst_pad_set_blocked_async(srcPad, false, &block_pad_cb, 0);
#endif

        m_pendingVideoSink = 0;
        gst_object_unref(GST_OBJECT(srcPad));
        return;
    }

#if !GST_CHECK_VERSION(1,0,0)
    if (m_usingColorspaceElement) {
        gst_element_set_state(m_colorSpace, GST_STATE_NULL);
        gst_element_set_state(m_videoSink, GST_STATE_NULL);

        gst_element_unlink(m_colorSpace, m_videoSink);
        gst_bin_remove(GST_BIN(m_videoOutputBin), m_colorSpace);
    } else {
#else
    {
#endif
        gst_element_set_state(m_videoSink, GST_STATE_NULL);
        gst_element_unlink(m_videoIdentity, m_videoSink);
    }

    removeVideoBufferProbe();

    gst_bin_remove(GST_BIN(m_videoOutputBin), m_videoSink);

    m_videoSink = m_pendingVideoSink;
    m_pendingVideoSink = 0;

    gst_bin_add(GST_BIN(m_videoOutputBin), m_videoSink);

    addVideoBufferProbe();

    bool linked = gst_element_link(m_videoIdentity, m_videoSink);
#if !GST_CHECK_VERSION(1,0,0)
    m_usingColorspaceElement = false;
    if (!linked) {
        m_usingColorspaceElement = true;
#ifdef DEBUG_PLAYBIN
        qDebug() << "Failed to connect video output, inserting the colorspace element.";
#endif
        gst_bin_add(GST_BIN(m_videoOutputBin), m_colorSpace);
        linked = gst_element_link_many(m_videoIdentity, m_colorSpace, m_videoSink, NULL);
    }
#endif

    if (!linked)
        qWarning() << "Linking video output element failed";

#ifdef DEBUG_PLAYBIN
    qDebug() << "notify the video connector it has to emit a new segment message...";
#endif

#if !GST_CHECK_VERSION(1,0,0)
    //it's necessary to send a new segment event just before
    //the first buffer pushed to the new sink
    g_signal_emit_by_name(m_videoIdentity,
                          "resend-new-segment",
                          true //emit connection-failed signal
                               //to have a chance to insert colorspace element
                          );
#endif

    GstState state = GST_STATE_VOID_PENDING;

    switch (m_pendingState) {
    case GstPlayState::StoppedState:
        state = GST_STATE_NULL;
        break;
    case GstPlayState::PausedState:
        state = GST_STATE_PAUSED;
        break;
    case GstPlayState::PlayingState:
        state = GST_STATE_PLAYING;
        break;
    }

#if !GST_CHECK_VERSION(1,0,0)
    if (m_usingColorspaceElement)
        gst_element_set_state(m_colorSpace, state);
#endif

    gst_element_set_state(m_videoSink, state);

    if (state == GST_STATE_NULL)
        flushVideoProbes();

    // Set state change that was deferred due the video output
    // change being pending
    gst_element_set_state(m_playbin, state);

    if (state != GST_STATE_NULL)
        resumeVideoProbes();

    //don't have to wait here, it will unblock eventually
    if (gst_pad_is_blocked(srcPad))
#if GST_CHECK_VERSION(1,0,0)
            gst_pad_remove_probe(srcPad, this->pad_probe_id);
#else
            gst_pad_set_blocked_async(srcPad, false, &block_pad_cb, 0);
#endif

    gst_object_unref(GST_OBJECT(srcPad));

#ifdef DEBUG_VO_BIN_DUMP
    gst_debug_bin_to_dot_file_with_ts(GST_BIN(m_playbin),
                                  GstDebugGraphDetails(GST_DEBUG_GRAPH_SHOW_ALL /* | GST_DEBUG_GRAPH_SHOW_MEDIA_TYPE | GST_DEBUG_GRAPH_SHOW_NON_DEFAULT_PARAMS | GST_DEBUG_GRAPH_SHOW_STATES */),
                                  "playbin_finish");
#endif
}







void GstPlayerSession::setSeekable(bool seekable)
{
#ifdef HSCHOI_DEBUG_PLAYBIN
        qDebug() << Q_FUNC_INFO << seekable << ". set seakable ... \n";
#endif
    if (seekable != m_seekable) {
        m_seekable = seekable;
        #ifdef TODO_LATER
        niPRN("");
        #else
        emit seekableChanged(m_seekable);
        #endif
    }
}


bool GstPlayerSession::seek(long long ms)
{
#ifdef HSCHOI_DEBUG_PLAYBIN
    qDebug() << Q_FUNC_INFO << ms << endl;
#endif
    
    //seek locks when the video output sink is changing and pad is blocked
    if (m_playbin && !m_pendingVideoSink ) { 
        
        // TODO: && m_state != GstPlayState::StoppedState && m_seekable) {
        hDBG("seek >> ");
        ms = qMax(ms, (long long) 0);
        gint64  position = ms * 1000000;
        bool isSeeking = gst_element_seek(m_playbin,
                                          m_playbackRate,
                                          GST_FORMAT_TIME,
                                          GstSeekFlags(GST_SEEK_FLAG_FLUSH),
                                          GST_SEEK_TYPE_SET,
                                          position,
                                          GST_SEEK_TYPE_NONE,
                                          0);
        if (isSeeking)
            m_lastPosition = ms;

        return isSeeking;
    }else{
        hDBG("%p, %p,%d,%d\n", m_playbin, m_pendingVideoSink, m_state, m_seekable);
    }

    return false;
}



bool GstPlayerSession::play()
{
#ifdef DEBUG_PLAYBIN
    qDebug() << Q_FUNC_INFO;
#endif

    m_everPlayed = false;
    if (m_playbin) {
        m_pendingState = GstPlayState::PlayingState;
        if (gst_element_set_state(m_playbin, GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
            qWarning() << "GStreamer; Unable to play -" << m_request_str;
            m_pendingState = m_state = GstPlayState::StoppedState;
            #ifdef TODO_LATER
            niPRN("stateChanged");
            #else
            emit stateChanged(m_state);
            #endif
        } else {
            resumeVideoProbes();
            return true;
        }
    }

    return false;
}



bool GstPlayerSession::pause()
{
#ifdef HSCHOI_DEBUG_PLAYBIN
    qDebug() << Q_FUNC_INFO << " puase ...\n";
#endif
    if (m_playbin) {
        m_pendingState = GstPlayState::PausedState;
        if (m_pendingVideoSink != 0)
            return true;

        if (gst_element_set_state(m_playbin, GST_STATE_PAUSED) == GST_STATE_CHANGE_FAILURE) {
            qWarning() << "GStreamer; Unable to pause -" << m_request_str;
            m_pendingState = m_state = GstPlayState::StoppedState;
            #ifdef TODO_LATER
            niPRN("stateChanged");
            #else
            emit stateChanged(m_state);
            #endif
        } else {

            resumeVideoProbes();

            return true;
        }
    }

    return false;
}


void GstPlayerSession::stop()
{
#ifdef HSCHOI_DEBUG_PLAYBIN
    qDebug() << Q_FUNC_INFO" stop...\n";
#endif
    m_everPlayed = false;
    if (m_playbin) {

        if (m_renderer)
            m_renderer->stopRenderer();

        flushVideoProbes();
        gst_element_set_state(m_playbin, GST_STATE_NULL);

        m_lastPosition = 0;
        GstPlayState oldState = m_state;
        m_pendingState = m_state = GstPlayState::StoppedState;

        finishVideoOutputChange();

        //we have to do it here, since gstreamer will not emit bus messages any more
        setSeekable(false);
        if (oldState != m_state){
            #ifdef TODO_LATER
            niPRN("");
            #else
            emit stateChanged(m_state);
            #endif
        }
    }
}


#ifdef TODO_LATER
void GstPlayerSession::loadFromUri(const string &str)
{
#ifdef HSCHOI_DEBUG_PLAYBIN
    qDebug() << Q_FUNC_INFO << str << ". loading ... \n";
#endif
#if 1
//    m_request = request;

    m_request_str = str;
    m_duration = -1;
    m_lastPosition = 0;


    if (m_playbin) {

        #ifndef TODO_LATER
        m_tags.clear();
        emit tagsChanged();
        #endif

        g_object_set(G_OBJECT(m_playbin), "uri", str.c_str(), NULL);

#if 1
        if (!m_streamTypes.empty()) {
            #ifndef TODO_LATER
            m_streamProperties.clear();
            #endif
            m_streamTypes.clear();

            #ifdef TODO_LATER
            niPRN("emit streamsChanged");
            #else
            emit streamsChanged();
            #endif
        }
#endif
    }
    #endif
}



#else
void GstPlayerSession::loadFromUri(const QNetworkRequest &request)
{
#ifdef DEBUG_PLAYBIN
    qDebug() << Q_FUNC_INFO << request.url();
#endif
    m_request = request;
    m_duration = -1;
    m_lastPosition = 0;

#if QT_CONFIG(gstreamer_app)
    if (m_appSrc) {
        m_appSrc->deleteLater();
        m_appSrc = 0;
    }
#endif

    if (m_playbin) {
        m_tags.clear();
        emit tagsChanged();

        g_object_set(G_OBJECT(m_playbin), "uri", m_request.url().toEncoded().constData(), NULL);

        if (!m_streamTypes.isEmpty()) {
            m_streamProperties.clear();
            m_streamTypes.clear();

            emit streamsChanged();
        }
    }
}

#endif    



