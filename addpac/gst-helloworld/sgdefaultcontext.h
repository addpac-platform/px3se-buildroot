// sgdefaultcontext.h
// SGDEFAULTCONTEXT
#ifndef __SGDEFAULTCONTEXT_H__
#define __SGDEFAULTCONTEXT_H__

#include "sgcontext.h"
#include "sgrendercontext.h"

class SGDefaultContext 
    : public SGContext
    //, public QSGRendererInterface
{
public:
    SGDefaultContext();
    ~SGDefaultContext();

    SGRenderContext *createRenderContext() override;


};

#endif // __SGDEFAULTCONTEXT_H__

