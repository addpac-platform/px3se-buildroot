

#include "apgstmessage.h"



apGstMessage::apGstMessage():
    m_message(0)
{
    //Q_UNUSED(wuchi);
}

apGstMessage::apGstMessage(GstMessage* message):
    m_message(message)
{
    if (m_message != 0)
        gst_message_ref(m_message);
}

apGstMessage::apGstMessage(apGstMessage const& m):
    m_message(m.m_message)
{
    if (m_message != 0)
        gst_message_ref(m_message);
}


apGstMessage::~apGstMessage()
{
    if (m_message != 0)
        gst_message_unref(m_message);
}

GstMessage* apGstMessage::rawMessage() const
{
    return m_message;
}

apGstMessage& apGstMessage::operator=(apGstMessage const& rhs)
{
    if (rhs.m_message != m_message) {
        if (rhs.m_message != 0)
            gst_message_ref(rhs.m_message);

        if (m_message != 0)
            gst_message_unref(m_message);

        m_message = rhs.m_message;
    }

    return *this;
}


