#ifndef __TEST_H__
#define __TEST_H__

#include <gst/gst.h>

#include <assert.h>

#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

void test();

//#define TEST_PLAYBIN_TUTORIAL
//#define TEST_GLIB_MAINLOOP
//default: helloworld


#define TEST_ONLY_MODESET




//#define ENABLE_TEST_FB




#ifdef TEST_PLAYBIN_TUTORIAL
#define TEST_MESSAGE_HANDLER
int main_gst_tutorial_playbin(int argc, char *argv[]) ;
#endif


void test_gst(int *argc, char **argv[]);
void test_glib_mainloop_in_gst(int *argc, char **argv[]);

int test_drm_doublebuf(int argc, char *argv[]);
int test_drm_vsync(int argc, char **argv);

#define     ENABLE_GST_HELPER
//#define     ADD_BUS_HELPER

// #deine   EANBLE_GST_VOLUME


#define EGL_MAJOR_VER   2
#define SUPPORTED_SURFACE_TYPE_EGLFSv2

#define qDebug()      std::cout
#define qWarning()    std::cout
#define DBGOUT        std::ostream

#define STRINGIFY2(x) #x
#define STRINGIFY(x) STRINGIFY2(x)

#define Q_FUNC_INFO __FILE__ ":" STRINGIFY(__LINE__) ":" 

#define Q_NULLPTR   NULL

#define Q_UNLIKELY(x)  __builtin_expect((x),0)




typedef unsigned int    quint32; 


template <typename T>
 inline const T &qMax(const T &a, const T &b) { return (a < b) ? b : a; }






template<typename T, typename R>
int indexOf (const T& list, const R& value)
{
    int index = 0;
    for (typename T::const_iterator it = list.begin(); it != list.end(); ++it, ++index)
    if ((*it) == value)
            return index;
    return -1;
}





#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#define EINTR_LOOP(ret, call) \
    do {                      \
        ret = call;           \
    } while (ret == -1 && errno == EINTR)


static inline int qt_safe_open(const char *pathname, int flags, mode_t mode = 0777)
{
#ifdef O_CLOEXEC
    flags |= O_CLOEXEC;
#endif
    int fd;
    EINTR_LOOP(fd, ::open(pathname, flags, mode));

    // unknown flags are ignored, so we have no way of verifying if
    // O_CLOEXEC was accepted
    if (fd != -1)
        ::fcntl(fd, F_SETFD, FD_CLOEXEC);
    return fd;
}

static inline int qt_safe_close(int fd)
{
    int ret;
    EINTR_LOOP(ret, ::close(fd));
    return ret;
}
    


#if 1 //ndef QT_NO_DEBUG
#define GL_RESET_ERROR()                                \
{                                                         \
    while (glGetError() != GL_NO_ERROR) {} \
}
#define GL_CHECK_ERROR()                                \
{                                                         \
    GLenum err = glGetError(); \
    if (err != GL_NO_ERROR) {                             \
        hDBG("[%s line %d] OpenGL Error: %d",           \
               __FILE__, __LINE__, (int)err);             \
    }                                                     \
}
#else
#define GL_RESET_ERROR() {}
#define GL_CHECK_ERROR() {}
#endif


/*************************************************************************/
#if 1 //defined(CONFIG_DEBUG_HSCHOI_MSG) 
#include <stdio.h>

#define PRINTF printf
#define myPRN(level, fmt, args...) \
    do{ \
    ((void)PRINTF("\t\t" level " : " fmt, ##args)); \
    }while(0)

#define myDBG(level, fmt, args...) \
    do{ \
    ((void)PRINTF("\t\t" level ": %s() " fmt " ... %s@%d\r\n", __FUNCTION__, ##args, __FILE__, __LINE__ )); \
    }while(0)



   
#else
#define myPRN(level, fmt, args...)
#define myDBG(level, fmt, args...)

#endif

#define myWAIT(waitCnt, waitStep, run) \
    { \
        int cnt=0; \
        while (cnt++ < waitCnt) \
        { \
            MDELAY(waitStep); \
            run;\
        } \
    }

#define niPRN(fmt, args...) myDBG("\t Not implmented :", fmt, ##args)


#define hPRN(fmt, args...)       myPRN("\t\t   ", fmt, ##args)
#define hDBG(fmt, args...)       myDBG("\t\tDBG:", fmt, ##args)
#define hINF(fmt, args...)       myPRN("\t\tINF", fmt, ##args)
#define hERR(fmt, args...)       myDBG("\t\tERR", fmt, ##args)
#define hWARN(fmt, args...)       myDBG("\t\tWARN", fmt, ##args)


/*************************************************************************/
//#define __HSCHOI_DEBUG_ION__ 

#if 0
#define d0PRN(fmt, args...) myPRN("\td0", fmt, ##args)
#define d0DBG(fmt, args...) myDBG("\td0", fmt, ##args) 
#define d0WAIT(waitCnt) myWAIT(waitCnt, 10, d0DBG(""))
    
#else
#define d0PRN(fmt, args...)
#define d0DBG(fmt, args...)
#define d0WAIT(waitCnt)
#endif



/*************************************************************************/
// debug busMessage 

#ifdef TEST_GLIB_MAINLOOP
#define d1PRN(fmt, args...) myPRN("\td1", fmt, ##args)
#define d1DBG(fmt, args...) myDBG("\td1", fmt, ##args) 
#define d1WAIT(waitCnt) myWAIT(waitCnt, 10, d1DBG(""))
    
#else
#define d1PRN(fmt, args...)
#define d1DBG(fmt, args...)
#define d1WAIT(waitCnt)
#endif


/*************************************************************************/
// debug gstreamer
#define TEST_GSTREAMER
#if 1
#define gPRN(fmt, args...) myPRN("\tgst", fmt, ##args)
#define gDBG(fmt, args...) myDBG("\tgst", fmt, ##args) 
#define gWAIT(waitCnt) myWAIT(waitCnt, 10, gDBG(""))
    
#else
#define gPRN(fmt, args...)
#define gDBG(fmt, args...)
#define gWAIT(waitCnt)
#endif



#endif // __TEST_H__
