#ifndef __ADDPAC_GST_VIDEO_WINDOW_H__
#define __ADDPAC_GST_VIDEO_WINDOW_H__


#include "gstutils.h"

#include "gstbushelper.h"

#include "gstvideooverlay.h"



class GstVideoRendererInterface
{
public:
    virtual ~GstVideoRendererInterface(){};
    virtual GstElement *videoSink() = 0;

    //stopRenderer() is called when the renderer element is stopped.
    //it can be reimplemented when video renderer can't detect
    //changes to NULL state but has to free video resources.
    virtual void stopRenderer() {}

    //the video output is configured, usually after the first paint event
    //(winId is known,
    virtual bool isReady() const { return true; }

    //signals:
    //void sinkChanged();
    //void readyChanged(bool);
};




class GstVideoWindow:
//        public QVideoWindowControl,
        public GstVideoRendererInterface,
        public GstSyncMessageFilter,
        public GstBusMessageFilter
{
public:
    GstVideoWindow();
    ~GstVideoWindow();

    GstElement *videoSink() override { return m_videoOverlay.videoSink(); }

    bool isReady() const override { return m_windowId != 0; }

    WId winId() const ;
    void setWinId(WId id) ;

    void setDisplayRect(const QRect &rect){
        m_videoOverlay.setRenderRectangle(m_displayRect = rect);
        repaint();
    }
    
    void repaint(){ m_videoOverlay.expose(); }
    void setAspectRatioMode(AspectRatioMode mode){ 
        m_videoOverlay.setAspectRatioMode(mode);
    }



    bool processSyncMessage(const apGstMessage &message) override;
    bool processBusMessage(const apGstMessage &message) override;
//    bool isReady() const override { return m_windowId != 0; }

private:
    apGstVideoOverlay m_videoOverlay;
    WId m_windowId;

    QRect m_displayRect;


};




#endif //__ADDPAC_GST_VIDEO_WINDOW_H__

