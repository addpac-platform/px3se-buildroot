#include "ap_global.h"

#include "openglcontext.h"
#include "eglfsscreen.h"
#include "eglfscontext.h"
#include "eglfsintegration.h"
#include <GLES2/gl2.h>

static OpenGLContext *global_share_context = 0;
static OpenGLContext *current_opengl_context = 0;

/*!
    \internal

    This function is used by Qt::AA_ShareOpenGLContexts and the Qt
    WebEngine to set up context sharing across multiple windows. Do
    not use it for any other purpose.

    Please maintain the binary compatibility of these functions.
*/
void qt_gl_set_global_share_context(OpenGLContext *context)
{
    global_share_context = context;
}

/*!
    \internal
*/
OpenGLContext *qt_gl_global_share_context()
{
    return global_share_context;
}






OpenGLContext::OpenGLContext(PlatformScreen *screen)
    : m_screen(screen)
    , m_shareContext(0)
    , platformGLContext(0)
{

}




OpenGLContext::~OpenGLContext()
{
    destroy();
}

void OpenGLContext::destroy()
{
    hDBG("not yet ");
}

bool OpenGLContext::create()
{
    hDBG("");
    assert(m_screen);
    

    if(platformGLContext) destroy();

    platformGLContext = AppPrivate::createPlatformOpenGLContext(this);
    assert(platformGLContext);

    platformGLContext->initialize(); // EGLPlatformContext::initialize

    platformGLContext->setContext(this);

    #if 0 // TODO: ??
        if (!d->platformGLContext->isSharing())
        d->shareContext = 0;
    d->shareGroup = d->shareContext ? d->shareContext->shareGroup() : new QOpenGLContextGroup;
    d->shareGroup->d_func()->addContext(this);
    qMyDbg("done");

    #endif

    return true;
}


OpenGLContext *OpenGLContext::currentContext(){
    return current_opengl_context;
}

OpenGLContext *OpenGLContext::setCurrentContext(OpenGLContext *new_context){
    OpenGLContext *pre = current_opengl_context;
    current_opengl_context = new_context;
    return pre;
}



bool OpenGLContext::makeCurrent(ASurface *surface)
{
    if (!surface->surfaceHandle()){
        assert(0);
        return false;
    }

    OpenGLContext *pre = OpenGLContext::setCurrentContext(this);
    hDBG("%p, %p",surface, surface->surfaceHandle());
    
    if(platformGLContext->makeCurrent(surface->surfaceHandle())){
        // TODO: functions

        const char *rendererString = (const char *)glGetString(GL_RENDERER); //
        
        hDBG("rendererString:%s", rendererString);
        // TODO: workaround_brokenFBOReadBack = true;
        
        hDBG("done");
        return true;
    }

    OpenGLContext::setCurrentContext(pre);
    return false;
}


GLuint OpenGLContext::defaultFramebufferObject() const
{
    #if 0
    if (!isValid())
        return 0;

    Q_D(const QOpenGLContext);
    if (!d->surface || !d->surface->surfaceHandle())
        return 0;

    if (d->defaultFboRedirect)
        return d->defaultFboRedirect;

    return d->platformGLContext->defaultFramebufferObject(d->surface->surfaceHandle());
    #else
    return 0;
    #endif
}

extern void waitForVSync(PlatformSurface *surface);
extern void presentBuffer(PlatformSurface *surface);


void OpenGLContext::swapBuffers(ASurface *surface)
{
    PlatformSurface *surfaceHandle = surface->surfaceHandle();
    if (!surfaceHandle){
        hERR("invalid handle");
        //printf("platformWindow:%p\n", platformWindow);

        //swapBuffers(static_cast<ASurface*>(platformWindow));
        return;
    }

    printf("%p, %p", surface, surfaceHandle);

    waitForVSync(surfaceHandle);
    platformGLContext->swapBuffers(surfaceHandle);
    presentBuffer(surfaceHandle);    
    
}
