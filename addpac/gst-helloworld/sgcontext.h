// SGCONTEXT.h
#ifndef __SGCONTEXT_H__
#define __SGCONTEXT_H__


#include "sgrendercontext.h"
#include "sgcontext.h"

class SGRenderContext;

class SGContext 
{

public:
    enum AntialiasingMethod {
        UndecidedAntialiasing,
        VertexAntialiasing,
        MsaaAntialiasing
    };

    explicit SGContext();
    virtual ~SGContext();

    

    virtual SGRenderContext *createRenderContext() = 0;
    
    
    static SGContext *createDefaultContext();

};


#endif //__SGCONTEXT_H__

