#ifndef __SURFACE_FORMAT_H__
#define __SURFACE_FORMAT_H__

#include "test.h"

class SurfaceFormat
{
    
public:
    enum FormatOption {
        StereoBuffers            = 0x0001,
        DebugContext             = 0x0002,
        DeprecatedFunctions      = 0x0004,
        ResetNotification        = 0x0008
    };
    
    enum SwapBehavior {
        DefaultSwapBehavior,
        SingleBuffer,
        DoubleBuffer,
        TripleBuffer
    };
    
    enum RenderableType {
        DefaultRenderableType = 0x0,
        OpenGL                = 0x1,
        OpenGLES              = 0x2,
        OpenVG                = 0x4
    };
    
    enum OpenGLContextProfile {
        NoProfile,
        CoreProfile,
        CompatibilityProfile
    };

    SurfaceFormat();
    ///*implicit*/ SurfaceFormat(FormatOptions options);
    SurfaceFormat(const SurfaceFormat &other);
    //SurfaceFormat &operator=(const SurfaceFormat &other);
    ~SurfaceFormat();
#if 0
    int redBufferSize() const;
    int depthBufferSize() const;
    int stencilBufferSize() const;
    int redBufferSize() const;
    int greenBufferSize() const;
    int blueBufferSize() const;
    int alphaBufferSize() const;
#endif
    int samples() const;

    
    int redBufferSize;
    int greenBufferSize;
    int blueBufferSize;
    int alphaBufferSize;
    int depthBufferSize;
    int stencilBufferSize;
    SurfaceFormat::SwapBehavior swapBehavior;
    int numSamples; //numSamples;
    SurfaceFormat::RenderableType renderableType;
    SurfaceFormat::OpenGLContextProfile profile;
    int swapInterval;
    int majorVersion;
    int minorVersion;

private:
    friend DBGOUT& operator<<(DBGOUT &, const SurfaceFormat &);


};

#endif //__SURFACE_FORMAT_H__

