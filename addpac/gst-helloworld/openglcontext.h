#ifndef __OPENGL_CONTEXT_H__
#define __OPENGL_CONTEXT_H__
#include "test.h"

#include "surfaceformat.h"
#include "eglconvenience.h"
#include "asurface.h"

#include <GLES2/gl2.h>


class PlatformScreen;
class SurfaceFormat;
class PlatformOpenGLContext;

class OpenGLContext{
public:
    //OpenGLContext();
    OpenGLContext(PlatformScreen *);

    ~OpenGLContext();

    void setShareContext(OpenGLContext *shareContext){
        m_shareContext = shareContext;
    }
    void setFormat(const SurfaceFormat &format){
        requestedFormat = format;
    }
    SurfaceFormat format(){ return requestedFormat; }
    

    PlatformScreen *screen(){ return m_screen; }
    bool create();
    void destroy();
    EGLNativeContext nativeHandle(){ return m_hNative; }
    void setNativeHandle(EGLNativeContext h){ m_hNative = h; 
        hDBG("%p,%p", m_hNative.context(), m_hNative.display());
    }

    OpenGLContext* shareHandle(){ return m_shareContext; }

    bool makeCurrent(ASurface *surface);

    static OpenGLContext *currentContext();
    static OpenGLContext *setCurrentContext(OpenGLContext *context);

    GLuint defaultFramebufferObject() const;

    void swapBuffers(ASurface *surface);



private:
    PlatformScreen *m_screen;

    SurfaceFormat requestedFormat;
    PlatformOpenGLContext *platformGLContext;
    OpenGLContext *m_shareContext;

    EGLNativeContext m_hNative;
    
};


extern void qt_gl_set_global_share_context(OpenGLContext *context);
extern OpenGLContext *qt_gl_global_share_context();

#endif //__OPENGL_CONTEXT_H__

