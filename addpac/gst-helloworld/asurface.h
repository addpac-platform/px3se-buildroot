#ifndef __A_SURFACE_H__
#define __A_SURFACE_H__

// abstract surface 
#include "platformsurface.h"

class PlatformSurface;

class ASurface
{
public:
    enum SurfaceClass {
        Window,
        Offscreen
    };

    enum SurfaceType {
        RasterSurface,
        OpenGLSurface,
        RasterGLSurface,
        OpenVGSurface,
    };

    virtual ~ASurface();
    SurfaceClass surfaceClass() const {
        return m_type;
    }

    //virtual QSurfaceFormat format() const = 0;
    virtual PlatformSurface *surfaceHandle() const = 0;
#if 0

    virtual SurfaceType surfaceType() const = 0;
    bool supportsOpenGL() const;

    virtual QSize size() const = 0;
#endif
protected:
    explicit ASurface(SurfaceClass type);
    SurfaceClass m_type;

    //QSurfacePrivate *m_reserved;
};




#endif // __A_SURFACE_H__


