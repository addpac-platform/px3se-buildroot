#ifndef __EGLFS_KMS_GBM_DEVICE_H__
#define __EGLFS_KMS_GBM_DEVICE_H__


#include "eglfskmsdevice.h"
#include <gbm.h>


class EglFSKmsGbmDevice: public EglFSKmsDevice
{
public:
    EglFSKmsGbmDevice(KmsScreenConfig *screenConfig, const string &path);

    bool open() override;

    void *nativeDisplay() const override;
    PlatformScreen *createScreen(const KmsOutput &output) override;
    gbm_device *gbmDevice() const;



    void handleDrmEvent();
    
private:
    gbm_device *m_gbm_device;


    static void pageFlipHandler(int fd,
                unsigned int sequence,
                unsigned int tv_sec,
                unsigned int tv_usec,
                void *user_data);


};



#endif // __EGLFS_KMS_GBM_DEVICE_H__

