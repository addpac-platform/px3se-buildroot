
#include "test.h"
#include "gstutils.h"
#include "quickrendercontrol.h"


SGContext *QuickRenderControl::sg = 0;

QuickRenderControl::QuickRenderControl()
    : renderer(nullptr)
    , rc(nullptr)
{
    hDBG("");

    if (!sg) {
        //qAddPostRoutine(cleanup);
        sg = SGContext::createDefaultContext();
    }
    hDBG(" createRenderContext"); 
    rc = sg->createRenderContext();
}

/*!
  Destroys the instance. Releases all scenegraph resources.

  \sa invalidate()
 */
QuickRenderControl::~QuickRenderControl()
{
    #if 0
    invalidate();

    if (d->window)
        QQuickWindowPrivate::get(d->window)->renderControl = 0;

    // It is likely that the cleanup in windowDestroyed() is not called since
    // the standard pattern is to destroy the rendercontrol before the QQuickWindow.
    // Do it here.
    d->windowDestroyed();

    delete d->rc;
    #endif
}


void QuickRenderControl::initialize(OpenGLContext *gl)
{
    rc->initialize(gl);
}

bool QuickRenderControl::sync()
{
    #if 1
    //    cd->syncSceneGraph();
    // QQuickWindowPrivate::syncSceneGraph
    if(!renderer){
       renderer = rc->createRenderer(); 

       //renderer->setClearColor(clearColor);

    }
    //rc->endSync();
    #endif
}


// void QQuickWindowPrivate::renderSceneGraph(const QSize &size)
void QuickRenderControl::render()
{
    // TODO: get size of window
    QSize size(1920, 1080);

    int fboId=0;
    
    if(!renderer) return;

    QRect rect(QPoint(0, 0), size);
    renderer->m_device_rect = rect;
    renderer->m_viewport_rect = rect;
    
    #if 0
    renderer->setProjectionMatrixToRect(QRect(QPoint(0, 0), size));
    renderer->setDevicePixelRatio(devicePixelRatio);
    #endif

    
    rc->renderNextFrame(renderer, fboId);


    
}

