// SGDEFAULTRENDERCONTEXT.h
#ifndef __SGDEFAULTRENDERCONTEXT_H__
#define __SGDEFAULTRENDERCONTEXT_H__

#include "sgrendercontext.h"
#include "openglcontext.h"

class SGRenderer;


class SGDefaultRenderContext : public SGRenderContext
{
public:
    SGDefaultRenderContext(SGContext *context);

    virtual void initialize(void *context) override;

    virtual SGRenderer *createRenderer() override;

    void renderNextFrame(SGRenderer *renderer, unsigned int fboId) override;


protected:
    OpenGLContext *m_gl;
};



#endif //__SGDEFAULTRENDERCONTEXT_H__

