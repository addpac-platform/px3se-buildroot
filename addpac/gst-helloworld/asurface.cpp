
#include "asurface.h"


ASurface::ASurface(SurfaceClass type)
    : m_type(type)
    //, m_reserved(0)
{
}

/*!
    Destroys the surface.
*/
ASurface::~ASurface()
{
    #if 0
#ifndef QT_NO_OPENGL
    QOpenGLContext *context = QOpenGLContext::currentContext();
    if (context && context->surface() == this)
        context->doneCurrent();
#endif
#endif
}

#if 0
/*!
   Returns the surface class of this surface.
 */
QSurface::SurfaceClass QSurface::surfaceClass() const
{
    return m_type;
}
#endif
