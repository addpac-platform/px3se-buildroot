#ifndef __AP_GLOBAL_H__
#define __AP_GLOBAL_H__


#include  "test.h"
#include "surfaceformat.h"

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "offscreensurface.h"


class PlatformScreen;
class PlatformOpenGLContext;
class OpenGLContext;
class PlatformOffscreenSurface;

class AppPrivate
{
public :
    AppPrivate();
    ~AppPrivate();
    void addScreen(PlatformScreen *s, bool isPrimary);
    PlatformScreen *getTopScreen();
    static SurfaceFormat surfaceFormatFor(const SurfaceFormat &inputFormat);
    static bool supportsSurfacelessContexts(){ return true; }
    static PlatformOpenGLContext *createPlatformOpenGLContext(OpenGLContext *context);

    static PlatformOffscreenSurface *createPlatformOffscreenSurface(OffscreenSurface *surface);

private:
    
    vector<PlatformScreen *> screen_list;
};






#endif //__AP_GLOBAL_H__

