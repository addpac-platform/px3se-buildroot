#ifndef __EGLFS_KMS_DEVICE_H__
#define __EGLFS_KMS_DEVICE_H__

#include "test.h"


#include "kmsdevice.h"
#include "platformscreen.h"



class EglFSKmsDevice : public KmsDevice
{
public:
    EglFSKmsDevice(KmsScreenConfig *screenConfig, const string &path);
#if 1
    void registerScreen(PlatformScreen *screen,
                        bool isPrimary,
                        const QPoint &virtualPos,
                        const vector<PlatformScreen *> &virtualSiblings) override;
#endif
    //friend class AppPrivate;

};



#endif // __EGLFS_KMS_DEVICE_H__

