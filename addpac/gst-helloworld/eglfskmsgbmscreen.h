#ifndef __EGLFS_KMS_GBM_SCREEN_H__
#define __EGLFS_KMS_GBM_SCREEN_H__

#include "eglfskmsscreen.h"
#include <gbm.h>


class EglFSKmsGbmScreen : public EglFSKmsScreen
{
public:
    EglFSKmsGbmScreen(KmsDevice *device, const KmsOutput &output);
    ~EglFSKmsGbmScreen();

    gbm_surface *surface() const { return m_gbm_surface; }
    gbm_surface *createSurface();
    void destroySurface();


    void waitForFlip() override;
    void flip() override;
    void flipFinished() override;


private:
    gbm_surface *m_gbm_surface;

    gbm_bo *m_gbm_bo_current;
    gbm_bo *m_gbm_bo_next;


    struct FrameBuffer {
        FrameBuffer() : fb(0) {}
        unsigned int fb;
        void *map_buf;
        void *kms_bo;
        // kms_bo
        int w;
        int h;
        int p;
        int id;
    };
    static void bufferDestroyedHandler(gbm_bo *bo, void *data);
    FrameBuffer *framebufferForBufferObject(gbm_bo *bo);


    static mutex m_waitForFlipMutex;

};



#endif // __EGLFS_KMS_GBM_SCREEN_H__

