
#include "gstbushelper.h"

#include "test.h"
#include "gstplayersession.h"



static GstBusSyncReply syncGstBusFilter(GstBus* bus, GstMessage* message, GstBusHelper *d)
{
//    Q_UNUSED(bus);
    QMutexLocker lock(&d->filterMutex);

#if 1
    for (GstSyncMessageFilter *filter : d->syncFilters) {
        if (filter->processSyncMessage(apGstMessage(message)))
            return GST_BUS_DROP;
    }
#endif
    return GST_BUS_PASS;
}


static gboolean busCallback(GstBus *bus, GstMessage *message, gpointer data)
{
    d1DBG("------ ");

    //qMyDbg("");
//    Q_UNUSED(bus);
    reinterpret_cast<GstBusHelper*>(data)->queueMessage(message);
    d1DBG("done");
    return TRUE;
}




static void doProcessBusMessage(GstBusHelper *p)
{
    hDBG("thread is created.");
    
    while(p->enableThread){

        // pending if empty message
        apGstMessage msg = p->queueFront();

        if(!p->queueIsEmpty()){
            
            for (GstBusMessageFilter *filter : p->busFilters) {
                //qMyDbg("%d", cnt++);
                if (filter->processBusMessage(msg))
                    break;
            }

            p->queuePop(); // remote element
        }
    }

    hDBG("end of thread.");

}


#include <glib.h>

static GThread *my_thread;
static GMainLoop *my_loop;

static gpointer
loop_func (gpointer data)
{
  GMainLoop *loop = (GMainLoop*) data;
  d1DBG("start run main loop\n");
  g_main_loop_run (loop);
  d1DBG("stop run main loop\n");
  return NULL;
}

static void
start_my_thread (void)
{
  GMainContext *context;

  context = g_main_context_new ();
  my_loop = g_main_loop_new (context, FALSE);
  g_main_context_unref (context);

  my_thread = g_thread_create (loop_func, my_loop, TRUE, NULL);
}

static void
stop_my_thread (void)
{
  g_main_loop_quit (my_loop);
  g_thread_join (my_thread);
  g_main_loop_unref (my_loop);
}



GstBusHelper::GstBusHelper(GstBus* bus, void* parent):
    m_tag(0),
    doThread(0),        
    m_bus(bus)        
{
    assert (bus != NULL);
    assert (GST_IS_BUS (bus));
    
    d1DBG("suppose that hasGlib: register busCallback");

    
    m_tag = gst_bus_add_watch_full(bus, G_PRIORITY_DEFAULT, busCallback, this, NULL);
    //m_tag = gst_bus_add_watch (bus, (GstBusFunc)busCallback, this);
    assert(m_tag != 0);


#if GST_CHECK_VERSION(1,0,0)
    gst_bus_set_sync_handler(bus, (GstBusSyncHandler)syncGstBusFilter, this, 0);
#else
    gst_bus_set_sync_handler(bus, (GstBusSyncHandler)syncGstBusFilter, this);
#endif
    gst_object_ref(GST_OBJECT(bus));

    enableThread = true;
    Int32 status;
    status = OSA_semCreate(&rdSem, 1, 0);
    OSA_assert(status==OSA_SOK);

    
    doThread = new std::thread(&doProcessBusMessage, this);

    //start_my_thread();


}

GstBusHelper::~GstBusHelper()
{

    //stop_my_thread();
    
    if (m_tag)
        g_source_remove(m_tag);

#if GST_CHECK_VERSION(1,0,0)
    gst_bus_set_sync_handler(m_bus, 0, 0, 0);
#else
    gst_bus_set_sync_handler(m_bus,0,0);
#endif
    gst_object_unref(GST_OBJECT(m_bus));

    if(doThread){
        enableThread = false;
        OSA_semSignal(&rdSem);
        doThread->join();
    }

    OSA_semDelete(&rdSem);

    hDBG("done.");

}

template <class T>
void _install_filter(list<T*> &filters, T *filter)
{
    bool findflag=false;
    for (T *iter : filters) {
        if(iter == filter) {
            findflag = true;
            break;
        }
    }

    if(!findflag){
    //if(1){
        filters.push_back(filter);
        //hDBG("install Filter");
    }else{
        //hDBG("already exist ");
    }        
}



template <class T>
static void _uninstall_all_filter(list<T*> &filters, T *filter)
{
	for(auto it = filters.begin(); it != filters.end(); ){
        if(*it == filter) {
            it =filters.erase(it);
            //hDBG("erase Filter:0x%08x ", filter);
        }else
            ++it;
    }
}


#define INSTALL_FILTER(filter) \
    GstSyncMessageFilter *syncFilter = dynamic_cast<GstSyncMessageFilter*>(filter); \
    if (syncFilter) { \
        QMutexLocker lock(&filterMutex); \
        _install_filter<GstSyncMessageFilter>(syncFilters, syncFilter); \
    } \
    GstBusMessageFilter *busFilter = dynamic_cast<GstBusMessageFilter*>(filter); \
    if(busFilter){ \
        _install_filter<GstBusMessageFilter>(busFilters, busFilter); \
    }

#define UNINSTALL_FILTER(filter) \
    GstSyncMessageFilter *syncFilter = dynamic_cast<GstSyncMessageFilter*>(filter); \
    if (syncFilter) { \
        QMutexLocker lock(&filterMutex); \
        _uninstall_all_filter<GstSyncMessageFilter>(syncFilters, syncFilter); \
    } \
    GstBusMessageFilter *busFilter = dynamic_cast<GstBusMessageFilter*>(filter); \
    if(busFilter){ \
        _uninstall_all_filter<GstBusMessageFilter>(busFilters, busFilter); \
    }


void GstBusHelper::installMessageFilter(GstPlayerSession *filter){
    hDBG("install GstPlayerSession filter\r\n");
    INSTALL_FILTER(filter);
}

void GstBusHelper::removeMessageFilter(GstPlayerSession *filter){
    hDBG("remove GstPlayerSession filter\r\n");    
    UNINSTALL_FILTER(filter);
}

void GstBusHelper::installMessageFilter(GstVideoWindow *filter){
    hDBG("install GstVideoWindow filter\r\n");    
    INSTALL_FILTER(filter);
}
void GstBusHelper::removeMessageFilter(GstVideoWindow *filter){
    hDBG("remove GstVideoWindow filter\r\n");
    UNINSTALL_FILTER(filter);
}
    


void GstBusHelper::queueMessage(GstMessage* message)
{
//        qMyDbg("");        
    apGstMessage msg(message);
#ifdef ADD_BUS_HELPER
    QMetaObject::invokeMethod(this, "doProcessMessage", Qt::QueuedConnection,
                              Q_ARG(QGstreamerMessage, msg));
#else

    #ifdef ENABLE_THREAD
    msgQ.push(msg);
    hDBG("queue message:0x%x", GST_MESSAGE_TYPE(message));
    // signal thread processing message
    OSA_semSignal(&rdSem);
    #else
    niPRN(" queue message ");
    #endif
#endif
}


