#ifndef __QUICK_RENDER_CONTROL_H__
#define __QUICK_RENDER_CONTROL_H__

#include "sgcontext.h"
#include "sgrendercontext.h"
#include "sgrenderer.h"

class OpenGLContext;

class QuickRenderControl
{
public:
    explicit QuickRenderControl();
    ~QuickRenderControl();

    void initialize(OpenGLContext *gl);


    bool sync();
    void render();

private:
    static SGContext *sg;
    SGRenderContext *rc;

    //
    SGRenderer *renderer;


};


#endif //__QUICK_RENDER_CONTROL_H__


