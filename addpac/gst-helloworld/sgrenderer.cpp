// sgrenderer.cpp
#include "sgrenderer.h"

#include "sgrendercontext.h"
#include "openglframebufferobject.h"

#include <GLES2/gl2.h>


void SGBindable::clear(SGRenderer::ClearModeBit mode) const
{

    GLuint bits = 0;
    if (mode & SGRenderer::ClearColorBuffer) bits |= GL_COLOR_BUFFER_BIT;
    if (mode & SGRenderer::ClearDepthBuffer) bits |= GL_DEPTH_BUFFER_BIT;
    if (mode & SGRenderer::ClearStencilBuffer) bits |= GL_STENCIL_BUFFER_BIT;
    glClear(bits);
}

// Reactivate the color buffer after switching to the stencil.
void SGBindable::reactivate() const
{

    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

}

SGBindableFboId::SGBindableFboId(GLuint id)
    : m_id(id)
{
}


void SGBindableFboId::bind() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
}



SGRenderer::SGRenderer(SGRenderContext *context)
    : m_current_opacity(1)
    , m_current_determinant(1)
    , m_device_pixel_ratio(1)
    , m_context(context)
//    , m_node_updater(0)
//    , m_bindable(0)
    , m_changed_emitted(false)
    , m_is_rendering(false)
{
}


SGRenderer::~SGRenderer()
{
//    setRootNode(0);
//    delete m_node_updater;
}


void SGRenderer::renderScene(unsigned int fboId)
{
#if 1
    if (fboId) {
        SGBindableFboId bindable(fboId);
        renderScene(bindable);
    } else {
        class B : public SGBindable
        {
        public:
            void bind() const { OpenGLFramebufferObject::bindDefault(); }
        } bindable;
        renderScene(bindable);
    }
#endif    
}

void SGRenderer::renderScene(const SGBindable &bindable)
{

    hDBG(" TODO: later ......");
    // Renderer::render()
    render();
    
}
