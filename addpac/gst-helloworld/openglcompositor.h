#ifndef __OPENGL_COMPOSITOR_H__
#define __OPENGL_COMPOSITOR_H__

#include "test.h"
#include "gstutils.h"
#include "awindow.h"

#include "opengltextureblitter.h"

class OpenGLContext;
class OpenGLFramebufferObject;


class OpenGLCompositor
{
public:
    static OpenGLCompositor *instance();
    static void destroy();

    void setTarget(OpenGLContext *context, AWindow *targetWindow,
                                  const QRect &nativeTargetGeometry);



    void renderAll(OpenGLFramebufferObject *fbo=0);

private:
    OpenGLCompositor();
    ~OpenGLCompositor();

    //void render(OpenGLCompositorWindow *window);




    OpenGLContext *m_context;
    AWindow *m_targetWindow;
    QRect m_nativeTargetGeometry;
    int m_rotation;


    OpenGLTextureBlitter m_blitter;


    
};

#endif // __OPENGL_COMPOSITOR_H__

