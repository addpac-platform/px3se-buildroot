// SGRENDERCONTEXT.h
#ifndef __SGRENDERCONTEXT_H__
#define __SGRENDERCONTEXT_H__

#include "sgcontext.h"

class SGContext;
class SGRenderer;
class SGRenderContext 
{
public:
    enum CreateTextureFlags {
        CreateTexture_Alpha       = 0x1,
        CreateTexture_Atlas       = 0x2,
        CreateTexture_Mipmap      = 0x4
    };

    SGRenderContext(SGContext *context);
    virtual ~SGRenderContext();

    virtual void initialize(void *context) =0;

    virtual SGRenderer *createRenderer() = 0;
    virtual void renderNextFrame(SGRenderer *renderer, unsigned int fboId) = 0;


protected:
    SGContext *m_sg;


};

#endif // __SGRENDERCONTEXT_H__

