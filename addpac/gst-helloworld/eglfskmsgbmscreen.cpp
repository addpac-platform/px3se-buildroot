
#include "eglfskmsgbmscreen.h"
#include "eglfskmsgbmdevice.h"

#include "gbm.h"
#include <sys/mman.h>
#include "osa.h"
#include <GLES2/gl2.h>


#define DEMO_FLIP_1SEC // wait 1sec before flipping

#define ENABLE_FB_ONCE // use 1 framebuffer

#define ENABLE_KMS  // test for framebuffer 

#ifdef ENABLE_KMS
#include <libkms/libkms.h>

#endif

mutex EglFSKmsGbmScreen::m_waitForFlipMutex;


void EglFSKmsGbmScreen::bufferDestroyedHandler(gbm_bo *bo, void *data)
{
    FrameBuffer *fb = static_cast<FrameBuffer *>(data);

    if (fb->fb) {
        gbm_device *device = gbm_bo_get_device(bo);
        if(fb->map_buf && fb->kms_bo){            
            kms_bo_unmap(fb->kms_bo);
            kms_bo_destroy(&fb->kms_bo);            
        }
        drmModeRmFB(gbm_device_get_fd(device), fb->fb);

        hDBG("");
    }

    delete fb;
}

extern void draw_buffer(char *addr, int w, int h, int pitch);
extern void draw_buffer2(char *addr, int w, int h, int pitch, int color);

extern void clear_buffer(char *addr, int w, int h, int pitch);

extern void draw_buffer_with_cairo(char *addr, int w, int h, int pitch);

static void draw_bufferx(char *addr, int w, int h, int pitch)
{
	int ret, i, j;
    static int cnt = 0;
    if(cnt++%2 == 0){
        draw_buffer(addr, w, h, pitch);
    }
    else {
        draw_buffer_with_cairo(addr, w, h, pitch);

    }
}
#ifdef ENABLE_KMS
typedef void (*draw_func_t)(char *addr, int w, int h, int pitch);



void *create_bufobj(struct kms_driver *kms_driver, 
	int w, int h, int *out_pitch, int *out_handle, 
	void **buf,
	draw_func_t draw)
{
	void *map_buf;
	struct kms_bo *bo;
	int pitch, handle;
	unsigned bo_attribs[] = {
		KMS_WIDTH,   w,
		KMS_HEIGHT,  h,
		KMS_BO_TYPE, KMS_BO_TYPE_SCANOUT_X8R8G8B8,
		KMS_TERMINATE_PROP_LIST
	};
	int ret;

	/* ceate kms buffer object, opaque struct identied by struct kms_bo pointer */
	ret = kms_bo_create(kms_driver, bo_attribs, &bo);
	if(ret){
		fprintf(stderr, "kms_bo_create failed: %s\n", strerror(errno));
		goto exit;
	}

	/* get the "pitch" or "stride" of the bo */
	ret = kms_bo_get_prop(bo, KMS_PITCH, &pitch);
	if(ret){
		fprintf(stderr, "kms_bo_get_prop KMS_PITCH failed: %s\n", strerror(errno));
		goto free_bo;
	}

	/* get the handle of the bo */
	ret = kms_bo_get_prop(bo, KMS_HANDLE, &handle);
	if(ret){
		fprintf(stderr, "kms_bo_get_prop KMS_HANDL failed: %s\n", strerror(errno));
		goto free_bo;
	}

	/* map the bo to user space buffer */
	ret = kms_bo_map(bo, &map_buf);
	if(ret){
		fprintf(stderr, "kms_bo_map failed: %s\n", strerror(errno));
		goto free_bo;
	}

	draw(map_buf, w, h, pitch);




	ret = 0;
	//*out_kms_bo = bo;
	*out_pitch = pitch;
	*out_handle = handle;
    *buf = map_buf;
	goto exit;

free_bo:
	kms_bo_unmap(bo);
	kms_bo_destroy(&bo);
    return NULL;
exit:
	return bo;
}
#endif // ENABLE_KMS

int flag=0;
EglFSKmsGbmScreen::FrameBuffer *EglFSKmsGbmScreen::framebufferForBufferObject(gbm_bo *bo)
{
#if 1//def ENABLE_KMS
    static int cnt=0;
#endif
    int ret ;



    
    {
        FrameBuffer *fb = static_cast<FrameBuffer *>(gbm_bo_get_user_data(bo));
        if (fb){
            printf("draw id:%d\n", fb->id);
            cnt++;
            #if 0
            if( cnt < 100){
                //clear_buffer(fb->map_buf, 1920, 1080, 1920*4);
                memset(fb->map_buf, 0, 1920*1080*4);

                draw_buffer(fb->map_buf + cnt*fb->p + cnt*4, fb->w, fb->h, fb->p);
                //OSA_waitMsecs(1000);
            }
            else if(cnt < 300){
                int idx = cnt - 100;
                if(cnt > 200)
                    memset(fb->map_buf, 0, 1920*1080*4);
                
                draw_buffer_with_cairo(fb->map_buf + idx*fb->p + idx*4, fb->w, fb->h, fb->p);
            }
            //else if(cnt > 300)
             #else
             static int color[4] = { 0xFF0000FF, 0xFF00FF00, 0xFFFF0000, 0xFFFFFFFF };
             draw_buffer2(fb->map_buf + cnt*fb->p + cnt*4, fb->w, fb->h, fb->p, color[cnt%4]);
             #endif
            return fb;
        }
    }

    unsigned int width = gbm_bo_get_width(bo);
    unsigned int height = gbm_bo_get_height(bo);
    unsigned int stride = gbm_bo_get_stride(bo);
    unsigned int handle = gbm_bo_get_handle(bo).u32;

    FrameBuffer *fb = new FrameBuffer;

#ifdef ENABLE_KMS // test 
    //if(cnt++ % 3 ==0)
    // if(flag < 300)
    {
                printf("================ %d:draw\r\n", flag);
    //device();
    struct kms_driver *kms_driver;
	ret = kms_create(device()->fd(), &kms_driver);
	if(ret){
		fprintf(stderr, "kms_create failed: %s\n", strerror(errno));
        assert(0);
	}
    else{
        hDBG("kms_driver:%p", kms_driver);
    }
    
    int pitch, bo_handle, kms_bo;
	void *map_buf = NULL;
    static int id=0;
	fb->kms_bo = create_bufobj(kms_driver, width, height, 
		&pitch, &bo_handle, &map_buf,
		//draw_buffer_with_cairo);
		draw_bufferx);
    fb->map_buf = map_buf;
    
    fb->w = width;
    fb->h = height;
    fb->p = pitch;
    fb->id = id++;

    hDBG("bo_handle:%p, handle:%p ", bo_handle, handle);
    // if you want to show drawed buffer, 
    //  set handle = bo_handle
    handle = bo_handle;
    }
#elif 0
{
	/* map the bo to user space buffer */
    // ERROR: unknown symbol : libgbm  -> libMali (we don't have source code)
	void *map_buf;

	ret = gbm_bo_map(bo, 0, 0, width, height, 0, &stride, &map_buf);
	if(ret){
		fprintf(stderr, "gbm_bo_map failed: %s\n", strerror(errno));
        assert(0);
		//goto free_bo;
	}

	draw_bufferx(map_buf, width, height, stride);

	gbm_bo_unmap(bo, map_buf);
}

#elif 0
{
    struct drm_mode_map_dumb mreq = drm_mode_map_dumb();
    mreq.handle = handle;
    ret = drmIoctl(device()->fd(), DRM_IOCTL_MODE_MAP_DUMB, &mreq);
    if(ret){
        assert(0);
    }
    char *map;
    map = (uint8_t *)mmap(0, width*height*stride, PROT_READ | PROT_WRITE, MAP_SHARED,
                      device()->fd(), mreq.offset);
    if (map == MAP_FAILED)
    {
        assert(0);
    }
    
	draw_bufferx(map, width, height, stride);
    munmap(map, width*height*stride);
}

#endif 

    ret = drmModeAddFB(device()->fd(), width, height, 32, 32,
                           stride, handle, &fb->fb);

    if (ret) {
        hWARN("Failed to create KMS FB!");
        return Q_NULLPTR;
    } else
    	hWARN("eglfs >> sucess to create KMS FB!");

    gbm_bo_set_user_data(bo, fb, bufferDestroyedHandler);
    return fb;
}



EglFSKmsGbmScreen::EglFSKmsGbmScreen(KmsDevice *device, const KmsOutput &output)
    : EglFSKmsScreen(device, output)
    , m_gbm_surface(Q_NULLPTR)
    , m_gbm_bo_current(Q_NULLPTR)
    , m_gbm_bo_next(Q_NULLPTR)
//    , m_cursor(Q_NULLPTR)
{
    hDBG("");
}

EglFSKmsGbmScreen::~EglFSKmsGbmScreen()
{
#if 0    
    const int remainingScreenCount = qGuiApp->screens().count();
    qCDebug(qLcEglfsKmsDebug, "Screen dtor. Remaining screens: %d", remainingScreenCount);
    if (!remainingScreenCount && !device()->screenConfig()->separateScreens())
        static_cast<EglFSKmsGbmDevice *>(device())->destroyGlobalCursor();
    #endif
}


gbm_surface *EglFSKmsGbmScreen::createSurface()
{
    if (!m_gbm_surface) {
        qDebug() << "Creating window for screen" << name() << endl;
        m_gbm_surface = gbm_surface_create(static_cast<EglFSKmsGbmDevice *>(device())->gbmDevice(),
                                           rawGeometry().width(),
                                           rawGeometry().height(),
                                           GBM_FORMAT_ARGB8888,
                                           GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
	    hDBG("eglfs >> createSurface GBM_FORMAT_ARGB8888");
    }
    return m_gbm_surface;
}

void EglFSKmsGbmScreen::destroySurface()
{
    if (m_gbm_bo_current) {
        gbm_bo_destroy(m_gbm_bo_current);
        m_gbm_bo_current = Q_NULLPTR;
    }

    if (m_gbm_bo_next) {
        gbm_bo_destroy(m_gbm_bo_next);
        m_gbm_bo_next = Q_NULLPTR;
    }

    if (m_gbm_surface) {
        gbm_surface_destroy(m_gbm_surface);
        m_gbm_surface = Q_NULLPTR;
    }
}



void EglFSKmsGbmScreen::waitForFlip()
{

    hDBG("");
    // Don't lock the mutex unless we actually need to
    if (!m_gbm_bo_next)
        return;

    QMutexLocker lock(&m_waitForFlipMutex);

    while (m_gbm_bo_next)
        static_cast<EglFSKmsGbmDevice *>(device())->handleDrmEvent();

}

//#define ENABLE_GBM_BO 
// ..> undefined symbol: gbm_bo_map

#ifdef ENABLE_GBM_BO
static uint32_t compute_color(int x, int y, int w, int h)
{
	uint32_t pixel = 0x00000000;
	float xratio = (x - w / 2) / ((float)(w / 2));
	float yratio = (y - h / 2) / ((float)(h / 2));
	// If a point is on or inside an ellipse, num <= 1.
	float num = xratio * xratio + yratio * yratio;
	uint32_t g = 255 * num;
	if (g < 256)
		pixel = 0x00FF0000 | (g << 8);
	return pixel;
}

static bool draw_ellipse(struct gbm_bo *bo)
{
	void *map_data;
	uint32_t stride;
	uint32_t w = gbm_bo_get_width(bo);
	uint32_t h = gbm_bo_get_height(bo);
	void *addr = gbm_bo_map(bo, 0, 0, w, h, 0, &stride, &map_data);
    
	if (addr == MAP_FAILED) {
		hERR("failed to mmap gbm bo");
		return false;
	}
	uint32_t *pixel = (uint32_t *)addr;
	uint32_t pixel_size = sizeof(*pixel);
	for (uint32_t y = 0; y < h; y++) {
		for (uint32_t x = 0; x < w; x++) {
			pixel[y * (stride / pixel_size) + x] = compute_color(x, y, w, h);
		}
	}
	gbm_bo_unmap(bo, map_data);
	return true;
}
#endif // ENABLE_GBM_BO

void EglFSKmsGbmScreen::flip()
{
    hDBG("");

    if (!m_gbm_surface) {
        hWARN("Cannot sync before platform init!");
        return;
    }
    #ifdef ENABLE_FB_ONCE
    if(m_gbm_bo_current == Q_NULLPTR)
        m_gbm_bo_next = gbm_surface_lock_front_buffer(m_gbm_surface);
    else 
        m_gbm_bo_next = m_gbm_bo_current;
    #else
    m_gbm_bo_next = gbm_surface_lock_front_buffer(m_gbm_surface);
    #endif
    
    if (!m_gbm_bo_next) {
        hWARN("Could not lock GBM surface front buffer!");
        return;
    }

    FrameBuffer *fb = framebufferForBufferObject(m_gbm_bo_next);
#ifdef DEMO_FLIP_1SEC
    OSA_waitMsecs(1000);
#endif

#ifdef ENABLE_GBM_BO // test of gbm buffer
    
	draw_ellipse(m_gbm_bo_next);


#endif

    
    KmsOutput &op(output());
    const int fd = device()->fd();
    const unsigned int w = op.modes[op.mode].hdisplay;
    const unsigned int h = op.modes[op.mode].vdisplay;

    if (!op.mode_set) {
        int ret = drmModeSetCrtc(fd,
                                 op.crtc_id,
                                 fb->fb,
                                 0, 0,
                                 &op.connector_id, 1,
                                 &op.modes[op.mode]);

        if (ret == -1) {
            hERR("Could not set DRM mode!");
        } else {
            op.mode_set = true;
            setPowerState(PowerStateOn);

            if (!op.plane_set) {
                op.plane_set = true;
                if (op.wants_plane) {
                    int ret = drmModeSetPlane(fd, op.plane_id, op.crtc_id,
                                              (unsigned int)-1, 0,
                                              0, 0, w, h,
                                              0 << 16, 0 << 16, w << 16, h << 16);
                    if (ret == -1)
                        hERR("drmModeSetPlane failed");
                }
            }
        }
    }

    int ret = drmModePageFlip(fd,
                              op.crtc_id,
                              fb->fb,
                              DRM_MODE_PAGE_FLIP_EVENT,
                              this);
    if (ret) {
        hERR("Could not queue DRM page flip! : ret(%d), errno(%d)", ret, errno);
        gbm_surface_release_buffer(m_gbm_surface, m_gbm_bo_next);
        m_gbm_bo_next = Q_NULLPTR;
    }

}

void EglFSKmsGbmScreen::flipFinished()
{
    #ifndef ENABLE_FB_ONCE
    if (m_gbm_bo_current)
        gbm_surface_release_buffer(m_gbm_surface,
                                   m_gbm_bo_current);
    #endif

    m_gbm_bo_current = m_gbm_bo_next;
    m_gbm_bo_next = Q_NULLPTR;
}


