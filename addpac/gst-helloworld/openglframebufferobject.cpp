// openglframebufferobject.cpp
#include "openglframebufferobject.h"
#include "openglcontext.h"
#include "openglcontext_p.h"


#define append push_back



#ifndef GL_MAX_SAMPLES
#define GL_MAX_SAMPLES 0x8D57
#endif

#ifndef GL_RENDERBUFFER_SAMPLES
#define GL_RENDERBUFFER_SAMPLES 0x8CAB
#endif

#ifndef GL_DEPTH24_STENCIL8
#define GL_DEPTH24_STENCIL8 0x88F0
#endif

#ifndef GL_DEPTH_COMPONENT24
#define GL_DEPTH_COMPONENT24 0x81A6
#endif

#ifndef GL_DEPTH_COMPONENT24_OES
#define GL_DEPTH_COMPONENT24_OES 0x81A6
#endif

#ifndef GL_READ_FRAMEBUFFER
#define GL_READ_FRAMEBUFFER 0x8CA8
#endif

#ifndef GL_DRAW_FRAMEBUFFER
#define GL_DRAW_FRAMEBUFFER 0x8CA9
#endif

#ifndef GL_RGB8
#define GL_RGB8                           0x8051
#endif

#ifndef GL_RGB10
#define GL_RGB10                          0x8052
#endif

#ifndef GL_RGBA8
#define GL_RGBA8                          0x8058
#endif

#ifndef GL_RGB10_A2
#define GL_RGB10_A2                       0x8059
#endif

#ifndef GL_BGRA
#define GL_BGRA                           0x80E1
#endif

#ifndef GL_UNSIGNED_INT_8_8_8_8_REV
#define GL_UNSIGNED_INT_8_8_8_8_REV       0x8367
#endif

#ifndef GL_UNSIGNED_INT_2_10_10_10_REV
#define GL_UNSIGNED_INT_2_10_10_10_REV    0x8368
#endif




OpenGLFramebufferObject::OpenGLFramebufferObject(const QSize &size, 
    const OpenGLFramebufferObjectFormat &format)
    : valid(false)
{
    m_format = new OpenGLFramebufferObjectFormat;
    
    *m_format = format;
    
    init(size);
}


/*!
    \fn QOpenGLFramebufferObject::~QOpenGLFramebufferObject()

    Destroys the framebuffer object and frees any allocated resources.
*/
OpenGLFramebufferObject::~OpenGLFramebufferObject()
{
    if(m_format) delete m_format;
    
}







bool OpenGLFramebufferObject::checkFramebufferStatus(OpenGLContext *ctx) const
{
    if (!ctx)
        return false;   // Context no longer exists.
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    switch(status) {
    case GL_NO_ERROR:
    case GL_FRAMEBUFFER_COMPLETE:
        return true;
    case GL_FRAMEBUFFER_UNSUPPORTED:
        hDBG("QOpenGLFramebufferObject: Unsupported framebuffer format.");
        break;
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        hDBG("QOpenGLFramebufferObject: Framebuffer incomplete attachment.");
        break;
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        hDBG("QOpenGLFramebufferObject: Framebuffer incomplete, missing attachment.");
        break;
#ifdef GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT
    case GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT:
        hDBG("QOpenGLFramebufferObject: Framebuffer incomplete, duplicate attachment.");
        break;
#endif
#ifdef GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS
    case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
        hDBG("QOpenGLFramebufferObject: Framebuffer incomplete, attached images must have same dimensions.");
        break;
#endif
#ifdef GL_FRAMEBUFFER_INCOMPLETE_FORMATS
    case GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
        hDBG("QOpenGLFramebufferObject: Framebuffer incomplete, attached images must have same format.");
        break;
#endif
#ifdef GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER
    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
        qDebug("QOpenGLFramebufferObject: Framebuffer incomplete, missing draw buffer.");
        break;
#endif
#ifdef GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER
    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
        hDBG("QOpenGLFramebufferObject: Framebuffer incomplete, missing read buffer.");
        break;
#endif
#ifdef GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE
    case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
        hDBG("QOpenGLFramebufferObject: Framebuffer incomplete, attachments must have same number of samples per pixel.");
        break;
#endif
    default:
        hDBG("QOpenGLFramebufferObject: An undefined error has occurred: %p", status);
        break;
    }
    return false;
}

namespace {
    void freeFramebufferFunc(GLuint id)
    {
        glDeleteFramebuffers(1, &id);
    }

    void freeRenderbufferFunc(GLuint id)
    {
        glDeleteRenderbuffers(1, &id);
    }

    void freeTextureFunc(GLuint id)
    {
        glDeleteTextures(1, &id);
    }
}



void OpenGLFramebufferObject::initTexture(int idx)
{
    OpenGLContext *ctx = OpenGLContext::currentContext();
    GLuint texture = 0;

    glGenTextures(1, &texture);
    glBindTexture(target, texture);

    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    ColorAttachment &color(colorAttachments[idx]);

    GLuint pixelType = GL_UNSIGNED_BYTE;
    if (color.internalFormat == GL_RGB10_A2 || color.internalFormat == GL_RGB10)
        pixelType = GL_UNSIGNED_INT_2_10_10_10_REV;

    glTexImage2D(target, 0, color.internalFormat, color.size.width(), color.size.height(), 0,
                       GL_RGBA, pixelType, NULL);
    if (m_format->mipmap) {
        int width = color.size.width();
        int height = color.size.height();
        int level = 0;
        while (width > 1 || height > 1) {
            width = qMax(1, width >> 1);
            height = qMax(1, height >> 1);
            ++level;
            glTexImage2D(target, level, color.internalFormat, width, height, 0,
                               GL_RGBA, pixelType, NULL);
        }
    }
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + idx,
                                 target, texture, 0);

    GL_CHECK_ERROR();
    glBindTexture(target, 0);
    valid = checkFramebufferStatus(ctx);
    if (valid) {
        hDBG("texture:%d", texture);
        color.guard = new OpenGLSharedResourceGuard(ctx, texture, freeTextureFunc);
    } else {
        glDeleteTextures(1, &texture);
    }
}

#if 0
void OpenGLFramebufferObject::initColorBuffer(int idx, GLint *samples)
{
    OpenGLContext *ctx = OpenGLContext::currentContext();
    GLuint color_buffer = 0;

    ColorAttachment &color(colorAttachments[idx]);

    GLenum storageFormat = color.internalFormat;
    // ES requires a sized format. The older desktop extension does not. Correct the format on ES.
    if (ctx->isOpenGLES() && color.internalFormat == GL_RGBA) {
        if (funcs.hasOpenGLExtension(QOpenGLExtensions::Sized8Formats))
            storageFormat = GL_RGBA8;
        else
            storageFormat = GL_RGBA4;
    }

    funcs.glGenRenderbuffers(1, &color_buffer);
    funcs.glBindRenderbuffer(GL_RENDERBUFFER, color_buffer);
    funcs.glRenderbufferStorageMultisample(GL_RENDERBUFFER, *samples, storageFormat, color.size.width(), color.size.height());
    funcs.glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + idx,
                                    GL_RENDERBUFFER, color_buffer);

    QT_CHECK_GLERROR();
    valid = checkFramebufferStatus(ctx);
    if (valid) {
        // Query the actual number of samples. This can be greater than the requested
        // value since the typically supported values are 0, 4, 8, ..., and the
        // requests are mapped to the next supported value.
        funcs.glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_SAMPLES, samples);
        color.guard = new QOpenGLSharedResourceGuard(ctx, color_buffer, freeRenderbufferFunc);
    } else {
        funcs.glDeleteRenderbuffers(1, &color_buffer);
    }
}

#endif


void OpenGLFramebufferObject::init(const QSize &size)
{
    OpenGLContext *ctx = OpenGLContext::currentContext();
    #if 0
    funcs.initializeOpenGLFunctions();

    if (!funcs.hasOpenGLFeature(QOpenGLFunctions::Framebuffers))
        return;

    // Fall back to using a normal non-msaa FBO if we don't have support for MSAA
    if (!funcs.hasOpenGLExtension(QOpenGLExtensions::FramebufferMultisample)
            || !funcs.hasOpenGLExtension(QOpenGLExtensions::FramebufferBlit)) {
        samples = 0;
    } else if (!ctx->isOpenGLES() || ctx->format().majorVersion() >= 3) {
        GLint maxSamples;
        funcs.glGetIntegerv(GL_MAX_SAMPLES, &maxSamples);
        samples = qBound(0, int(samples), int(maxSamples));
    }
    #endif

    #ifdef ENABLE_COLORATTACH
    colorAttachments.append(ColorAttachment(size, m_format->internal_format));
    #endif

    dsSize = size;
    target = m_format->target;
    GLuint fbo = 0;

    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    //QOpenGLContextPrivate::get(ctx)->qgl_current_fbo_invalid = true;

    GL_CHECK_ERROR();

    if (m_format->samples == 0)
        initTexture(0);
    else
    {
        assert(0);
        //initColorBuffer(0, &samples);
        //format.setSamples(int(samples));
    }


    

    //initDepthStencilAttachments(ctx, attachment);
    

    if (valid)
        fbo_guard = new OpenGLSharedResourceGuard(ctx, fbo, freeFramebufferFunc);
    else
        glDeleteFramebuffers(1, &fbo);

    GL_CHECK_ERROR();
}



bool OpenGLFramebufferObject::bindDefault()
{
    OpenGLContext *ctx = const_cast<OpenGLContext *>(OpenGLContext::currentContext());

    if (ctx) {
        glBindFramebuffer(GL_FRAMEBUFFER, ctx->defaultFramebufferObject());
        //QOpenGLContextPrivate::get(ctx)->qgl_current_fbo_invalid = true;
        //QOpenGLContextPrivate::get(ctx)->qgl_current_fbo = Q_NULLPTR;
    }
    else
        hWARN("QOpenGLFramebufferObject::bindDefault() called without current context.");

    return ctx != 0;
}






























OpenGLFramebufferObjectFormat::OpenGLFramebufferObjectFormat()
    : samples(0)
    , attachment(OpenGLFramebufferObject::NoAttachment)
    , target(GL_TEXTURE_2D)
    , mipmap(false)
{
#if 0//ndef QT_OPENGL_ES_2
        // There is nothing that says QOpenGLFramebufferObjectFormat needs a current
        // context, so we need a fallback just to be safe, even though in pratice there
        // will usually be a context current.
        QOpenGLContext *ctx = QOpenGLContext::currentContext();
        const bool isES = ctx ? ctx->isOpenGLES() : QOpenGLContext::openGLModuleType() != QOpenGLContext::LibGL;
        internal_format = isES ? GL_RGBA : GL_RGBA8;
#else
        internal_format = GL_RGBA;
#endif
    
}


OpenGLFramebufferObjectFormat::~OpenGLFramebufferObjectFormat()
{
    
}






