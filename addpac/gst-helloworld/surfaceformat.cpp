
#include "surfaceformat.h"

SurfaceFormat::SurfaceFormat()
    : redBufferSize(-1)
    , greenBufferSize(-1)
    , blueBufferSize(-1)
    , alphaBufferSize(-1)
    , depthBufferSize(-1)
    , stencilBufferSize(-1)
    , swapBehavior(SurfaceFormat::DefaultSwapBehavior)
    , numSamples(-1)
    , renderableType(SurfaceFormat::DefaultRenderableType)
    , profile(SurfaceFormat::NoProfile)
    , swapInterval(1) // default to vsync
    , majorVersion(EGL_MAJOR_VER)
    , minorVersion(0)
{
}


SurfaceFormat::~SurfaceFormat()
{
    
}

SurfaceFormat::SurfaceFormat(const SurfaceFormat &other)
{
    redBufferSize = other.redBufferSize;
    greenBufferSize = other.greenBufferSize;
    blueBufferSize = other.blueBufferSize;

    alphaBufferSize = other.alphaBufferSize;
    depthBufferSize = other.depthBufferSize;
    stencilBufferSize = other.stencilBufferSize;
    swapBehavior = other.swapBehavior;
    numSamples = other.numSamples;
    renderableType = other.renderableType;
    
    profile = other.profile;
    swapInterval = other.swapInterval;
    majorVersion = other.majorVersion;
    minorVersion = other.minorVersion;

    
}


#if 0

/*!
    Get the size in bits of the red channel of the color buffer.
*/
int SurfaceFormat::redBufferSize() const
{
    return redBufferSize;
}

/*!
    Get the size in bits of the green channel of the color buffer.
*/
int SurfaceFormat::greenBufferSize() const
{
    return greenBufferSize;
}

/*!
    Get the size in bits of the blue channel of the color buffer.
*/
int SurfaceFormat::blueBufferSize() const
{
    return blueBufferSize;
}

/*!
    Get the size in bits of the alpha channel of the color buffer.
*/
int SurfaceFormat::alphaBufferSize() const
{
    return alphaBufferSize;
}


#endif 


int SurfaceFormat::samples() const
{
   return numSamples;
}



DBGOUT& operator<<(DBGOUT &out, const SurfaceFormat &f)
{
    const SurfaceFormat* const d = &f;
    //QDebugStateSaver saver(dbg);

    out << "SurfaceFormat("
                  << "version " << d->majorVersion << '.' << d->minorVersion
                  << ", depthBufferSize " << d->depthBufferSize
                  << ", redBufferSize " << d->redBufferSize
                  << ", greenBufferSize " << d->greenBufferSize
                  << ", blueBufferSize " << d->blueBufferSize
                  << ", alphaBufferSize " << d->alphaBufferSize
                  << ", stencilBufferSize " << d->stencilBufferSize
                  << ", samples " << d->numSamples
                  << ", swapBehavior " << d->swapBehavior
                  << ", swapInterval " << d->swapInterval
                  << ", profile  " << d->profile
                  << ')';

    return out;
}

