// gcc -o drmgl Linux_DRM_OpenGLES.c `pkg-config --cflags --libs libdrm` -lgbm -lEGL -lGLESv2

/*
 * Copyright (c) 2012 Arvin Schnell <arvin.schnell@gmail.com>
 * Copyright (c) 2012 Rob Clark <rob@ti.com>
 * Copyright (c) 2017 Miouyouyou <Myy> <myy@miouyouyou.fr>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* Based on a egl cube test app originally written by Arvin Schnell */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <xf86drm.h>
#include <xf86drmMode.h>
#include <gbm.h>

#define GL_GLEXT_PROTOTYPES 1
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <assert.h>

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

//#define TEST_GST
#ifdef TEST_GST
#include "gstutils.h"
#include "gstplayersession.h"
#include "gstvideowindow.h"

#endif

static struct {
	EGLDisplay display;
	EGLConfig config;
	EGLContext context;
	EGLSurface surface;
	GLuint program;
	GLint modelviewmatrix, modelviewprojectionmatrix, normalmatrix;
	GLuint vbo;
	GLuint positionsoffset, colorsoffset, normalsoffset;
} gl;

static struct {
	struct gbm_device *dev;
	struct gbm_surface *surface;
} gbm;

static struct {
	int fd;
	drmModeModeInfo *mode;
	uint32_t crtc_id;
	uint32_t connector_id;
} drm;

struct drm_fb {
	struct gbm_bo *bo;
	uint32_t fb_id;
};

static uint32_t find_crtc_for_encoder(const drmModeRes *resources,
				      const drmModeEncoder *encoder) {
	int i;

	for (i = 0; i < resources->count_crtcs; i++) {
		/* possible_crtcs is a bitmask as described here:
		 * https://dvdhrm.wordpress.com/2012/09/13/linux-drm-mode-setting-api
		 */
		const uint32_t crtc_mask = 1 << i;
		const uint32_t crtc_id = resources->crtcs[i];
		if (encoder->possible_crtcs & crtc_mask) {
			return crtc_id;
		}
	}

	/* no match found */
	return -1;
}

static uint32_t find_crtc_for_connector(const drmModeRes *resources,
					const drmModeConnector *connector) {
	int i;

	for (i = 0; i < connector->count_encoders; i++) {
		const uint32_t encoder_id = connector->encoders[i];
		drmModeEncoder *encoder = drmModeGetEncoder(drm.fd, encoder_id);

		if (encoder) {
			const uint32_t crtc_id = find_crtc_for_encoder(resources, encoder);

			drmModeFreeEncoder(encoder);
			if (crtc_id != 0) {
				return crtc_id;
			}
		}
	}

	/* no match found */
	return -1;
}

static int init_drm(void)
{
	drmModeRes *resources;
	drmModeConnector *connector = NULL;
	drmModeEncoder *encoder = NULL;
	int i, area;

	drm.fd = open("/dev/dri/card0", O_RDWR);
	
	if (drm.fd < 0) {
		printf("could not open drm device\n");
		return -1;
	}

	resources = drmModeGetResources(drm.fd);
	if (!resources) {
		printf("drmModeGetResources failed: %s\n", strerror(errno));
		return -1;
	}

	/* find a connected connector: */
	for (i = 0; i < resources->count_connectors; i++) {
		connector = drmModeGetConnector(drm.fd, resources->connectors[i]);
		if (connector->connection == DRM_MODE_CONNECTED) {
			/* it's connected, let's use this! */
			break;
		}
		drmModeFreeConnector(connector);
		connector = NULL;
	}

	if (!connector) {
		/* we could be fancy and listen for hotplug events and wait for
		 * a connector..
		 */
		printf("no connected connector!\n");
		return -1;
	}

	/* find prefered mode or the highest resolution mode: */
	for (i = 0, area = 0; i < connector->count_modes; i++) {
		drmModeModeInfo *current_mode = &connector->modes[i];

		if (current_mode->type & DRM_MODE_TYPE_PREFERRED) {
			drm.mode = current_mode;
		}

		int current_area = current_mode->hdisplay * current_mode->vdisplay;
		if (current_area > area) {
			drm.mode = current_mode;
			area = current_area;
		}
	}

	if (!drm.mode) {
		printf("could not find mode!\n");
		return -1;
	}

    printf("%dx%d\n", drm.mode->hdisplay, drm.mode->vdisplay);

	/* find encoder: */
	for (i = 0; i < resources->count_encoders; i++) {
		encoder = drmModeGetEncoder(drm.fd, resources->encoders[i]);
		if (encoder->encoder_id == connector->encoder_id)
			break;
		drmModeFreeEncoder(encoder);
		encoder = NULL;
	}

	if (encoder) {
		drm.crtc_id = encoder->crtc_id;
	} else {
		uint32_t crtc_id = find_crtc_for_connector(resources, connector);
		if (crtc_id == 0) {
			printf("no crtc found!\n");
			return -1;
		}

		drm.crtc_id = crtc_id;
	}

	drm.connector_id = connector->connector_id;

	return 0;
}

static int init_gbm(void)
{
	gbm.dev = gbm_create_device(drm.fd);

	gbm.surface = gbm_surface_create(gbm.dev,
			drm.mode->hdisplay, drm.mode->vdisplay,
			GBM_FORMAT_XRGB8888,
			GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
	if (!gbm.surface) {
		printf("failed to create gbm surface\n");
		return -1;
	}

	return 0;
}

static int init_gl(void)
{
	EGLint major, minor, n;
	GLuint vertex_shader, fragment_shader;
	GLint ret;

	static const EGLint context_attribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};

	static const EGLint config_attribs[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RED_SIZE, 1,
		EGL_GREEN_SIZE, 1,
		EGL_BLUE_SIZE, 1,
		EGL_ALPHA_SIZE, 0,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};

	PFNEGLGETPLATFORMDISPLAYEXTPROC get_platform_display = NULL;
	get_platform_display =
		(PFNEGLGETPLATFORMDISPLAYEXTPROC) eglGetProcAddress("eglGetPlatformDisplayEXT");
	assert(get_platform_display != NULL);

	gl.display = get_platform_display(EGL_PLATFORM_GBM_KHR, gbm.dev, NULL);

	if (!eglInitialize(gl.display, &major, &minor)) {
		printf("failed to initialize\n");
		return -1;
	}

	printf("Using display %p with EGL version %d.%d\n",
			gl.display, major, minor);

	printf("EGL Version \"%s\"\n", eglQueryString(gl.display, EGL_VERSION));
	printf("EGL Vendor \"%s\"\n", eglQueryString(gl.display, EGL_VENDOR));
	printf("EGL Extensions \"%s\"\n", eglQueryString(gl.display, EGL_EXTENSIONS));

	if (!eglBindAPI(EGL_OPENGL_ES_API)) {
		printf("failed to bind api EGL_OPENGL_ES_API\n");
		return -1;
	}

	if (!eglChooseConfig(gl.display, config_attribs, &gl.config, 1, &n) || n != 1) {
		printf("failed to choose config: %d\n", n);
		return -1;
	}

	gl.context = eglCreateContext(gl.display, gl.config,
			EGL_NO_CONTEXT, context_attribs);
	if (gl.context == NULL) {
		printf("failed to create context\n");
		return -1;
	}

	gl.surface = eglCreateWindowSurface(gl.display, gl.config, gbm.surface, NULL);
	if (gl.surface == EGL_NO_SURFACE) {
		printf("failed to create egl surface\n");
		return -1;
	}

	/* connect the context to the surface */
	eglMakeCurrent(gl.display, gl.surface, gl.surface, gl.context);

	printf("GL Extensions: \"%s\"\n", glGetString(GL_EXTENSIONS));

	return 0;
}

/* Draw code here */
void draw(uint32_t i)
{
	glClear(GL_COLOR_BUFFER_BIT);
    float v= 0.01 *(i& 0xFF);
    
    
    glClearColor(0.5, v, 0.5, 0.5);
    
}

static void
drm_fb_destroy_callback(struct gbm_bo *bo, void *data)
{
	struct drm_fb *fb = (struct drm_fb *)data;
	struct gbm_device *gbm = (struct gbm_device *)gbm_bo_get_device(bo);

	if (fb->fb_id)
		drmModeRmFB(drm.fd, fb->fb_id);

	free(fb);
}

static struct drm_fb * drm_fb_get_from_bo(struct gbm_bo *bo)
{
	struct drm_fb *fb = (struct drm_fb *)gbm_bo_get_user_data(bo);
	uint32_t width, height, stride, handle;
	int ret;

	if (fb)
		return fb;

	fb = (drm_fb*)calloc(1, sizeof *fb);
	fb->bo = bo;

	width = gbm_bo_get_width(bo);
	height = gbm_bo_get_height(bo);
	stride = gbm_bo_get_stride(bo);
	handle = gbm_bo_get_handle(bo).u32;

	ret = drmModeAddFB(drm.fd, width, height, 24, 32, stride, handle, &fb->fb_id);
	if (ret) {
		printf("failed to create fb: %s\n", strerror(errno));
		free(fb);
		return NULL;
	}

	gbm_bo_set_user_data(bo, fb, drm_fb_destroy_callback);

	return fb;
}

static void page_flip_handler(int fd, unsigned int frame,
		  unsigned int sec, unsigned int usec, void *data)
{
	int *waiting_for_flip = (int*)data;
	*waiting_for_flip = 0;
}

int test_drm_opengles() //int argc, char *argv[])
{
	fd_set fds;
    #if 0
	drmEventContext evctx = {
			.version = DRM_EVENT_CONTEXT_VERSION,
			.page_flip_handler = page_flip_handler
	};
    #else
    drmEventContext evctx;
    memset(&evctx, 0, sizeof(evctx));
    #endif
	struct gbm_bo *bo;
	struct drm_fb *fb;
	uint32_t i = 0;
	int ret;

    evctx.version = DRM_EVENT_CONTEXT_VERSION;
    evctx.page_flip_handler = page_flip_handler;

	ret = init_drm();
	if (ret) {
		printf("failed to initialize DRM\n");
		return ret;
	}

	FD_ZERO(&fds);
	FD_SET(0, &fds);
	FD_SET(drm.fd, &fds);

	ret = init_gbm();
	if (ret) {
		printf("failed to initialize GBM\n");
		return ret;
	}

	ret = init_gl();
	if (ret) {
		printf("failed to initialize EGL\n");
		return ret;
	}

	/* clear the color buffer */
	glClearColor(0.5, 0.5, 0.5, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	eglSwapBuffers(gl.display, gl.surface);
	bo = gbm_surface_lock_front_buffer(gbm.surface);
	fb = drm_fb_get_from_bo(bo);

	/* set mode: */
	ret = drmModeSetCrtc(drm.fd, drm.crtc_id, fb->fb_id, 0, 0,
			&drm.connector_id, 1, drm.mode);
	if (ret) {
		printf("failed to set mode: %s\n", strerror(errno));
		return ret;
	}

#ifdef TEST_GST
    gst_init (0, 0);

    GstPlayerSession player_session;

    GstVideoWindow videoWindow;

   // If the GStreamer video sink is not available, don't provide the video window control since
    // it won't work anyway.
    #if 1
    if (!videoWindow.videoSink()) {
        printf("================ error .\n");    
        return ;
    }    
    #endif


    player_session.setVideoRenderer(&videoWindow);

    //
    videoWindow.setWinId(1);
    
    player_session.updateVideoRenderer();

    
    videoWindow.setAspectRatioMode( KeepAspectRatio );
    QRect rect(900, 500, 1920/2,1080/2);
    videoWindow.setDisplayRect(rect);

    // ********************************
    // set media : QGstreamerPlayerControl::setMedia
    
    player_session.showPrerollFrames(false); // do not show prerolled frames until pause() or play() explicitly called


    player_session.stop();



    player_session.loadFromUri("file:///opt/dragon.mp4");


    player_session.pause();
    
#define NO_WAIT


#ifndef NO_WAIT
    OSA_waitMsecs(10000);
#endif

    printf(" ======== puase  ... after 5sec\n ");

#ifndef NO_WAIT
    OSA_waitMsecs(3000);
#endif
    player_session.pause();
    player_session.showPrerollFrames(true);
    player_session.seek(0);
#ifndef NO_WAIT    
    OSA_waitMsecs(3000);
#endif

    player_session.play();

#endif // TEST_GST


    int cnt = 0;
#ifndef TEST_GST
	while (cnt++ < 100) {
#else
	while (cnt++ < 1000) {
#endif
		struct gbm_bo *next_bo;
		int waiting_for_flip = 1;

		draw(i++);

		eglSwapBuffers(gl.display, gl.surface);
		next_bo = gbm_surface_lock_front_buffer(gbm.surface);
		fb = drm_fb_get_from_bo(next_bo);

		/*
		 * Here you could also update drm plane layers if you want
		 * hw composition
		 */

		ret = drmModePageFlip(drm.fd, drm.crtc_id, fb->fb_id,
				DRM_MODE_PAGE_FLIP_EVENT, &waiting_for_flip);
		if (ret) {
			printf("failed to queue page flip: %s\n", strerror(errno));
			return -1;
		}

		while (waiting_for_flip) {
			ret = select(drm.fd + 1, &fds, NULL, NULL, NULL);
			if (ret < 0) {
				printf("select err: %s\n", strerror(errno));
				return ret;
			} else if (ret == 0) {
				printf("select timeout!\n");
				return -1;
			} else if (FD_ISSET(0, &fds)) {
				printf("user interrupted!\n");
				break;
			}
			drmHandleEvent(drm.fd, &evctx);
		}

		/* release last buffer to render on again: */
		gbm_surface_release_buffer(gbm.surface, bo);
		bo = next_bo;
	}

	return ret;
}





#include <stdio.h>
#include <libdrm/drm.h>

#include <stdint.h>

#include <sys/mman.h>

#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>

#include <sys/ioctl.h>
#include <errno.h>

#include <xf86drm.h>
#include <xf86drmMode.h>

// rand
#include <stdlib.h>

#define LOG(msg, ...) \
	fprintf(\
		stderr, "\n[%s (%s:%d)]\n" msg,\
		__func__, __FILE__, __LINE__, ##__VA_ARGS__ \
	)

#define ALIGN_ON_POW2(n, align) ((n + align - 1) & ~(align - 1))

// Works on Rockchip systems but fail with ENOSYS on AMDGPU
int test_drm_dumb_kms()
{
    int dma_buf_fd;
    uint8_t * primed_framebuffer;
	/* DRM is based on the fact that you can connect multiple screens,
	 * on multiple different connectors which have, of course, multiple
	 * encoders that transform CRTC (The screen final buffer where all
	 * the framebuffers are blended together) represented in XRGB8888 (or
	 * similar) into something the selected screen comprehend.
	 * (Think XRGB8888 to DVI-D format for example)
	 * 
	 * The default selection system is simple :
	 * - We try to find the first connected screen and choose its
	 *   preferred resolution.
	 */
	 #if 1
	drmModeRes         * __restrict drm_resources;
	drmModeConnector   * __restrict valid_connector   = NULL;
	drmModeModeInfo    * __restrict chosen_resolution = NULL;
	drmModeEncoder     * __restrict screen_encoder    = NULL;
    #else
    drmModeRes         * drm_resources;
	drmModeConnector   * valid_connector   = NULL;
	drmModeModeInfo    * chosen_resolution = NULL;
	drmModeEncoder     * screen_encoder    = NULL;
    #endif
    uint32_t current_crtc_id;
    drmModeCrtc * __restrict crtc_to_restore = NULL;
    struct drm_prime_handle prime_request;
	int ret = 0;

	/* Open the DRM device node and get a File Descriptor */
	int const drm_fd = open("/dev/dri/card0", O_RDWR | O_CLOEXEC);

	if (drm_fd < 0) {
		LOG("Could not open /dev/dri/card0 : %m\n");
		goto could_not_open_drm_fd;
	}

	/* Let's see what we can use through this drm node */
	drm_resources = drmModeGetResources(drm_fd);
	/* Get a valid connector. A valid connector is one that's connected */
	for (int_fast32_t c = 0; c < drm_resources->count_connectors; c++) {
		valid_connector =
			drmModeGetConnector(drm_fd, drm_resources->connectors[c]);

		if (valid_connector->connection == DRM_MODE_CONNECTED)
			break;

		drmModeFreeConnector(valid_connector);
		valid_connector = NULL;
	}

	/* Bail out if nothing was connected */
	if (!valid_connector) {
		/* Then there was no connectors,
		 * or no connector were connected */
		LOG("No connectors or no connected connectors found...\n");
		ret = -ENOLINK;
		goto no_valid_connector;
	}

	/* Get the preferred resolution */
	for (int_fast32_t m = 0; m < valid_connector->count_modes; m++) {
		drmModeModeInfo * __restrict tested_resolution =
			&valid_connector->modes[m];
		if (tested_resolution->type & DRM_MODE_TYPE_PREFERRED) {
			chosen_resolution = tested_resolution;
			break;
		}
	}

	/* Bail out if there's no such thing as a "preferred resolution" */
	if (!chosen_resolution) {
		LOG(
			"No preferred resolution on the selected connector %u ?\n",
			valid_connector->connector_id
		);
		ret = -ENAVAIL;
		goto no_valid_resolution;
	}

	/* Get an encoder that will transform our CRTC data into something
	 * the screen comprehend natively, through the chosen connector */
	screen_encoder =
		drmModeGetEncoder(drm_fd, valid_connector->encoder_id);

	/* If there's no screen encoder through the chosen connector, bail
	 * out quickly. */
	if (!screen_encoder) {
		LOG(
			"Could not retrieve the encoder for mode %s on connector %u.\n",
			chosen_resolution->name,
			valid_connector->connector_id
		);
		ret = -ENOLINK;
		goto could_not_retrieve_encoder;
	}

	/* We're almost done with KMS. We'll now allocate a "dumb" buffer on
	 * the GPU, and use it as a "frame buffer", that is something that
	 * will be read and displayed on screen (the CRTC to be exact) */

	/* Request a dumb buffer */
    #if 0
	struct drm_mode_create_dumb create_request = {
		.width  = chosen_resolution->hdisplay,
		.height = chosen_resolution->vdisplay,
		.bpp    = 32
	};
    #else
    struct drm_mode_create_dumb create_request ;
    memset(&create_request, 0, sizeof(create_request));
    create_request.width  = chosen_resolution->hdisplay;
    create_request.height = chosen_resolution->vdisplay;
    create_request.bpp    = 32;
    #endif
    
	ret = ioctl(drm_fd, DRM_IOCTL_MODE_CREATE_DUMB, &create_request);


	/* Bail out if we could not allocate a dumb buffer */
	if (ret) {
		LOG(
			"Dumb Buffer Object Allocation request of %ux%u@%u failed : %s\n",
			create_request.width, create_request.height,
			create_request.bpp,
			strerror(ret)
		);
		goto could_not_allocate_buffer;
	}

	/* Create a framebuffer, using the old method.
	 * 
	 * A new method exist, drmModeAddFB2 : Return of the Revengeance !
	 * 
	 * Jokes aside, this other method takes well defined color formats
	 * as arguments instead of specifying the depth and BPP manually.
	 */
	uint32_t frame_buffer_id;
	ret = drmModeAddFB(
		drm_fd,
		chosen_resolution->hdisplay, chosen_resolution->vdisplay,
		24, 32, create_request.pitch,
		create_request.handle, &frame_buffer_id
	);

    
	/* Without framebuffer, we won't do anything so bail out ! */
	if (ret) {
		LOG(
			"Could not add a framebuffer using drmModeAddFB : %s\n",
			strerror(ret)
		);
        
		goto could_not_add_frame_buffer;
	}


	/* We assume that the currently chosen encoder CRTC ID is the current
	 * one.
	 */
	 current_crtc_id = screen_encoder->crtc_id;


	if (!current_crtc_id) {
		LOG("The retrieved encoder has no CRTC attached... ?\n");
		goto could_not_retrieve_current_crtc;
	}

	/* Backup the informations of the CRTC to restore when we're done.
	 * The most important piece seems to currently be the buffer ID.
	 */
	crtc_to_restore =
		drmModeGetCrtc(drm_fd, current_crtc_id);
	if (!crtc_to_restore) {
		LOG("Could not retrieve the current CRTC with a valid ID !\n");
		goto could_not_retrieve_current_crtc;
	}

	/* Set the CRTC so that it uses our new framebuffer */
	ret = drmModeSetCrtc(
		drm_fd, current_crtc_id, frame_buffer_id,
		0, 0,
		&valid_connector->connector_id,
		1,
		chosen_resolution);
	/* For this test only : Export our dumb buffer using PRIME */
	/* This will provide us a PRIME File Descriptor that we'll use to
	 * map the represented buffer. This could be also be used to reimport
	 * the GEM buffer into another GPU */
	memset(&prime_request, 0, sizeof(prime_request ));
    prime_request.handle = create_request.handle;
    prime_request.flags  = DRM_CLOEXEC | DRM_RDWR;
    prime_request.fd     = -1;


	ret = ioctl(drm_fd, DRM_IOCTL_PRIME_HANDLE_TO_FD, &prime_request);
	dma_buf_fd = prime_request.fd;

	/* If we could not export the buffer, bail out since that's the
	 * purpose of our test */
	if (ret || dma_buf_fd < 0) {
		LOG(
			"Could not export buffer : %s (%d) - FD : %d\n",
			strerror(ret), ret,
			dma_buf_fd
 		);
		goto could_not_export_buffer;
	}

	/* Map the exported buffer, using the PRIME File descriptor */
	/* That ONLY works if the DRM driver implements gem_prime_mmap.
	 * This function is not implemented in most of the DRM drivers for 
	 * GPU with discrete memory. Meaning that it will surely fail with
	 * Radeon, AMDGPU and Nouveau drivers for desktop cards ! */
	primed_framebuffer = mmap(
		0, create_request.size,	PROT_READ | PROT_WRITE, MAP_SHARED,
		dma_buf_fd, 0);
	ret = errno;

	/* Bail out if we could not map the framebuffer using this method */
	if (primed_framebuffer == NULL || primed_framebuffer == MAP_FAILED) {
		LOG(
			"Could not map buffer exported through PRIME : %s (%d)\n"
			"Buffer : %p\n",
			strerror(ret), ret,
			primed_framebuffer
		);
		goto could_not_map_buffer;
	}

	LOG("Buffer mapped !\n");
{
	/* The fun begins ! At last !
	 * We'll do something simple :
	 * We'll lit a row of pixel, on the screen, starting from the top,
	 * down to the bottom of screen, using either Red, Blue or Green
	 * randomly, each time we press Enter.
	 * If we press 'q' and then Enter, the process will stop.
	 * The process will also stop once we've reached the bottom of the
	 * screen.
	 */
	uint32_t const bytes_per_pixel = 4;
	uint_fast64_t
	  pixel = 0,
	  size = create_request.size;

	/* Cleanup the framebuffer */
	memset(primed_framebuffer, 0, size);

	/* The colors table */
	uint32_t const red   = (0xff<<16);
	uint32_t const green = (0xff<<8);
	uint32_t const blue  = (0xff);
	uint32_t const colors[] = {red, green, blue};

	/* Pitch is the stride in bytes.
	 * However, for our purpose we'd like to know the stride in pixels.
	 * So we'll divide the pitch (in bytes) by the number of bytes
	 * composing a pixel to get that information.
	 */
	uint32_t const stride_pixel = create_request.pitch / bytes_per_pixel;
	uint32_t const width_pixel  = create_request.width;

	/* The width is padded so that each row starts with a specific
	 * alignment. That means that we have useless pixels that we could
	 * avoid dealing with in the first place.
	 * Now, it might be faster to just lit these useless pixels and get
	 * done with it. */
	uint32_t const diff_between_width_and_stride =
		stride_pixel - width_pixel;
	uint_fast64_t const size_in_pixels =
		create_request.height * stride_pixel;

	/* While we didn't get a 'q' + Enter or reached the bottom of the
	 * screen... */
	while (getc(stdin) != 'q' && pixel < size_in_pixels) {
		/* Choose a random color. 3 being the size of the colors table. */
		uint32_t current_color = colors[rand()%3];

		/* Color every pixel of the row.
		 * Now, the framebuffer is linear. Meaning that the first pixel of
		 * the first row should be at index 0, but the first pixel of the
		 * second row should be at index (stride+0) and the first pixel of
		 * the n-th row should be at (n*stride+0).
		 * 
		 * Instead of computing the value, we'll just increment the "pixel"
		 * index and accumulate the padding once done with the current row,
		 * in order to be ready to start for the next row.
		 */
		for (uint_fast32_t p = 0; p < width_pixel; p++)
			((uint32_t *) primed_framebuffer)[pixel++] = current_color;
		pixel += diff_between_width_and_stride;
		//LOG("pixel : %lu, size : %lu\n", pixel, size_in_pixels);
	}

	munmap(primed_framebuffer, create_request.size);
}

could_not_map_buffer:
could_not_export_buffer:
	// very ugly but will do for an example...
	{
		struct drm_mode_destroy_dumb destroy_request = {
			.handle = create_request.handle
		};
		
		ioctl(drm_fd, DRM_IOCTL_MODE_DESTROY_DUMB, &destroy_request);
	}
	drmModeSetCrtc(
		drm_fd,
		crtc_to_restore->crtc_id, crtc_to_restore->buffer_id,
		0, 0, &valid_connector->connector_id, 1, &crtc_to_restore->mode
	);

    
could_not_retrieve_current_crtc:
	drmModeRmFB(drm_fd, frame_buffer_id);
could_not_add_frame_buffer:
could_not_allocate_buffer:
	drmModeFreeEncoder(screen_encoder);
could_not_retrieve_encoder:
	drmModeFreeModeInfo(chosen_resolution);
no_valid_resolution:
	drmModeFreeConnector(valid_connector);
no_valid_connector:
	close(drm_fd);
could_not_open_drm_fd:
	return 0;
#if 0
    

could_not_map_buffer:
could_not_export_buffer:
could_not_retrieve_current_crtc:
could_not_add_frame_buffer:
could_not_allocate_buffer:
could_not_retrieve_encoder:
no_valid_resolution:
no_valid_connector:
could_not_open_drm_fd:
	return 0;
#endif
}

