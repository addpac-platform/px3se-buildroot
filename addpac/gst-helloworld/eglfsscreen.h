#ifndef __EGLFS_SCREEN_H__
#define __EGLFS_SCREEN_H__

#include "platformscreen.h"
#include <EGL/egl.h>
#include <EGL/eglext.h>


class EglFSScreen : public PlatformScreen
{
public:
    EglFSScreen(EGLDisplay display);
    ~EglFSScreen();

    QRect geometry() const override;
    virtual QRect rawGeometry() const;

    EGLSurface primarySurface() const { return m_surface; }
    EGLDisplay display() const { return m_dpy; }
    void setPrimarySurface(EGLSurface surface)
    {
        m_surface = surface;
    }


private:
    EGLDisplay m_dpy;
    EGLSurface m_surface;
    


};






#endif // __EGLFS_SCREEN_H__

