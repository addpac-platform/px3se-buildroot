#ifndef __EGLFS_KMS_SCREEN_H__
#define __EGLFS_KMS_SCREEN_H__

#include "eglfsscreen.h"
#include "kmsdevice.h"

class EglFSKmsScreen : public EglFSScreen
{
public:
    EglFSKmsScreen(KmsDevice *device, const KmsOutput &output);
    ~EglFSKmsScreen();

    void setVirtualPosition(const QPoint &pos);


    QRect rawGeometry() const override;    
    string name() const override;

    void setVirtualSiblings(vector<PlatformScreen *> sl) { m_siblings = sl; }
    KmsDevice *device() const { return m_device; }



    virtual void waitForFlip() =0;
    virtual void flip() =0;
    virtual void flipFinished() =0;

    KmsOutput &output() { return m_output; }


    void setPowerState(PlatformScreen::PowerState state) ;
protected:
    KmsDevice *m_device;

    KmsOutput m_output;
    QPoint m_pos;

    vector<PlatformScreen *> m_siblings;

    PowerState m_powerState;

    

};

#endif // __EGLFS_KMS_SCREEN_H__

