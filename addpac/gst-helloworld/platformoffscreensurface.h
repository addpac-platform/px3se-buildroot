#ifndef __PLATFORM_OFFSCREEN_SURFACE_H__
#define __PLATFORM_OFFSCREEN_SURFACE_H__

#include "platformsurface.h"
#include "offscreensurface.h"

class OffscreenSurface;

class PlatformOffscreenSurface : public PlatformSurface
{
public:
    explicit PlatformOffscreenSurface(OffscreenSurface *offscreenSurface);
    virtual ~PlatformOffscreenSurface();


};


#endif //__PLATFORM_OFFSCREEN_SURFACE_H__

