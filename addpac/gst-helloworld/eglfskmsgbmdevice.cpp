#include "eglfskmsgbmdevice.h"
#include "eglfskmsgbmscreen.h"







void EglFSKmsGbmDevice::pageFlipHandler(int fd, unsigned int sequence, unsigned int tv_sec, unsigned int tv_usec, void *user_data)
{
    Q_UNUSED(fd);
    Q_UNUSED(sequence);
    Q_UNUSED(tv_sec);
    Q_UNUSED(tv_usec);

    EglFSKmsScreen *screen = static_cast<EglFSKmsScreen *>(user_data);
    screen->flipFinished();
}






EglFSKmsGbmDevice::EglFSKmsGbmDevice(KmsScreenConfig *screenConfig, const string &path)
    : EglFSKmsDevice(screenConfig, path)
    , m_gbm_device(Q_NULLPTR)
    //, m_globalCursor(Q_NULLPTR)
{
}

bool EglFSKmsGbmDevice::open()
{
    assert(fd() == -1);
    assert(m_gbm_device == Q_NULLPTR);

    int fd = qt_safe_open(devicePath().c_str(), O_RDWR | O_CLOEXEC);
    if (fd == -1) {
        hDBG("Could not open DRM device %s", devicePath().c_str());
        return false;
    }

    qDebug() << "Creating GBM device for file descriptor:" << fd
                              << " obtained from" << devicePath() << endl;
    m_gbm_device = gbm_create_device(fd);
    if (!m_gbm_device) {
        qWarning() << "Could not create GBM device";
        qt_safe_close(fd);
        fd = -1;
        return false;
    }

    setFd(fd);

    return true;
}


void *EglFSKmsGbmDevice::nativeDisplay() const
{
    return m_gbm_device;
}


gbm_device * EglFSKmsGbmDevice::gbmDevice() const
{
    return m_gbm_device;
}


void EglFSKmsGbmDevice::handleDrmEvent()
{
    drmEventContext drmEvent;
    memset(&drmEvent, 0, sizeof(drmEvent));
    drmEvent.version = DRM_EVENT_CONTEXT_VERSION;
    drmEvent.vblank_handler = nullptr;
    drmEvent.page_flip_handler = pageFlipHandler;

    drmHandleEvent(fd(), &drmEvent);
}



PlatformScreen *EglFSKmsGbmDevice::createScreen(const KmsOutput &output)
{
    hDBG("");
    EglFSKmsGbmScreen *screen = new EglFSKmsGbmScreen(this, output);


    return screen;
}

