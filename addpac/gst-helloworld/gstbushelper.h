#ifndef __ADDPAC_GST_BUS_HELPER_H__
#define __ADDPAC_GST_BUS_HELPER_H__

#include "gstutils.h"
#include "apgstmessage.h"



#define ENABLE_THREAD


#ifdef ENABLE_THREAD
#include "osa_thread.h"
#include "osa_sem.h"
#endif


#include <queue>

class GstPlayerSession;
class GstVideoWindow;

class GstSyncMessageFilter {
public:
    //returns true if message was processed and should be dropped, false otherwise
    virtual bool processSyncMessage(const apGstMessage &message) = 0;
};


class GstBusMessageFilter {
public:
    //returns true if message was processed and should be dropped, false otherwise
    virtual bool processBusMessage(const apGstMessage &message) = 0;
};

class GstBusHelper 
{

public:
    GstBusHelper(GstBus* bus, void* parent = 0);
    ~GstBusHelper();
        
    void installMessageFilter(GstPlayerSession *filter);
    void removeMessageFilter(GstPlayerSession *filter);
    void installMessageFilter(GstVideoWindow *filter);
    void removeMessageFilter(GstVideoWindow *filter);

    // queue message & signal to thread
    void queueMessage(GstMessage* message);
    
    //static void doProcessBusMessage(GstBusHelper *);

    apGstMessage queueFront(){
        if(msgQ.empty()) 
            OSA_semWait(&rdSem, OSA_TIMEOUT_FOREVER);
        
        return msgQ.empty() ? apGstMessage() : msgQ.front();
    }
    bool queueIsEmpty() { return msgQ.empty(); }
    void queuePop(){ msgQ.pop(); }

public:
    mutex filterMutex;
    list<GstSyncMessageFilter*> syncFilters;
    list<GstBusMessageFilter*> busFilters;
    bool enableThread;

private:
    guint m_tag;
    GstBus* m_bus;

    queue<apGstMessage> msgQ;    
    std::thread *doThread;

    OSA_SemHndl rdSem;
};

#endif //__ADDPAC_GST_BUS_HELPER_H__

