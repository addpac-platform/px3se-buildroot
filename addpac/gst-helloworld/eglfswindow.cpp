#include "eglfswindow.h"
#include "eglfsscreen.h"
#include "openglcompositor.h"
#include "eglconvenience.h"
#include "ap_global.h"
#include "eglfsintegration.h"
#include "openglcontext.h"

extern AppPrivate app;


EglFSWindow::EglFSWindow(AWindow *w)
    : PlatformWindow(w)
    , m_screen(0)

//      ,m_backingStore(0),

//      m_raster(false),
//      m_winId(0),
    , m_surface(EGL_NO_SURFACE)
    , m_window(0)
//      m_flags(0)

{
    
}

EglFSWindow::~EglFSWindow()
{
    //destroy();
}


void EglFSWindow::create(PlatformScreen *s)
{
    //EglFSScreen *s = dynamic_cast<EglFSScreen *>(s);
    m_screen = static_cast<EglFSScreen *>(s);
    assert(m_screen);

    OpenGLCompositor *compositor = OpenGLCompositor::instance();
    if (m_screen->primarySurface() != EGL_NO_SURFACE) {
        hERR("not handled !!!\n");
        return;
    }


    // set geometry 
    //app.setGemetry(screen->geometry());

    resetSurface();

    if (Q_UNLIKELY(m_surface == EGL_NO_SURFACE)) {
        EGLint error = eglGetError();
        eglTerminate(m_screen->display());
        hERR("EGL Error : Could not create the egl surface: error = 0x%x\n", error);
    }

    m_screen->setPrimarySurface(m_surface);


    //if (isRaster()) 
    {
        OpenGLContext *context = new OpenGLContext(m_screen);
        context->setShareContext(qt_gl_global_share_context());
        

        context->setFormat(m_format);
        //context->setScreen(window()->screen());

        
        if (Q_UNLIKELY(!context->create()))
            hERR("EGLFS: Failed to create compositing context");

        printf("m_surface:%p, window():%p, this:%p\n", m_surface, window(), this);
        compositor->setTarget(context, window(), m_screen->rawGeometry());
        //compositor->setTarget(context, static_cast<AWindow *>(this), m_screen->rawGeometry());
        
        //compositor->setTarget(context, this, m_screen->rawGeometry());
        //compositor->setRotation(qEnvironmentVariableIntValue("QT_QPA_EGLFS_ROTATION"));
        
        // If there is a "root" window into which raster and QOpenGLWidget content is
        // composited, all other contexts must share with its context.
        if (!qt_gl_global_share_context()) {
            qt_gl_set_global_share_context(context);
            // What we set up here is in effect equivalent to the application setting
            // AA_ShareOpenGLContexts. Set the attribute to be fully consistent.

            // QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
        }
    }

    
}




void EglFSWindow::resetSurface()
{
    EGLDisplay display = m_screen->display();

    qDebug() << "reqFormat:" << window()->requestedFormat() <<endl;

    SurfaceFormat platformFormat = app.surfaceFormatFor(window()->requestedFormat());
    qDebug() << "platformFormat:" << platformFormat << endl;

    m_config = chooseConfig(display, platformFormat);

    m_format = q_glFormatFromConfig(display, m_config, platformFormat);
    std::cout << m_format << endl;
    
    const QSize surfaceSize = m_screen->rawGeometry().size();
    std::cout << "surfaceSize :" << surfaceSize << endl;

    m_window = createNativeWindow(m_screen, surfaceSize, m_format);
    
    m_surface = eglCreateWindowSurface(display, m_config, m_window, NULL);
    printf("m_surface:%p\n", m_surface);
    
}


