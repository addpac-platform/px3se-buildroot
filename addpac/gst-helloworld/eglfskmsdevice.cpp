
#include "eglfskmsdevice.h"
#include "eglfskmsscreen.h"

#include "ap_global.h"
extern AppPrivate app;


EglFSKmsDevice::EglFSKmsDevice(KmsScreenConfig *screenConfig, const string &path)
    : KmsDevice(screenConfig, path)
{
}

#if 1
void EglFSKmsDevice::registerScreen(PlatformScreen *screen,
                                     bool isPrimary,
                                     const QPoint &virtualPos,
                                     const vector<PlatformScreen *> &virtualSiblings)
{

    EglFSKmsScreen *s = static_cast<EglFSKmsScreen *>(screen);
    s->setVirtualPosition(virtualPos);
    s->setVirtualSiblings(virtualSiblings);

    app.addScreen(s, isPrimary);
    // TODO: addScreen
    //static_cast<EglFSIntegration *>(QGuiApplicationPrivate::platformIntegration())->addScreen(s, isPrimary);    
}
#endif
