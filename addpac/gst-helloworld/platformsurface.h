#ifndef __PLATFORM_SURFACE_H__
#define __PLATFORM_SURFACE_H__


#include "asurface.h"

class ASurface;

class PlatformSurface
{
public:

    explicit PlatformSurface(ASurface *surface);    
    virtual ~PlatformSurface();
//    virtual QSurfaceFormat format() const = 0;

    ASurface *surface() const{ return m_surface; }

private:

    ASurface *m_surface;

    friend class PlatformWindow;
//    friend class QPlatformOffscreenSurface;
};



#endif //__PLATFORM_SURFACE_H__

