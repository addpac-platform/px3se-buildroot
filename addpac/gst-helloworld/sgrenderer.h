// sgrenderer SGRENDERER
#ifndef __SGRENDERER_H__
#define __SGRENDERER_H__

#include "gstutils.h"



class SGRenderContext;
class SGBindable;


class SGRenderer
{
public:

    enum ClearModeBit
    {
        ClearColorBuffer    = 0x0001,
        ClearDepthBuffer    = 0x0002,
        ClearStencilBuffer  = 0x0004
    };

    
    SGRenderer(SGRenderContext *context);
    virtual ~SGRenderer();

    void renderScene(const SGBindable &bindable);
    void renderScene(unsigned int fboId = 0);

    QRect m_device_rect;
    QRect m_viewport_rect;


protected:
    virtual void render() = 0;

    double m_current_opacity;
    double m_current_determinant;
    double m_device_pixel_ratio;

    SGRenderContext *m_context;



private:
    bool m_changed_emitted;
    bool m_is_rendering;



};



class SGBindable
{
public:
    virtual ~SGBindable() { }
    virtual void bind() const = 0;
    virtual void clear(SGRenderer::ClearModeBit mode) const;
    virtual void reactivate() const;
};

//#if QT_CONFIG(opengl)
class SGBindableFboId : public SGBindable
{
public:
    SGBindableFboId(unsigned int);
    void bind() const override;
private:
    unsigned int m_id;
};



#endif //__SGRENDERER_H__

