
#include "gstutils.h"

void initializeGst()
{
    static bool initialized = false;
    if (!initialized) {
        initialized = true;
        gst_init(NULL, NULL);
    }
}



void qt_gst_object_ref_sink(gpointer object)
{
#if GST_CHECK_VERSION(0,10,24)
    gst_object_ref_sink(object);
#else
    g_return_if_fail (GST_IS_OBJECT(object));

    GST_OBJECT_LOCK(object);
    if (G_LIKELY(GST_OBJECT_IS_FLOATING(object))) {
        GST_OBJECT_FLAG_UNSET(object, GST_OBJECT_FLOATING);
        GST_OBJECT_UNLOCK(object);
    } else {
        GST_OBJECT_UNLOCK(object);
        gst_object_ref(object);
    }
#endif
}

GstCaps *qt_gst_pad_get_current_caps(GstPad *pad)
{
#if GST_CHECK_VERSION(1,0,0)
    return gst_pad_get_current_caps(pad);
#else
    return gst_pad_get_negotiated_caps(pad);
#endif
}

GstCaps *qt_gst_pad_get_caps(GstPad *pad)
{
#if GST_CHECK_VERSION(1,0,0)
    return gst_pad_query_caps(pad, NULL);
#elif GST_CHECK_VERSION(0, 10, 26)
    return gst_pad_get_caps_reffed(pad);
#else
    return gst_pad_get_caps(pad);
#endif
}

GstStructure *qt_gst_structure_new_empty(const char *name)
{
#if GST_CHECK_VERSION(1,0,0)
    return gst_structure_new_empty(name);
#else
    return gst_structure_new(name, NULL);
#endif
}

gboolean qt_gst_element_query_position(GstElement *element, GstFormat format, gint64 *cur)
{
#if GST_CHECK_VERSION(1,0,0)
    return gst_element_query_position(element, format, cur);
#else
    return gst_element_query_position(element, &format, cur);
#endif
}

gboolean qt_gst_element_query_duration(GstElement *element, GstFormat format, gint64 *cur)
{
#if GST_CHECK_VERSION(1,0,0)
    return gst_element_query_duration(element, format, cur);
#else
    return gst_element_query_duration(element, &format, cur);
#endif
}

GstCaps *qt_gst_caps_normalize(GstCaps *caps)
{
#if GST_CHECK_VERSION(1,0,0)
    // gst_caps_normalize() takes ownership of the argument in 1.0
    return gst_caps_normalize(caps);
#else
    // in 0.10, it doesn't. Unref the argument to mimic the 1.0 behavior
    GstCaps *res = gst_caps_normalize(caps);
    gst_caps_unref(caps);
    return res;
#endif
}

const gchar *qt_gst_element_get_factory_name(GstElement *element)
{
    const gchar *name = 0;
    const GstElementFactory *factory = 0;

    if (element && (factory = gst_element_get_factory(element)))
        name = gst_plugin_feature_get_name(GST_PLUGIN_FEATURE(factory));

    return name;
}

gboolean qt_gst_caps_can_intersect(const GstCaps * caps1, const GstCaps * caps2)
{
#if GST_CHECK_VERSION(0, 10, 25)
    return gst_caps_can_intersect(caps1, caps2);
#else
    GstCaps *intersection = gst_caps_intersect(caps1, caps2);
    gboolean res = !gst_caps_is_empty(intersection);
    gst_caps_unref(intersection);
    return res;
#endif
}

#if !GST_CHECK_VERSION(0, 10, 31)
static gboolean qt_gst_videosink_factory_filter(GstPluginFeature *feature, gpointer)
{
  guint rank;
  const gchar *klass;

  if (!GST_IS_ELEMENT_FACTORY(feature))
    return FALSE;

  klass = gst_element_factory_get_klass(GST_ELEMENT_FACTORY(feature));
  if (!(strstr(klass, "Sink") && strstr(klass, "Video")))
    return FALSE;

  rank = gst_plugin_feature_get_rank(feature);
  if (rank < GST_RANK_MARGINAL)
    return FALSE;

  return TRUE;
}

static gint qt_gst_compare_ranks(GstPluginFeature *f1, GstPluginFeature *f2)
{
  gint diff;

  diff = gst_plugin_feature_get_rank(f2) - gst_plugin_feature_get_rank(f1);
  if (diff != 0)
    return diff;

  return strcmp(gst_plugin_feature_get_name(f2), gst_plugin_feature_get_name (f1));
}
#endif

GList *qt_gst_video_sinks()
{
    GList *list = NULL;

#if GST_CHECK_VERSION(0, 10, 31)
    list = gst_element_factory_list_get_elements(GST_ELEMENT_FACTORY_TYPE_SINK | GST_ELEMENT_FACTORY_TYPE_MEDIA_VIDEO,
                                                 GST_RANK_MARGINAL);
#else
    list = gst_registry_feature_filter(gst_registry_get_default(),
                                       (GstPluginFeatureFilter)qt_gst_videosink_factory_filter,
                                       FALSE, NULL);
    list = g_list_sort(list, (GCompareFunc)qt_gst_compare_ranks);
#endif

    return list;
}

void qt_gst_util_double_to_fraction(gdouble src, gint *dest_n, gint *dest_d)
{
#if GST_CHECK_VERSION(0, 10, 26)
    gst_util_double_to_fraction(src, dest_n, dest_d);
#else
    qt_real_to_fraction(src, dest_n, dest_d);
#endif
}

