#include <stdio.h>

void test(){
	printf("%s\n", __FUNCTION__);
}


#include <gst/gst.h>

#include <gst/gstvalue.h>
#include <gst/base/gstbasesrc.h>





#define true TRUE
#define false FALSE








GstElement* m_playbin;

GstElement* m_videoSink;

GstElement* m_videoOutputBin;
GstElement* m_videoIdentity;
GstElement* m_pendingVideoSink;
GstElement* m_nullVideoSink;

GstElement* m_audioSink;
GstElement* m_volumeElement;

GstBus* m_bus;

# define QT_GSTREAMER_PLAYBIN_ELEMENT_NAME "playbin"
# define QT_GSTREAMER_CAMERABIN_ELEMENT_NAME "camerabin"
# define QT_GSTREAMER_COLORCONVERSION_ELEMENT_NAME "videoconvert"
# define QT_GSTREAMER_RAW_AUDIO_MIME "audio/x-raw"
# define QT_GSTREAMER_VIDEOOVERLAY_INTERFACE_NAME "GstVideoOverlay"


typedef enum {
    GST_PLAY_FLAG_VIDEO         = 0x00000001,
    GST_PLAY_FLAG_AUDIO         = 0x00000002,
    GST_PLAY_FLAG_TEXT          = 0x00000004,
    GST_PLAY_FLAG_VIS           = 0x00000008,
    GST_PLAY_FLAG_SOFT_VOLUME   = 0x00000010,
    GST_PLAY_FLAG_NATIVE_AUDIO  = 0x00000020,
    GST_PLAY_FLAG_NATIVE_VIDEO  = 0x00000040,
    GST_PLAY_FLAG_DOWNLOAD      = 0x00000080,
    GST_PLAY_FLAG_BUFFERING     = 0x000000100
} GstPlayFlags;


void test_my_gst(void )
{
    m_playbin = gst_element_factory_make(QT_GSTREAMER_PLAYBIN_ELEMENT_NAME, NULL);
    if (m_playbin) {
        int flags = GST_PLAY_FLAG_VIDEO | GST_PLAY_FLAG_AUDIO;
        g_object_set(G_OBJECT(m_playbin), "flags", flags, NULL);

        GstElement *audioSink = gst_element_factory_make("autoaudiosink", "audiosink");
        if (audioSink) {
            m_volumeElement = gst_element_factory_make("volume", "volumeelement");
            if (m_volumeElement) {
                m_audioSink = gst_bin_new("audio-output-bin");
                gst_bin_add_many(GST_BIN(m_audioSink), m_volumeElement, audioSink, NULL);
                gst_element_link(m_volumeElement, audioSink);

                GstPad *pad = gst_element_get_static_pad(m_volumeElement, "sink");
                gst_element_add_pad(GST_ELEMENT(m_audioSink), gst_ghost_pad_new("sink", pad));
                gst_object_unref(GST_OBJECT(pad));                
            }
            g_object_set(G_OBJECT(m_playbin), "audio-sink", m_audioSink, NULL);            
        }
    }


    m_videoIdentity = gst_element_factory_make("identity", NULL); // floating ref

    m_nullVideoSink = gst_element_factory_make("fakesink", NULL);
    g_object_set(G_OBJECT(m_nullVideoSink), "sync", TRUE, NULL);
    gst_object_ref(GST_OBJECT(m_nullVideoSink));

    m_videoOutputBin = gst_bin_new("video-output-bin");
    // might not get a parent, take ownership to avoid leak
    qt_gst_object_ref_sink(GST_OBJECT(m_videoOutputBin));
    gst_bin_add_many(GST_BIN(m_videoOutputBin), m_videoIdentity, m_nullVideoSink, NULL);
    gst_element_link(m_videoIdentity, m_nullVideoSink);

    m_videoSink = m_nullVideoSink;

    // add ghostpads
    GstPad *pad = gst_element_get_static_pad(m_videoIdentity,"sink");
    gst_element_add_pad(GST_ELEMENT(m_videoOutputBin), gst_ghost_pad_new("sink", pad));
    gst_object_unref(GST_OBJECT(pad));

    if (m_playbin != 0) {
#if 0    
        // Sort out messages
        m_bus = gst_element_get_bus(m_playbin);
        m_busHelper = new QGstreamerBusHelper(m_bus, this);
        m_busHelper->installMessageFilter(this);

        g_object_set(G_OBJECT(m_playbin), "video-sink", m_videoOutputBin, NULL);

        g_signal_connect(G_OBJECT(m_playbin), "notify::source", G_CALLBACK(playbinNotifySource), this);
        g_signal_connect(G_OBJECT(m_playbin), "element-added",  G_CALLBACK(handleElementAdded), this);

        g_signal_connect(G_OBJECT(m_playbin), "video-changed", G_CALLBACK(handleStreamsChange), this);
        g_signal_connect(G_OBJECT(m_playbin), "audio-changed", G_CALLBACK(handleStreamsChange), this);
        g_signal_connect(G_OBJECT(m_playbin), "text-changed", G_CALLBACK(handleStreamsChange), this);


        g_signal_connect(G_OBJECT(m_playbin), "deep-notify::source", G_CALLBACK(configureAppSrcElement), this);
#endif
    }


}

