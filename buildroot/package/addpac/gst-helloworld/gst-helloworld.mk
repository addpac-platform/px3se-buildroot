################################################################################
#
# addpac 
#
################################################################################

GST_HELLOWORLD_SITE = $(TOPDIR)/../addpac/gst-helloworld
GST_HELLOWORLD_SITE_METHOD = local


define GST_HELLOWORLD_BUILD_CMDS
    $(MAKE) $(TARGET_CONFIGURE_OPTS) -C $(@D) all
# aptest
endef



define GST_HELLOWORLD_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0755 $(@D)/helloworld $(TARGET_DIR)/usr/bin/helloworld
#	$(INSTALL) -D -m 0755 $(@D)/test_libavsys $(TARGET_DIR)/usr/bin/test_libavsys
#    $(INSTALL) -D -m 0755 $(@D)/rect $(TARGET_DIR)/usr/bin/fb-test-rect
#    $(INSTALL) -D -m 0755 $(@D)/fb-test $(TARGET_DIR)/usr/bin/fb-test
#    $(INSTALL) -D -m 0755 $(@D)/offset $(TARGET_DIR)/usr/bin/fb-test-offset
endef


$(eval $(generic-package))

