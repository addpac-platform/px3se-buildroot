################################################################################
#
# netcat
#
################################################################################

BRCM_PATCHRAM_PLUS_VERSION = 2011
# source included in the package
# came from android repository:
# https://android.googlesource.com/platform/system/bluetooth/+/master/brcm_patchram_plus/brcm_patchram_plus.c
BRCM_PATCHRAM_PLUS_SITE=$(TOPDIR)/package/brcm_patchram_plus
BRCM_PATCHRAM_PLUS_SITE_METHOD=local
BRCM_PATCHRAM_PLUS_LICENSE = Apache-2.0

define BRCM_PATCHRAM_PLUS_BUILD_CMDS
  $(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) \
    $(@D)/brcm_patchram_plus.c -o $(@D)/brcm_patchram_plus
endef

define BRCM_PATCHRAM_PLUS_INSTALL_TARGET_CMDS
  $(INSTALL) -D -m0755 $(@D)/brcm_patchram_plus $(TARGET_DIR)/usr/bin
endef

$(eval $(generic-package))
