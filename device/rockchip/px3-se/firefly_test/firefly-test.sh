#!/bin/bash
Fire_Dir="/var/cache/firefly"
Mnt="/mnt"
K=$1
firefly_update=0

update_tool()
{
if [ -d $Mnt/firefly-test ]; then
	echo "update firefly-test ... "
		if [ -d $Fire_Dir ]; then
	       		rm $Fire_Dir/ -fr
		fi
       	cp -rf $Mnt/firefly-test $Fire_Dir/
	firefly_update=1
fi
}

kill_proc()
{
NAME="/var/cache/firefly"
echo $NAME
ID=`ps -ef | grep "$NAME" | grep -v "$0" | grep -v "grep" | awk '{print $2}'`
echo $ID
echo "---------------"
for id in $ID
do
kill -9 $id
echo "killed $id"
done
echo "---------------"
}


if [ "$K" = "k" ] || [ "$K" = "K" ] || [ "$K" = "1" ]; then
	kill_proc
	exit 1
fi

for dev in `(ls /dev/mmcblk* /dev/sd*)`; do
	if mount $dev $Mnt ; then
	update_tool
	umount $Mnt
		if [ $firefly_update = 1 ]; then
			break
		fi
	fi
done

cd $Fire_Dir
bash ./startup.sh $firefly_update 

