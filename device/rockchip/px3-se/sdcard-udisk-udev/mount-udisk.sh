#!/bin/sh
#TYPE=`blkid /dev/$1 | awk -F '"' '{print $4}'`
TYPE=`parted  /dev/$1 print | grep -i -A1 "File system" | tail -n1`

if [ $(echo $TYPE |  grep -i exfat | wc -l) == "1" ] ;then
	mount.exfat /dev/$1 /mnt/udisk
elif [ $(echo $TYPE |  grep -i ntfs | wc -l) == "1" ] ;then

	mount.ntfs-3g /dev/$1 /mnt/udisk
else
	mount /dev/$1 /mnt/udisk
fi

sync
